-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2018 at 04:11 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `halallocal_ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_resources`
--

CREATE TABLE `acl_resources` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('module','controller','action','other') NOT NULL DEFAULT 'other',
  `parent` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_resources`
--

INSERT INTO `acl_resources` (`id`, `name`, `type`, `parent`, `created`, `modified`) VALUES
(1, 'welcome', 'module', NULL, '2012-11-12 12:07:26', NULL),
(2, 'auth', 'module', NULL, '2012-11-12 04:00:23', NULL),
(3, 'auth/login', 'controller', 2, '2012-11-12 12:43:42', '2012-11-12 12:44:06'),
(4, 'auth/logout', 'controller', 2, '2012-11-12 12:43:56', NULL),
(5, 'auth/user', 'controller', 2, '2012-11-12 04:07:59', '2012-11-12 08:29:29'),
(6, 'acl', 'module', NULL, '2012-02-02 13:47:43', NULL),
(7, 'acl/resource', 'controller', 6, '2012-02-02 13:47:57', NULL),
(8, 'acl/resource/index', 'action', 7, '2012-02-02 13:48:21', NULL),
(9, 'acl/resource/add', 'action', 7, '2012-02-02 13:48:35', '2012-10-16 17:26:12'),
(10, 'acl/resource/edit', 'action', 7, '2012-02-02 13:48:50', '2012-07-09 18:44:38'),
(11, 'acl/resource/delete', 'action', 7, '2012-02-02 13:49:06', NULL),
(12, 'acl/role', 'controller', 6, '2012-07-12 17:54:16', NULL),
(13, 'acl/role/index', 'action', 12, '2012-07-12 17:55:29', NULL),
(14, 'acl/role/add', 'action', 12, '2012-07-12 17:56:00', NULL),
(15, 'acl/role/edit', 'action', 12, '2012-07-12 17:56:19', NULL),
(16, 'acl/role/delete', 'action', 12, '2012-07-12 17:56:55', NULL),
(17, 'acl/rule', 'controller', 6, '2012-07-12 17:53:04', NULL),
(18, 'acl/rule/edit', 'action', 17, '2012-07-12 17:53:25', NULL),
(19, 'utils', 'module', NULL, NULL, NULL),
(20, 'dashboard', 'module', NULL, NULL, NULL),
(21, 'api', 'module', NULL, NULL, NULL),
(22, 'samples', 'module', NULL, NULL, NULL),
(25, 'slide', 'module', NULL, NULL, NULL),
(26, 'booking', 'module', NULL, NULL, NULL),
(27, 'master', 'module', NULL, NULL, NULL),
(28, 'data_pelanggan', 'module', 27, NULL, NULL),
(29, 'data_fasilitas', 'module', 27, NULL, NULL),
(30, 'tour', 'module', NULL, NULL, NULL),
(31, 'trip', 'module', 30, NULL, NULL),
(32, 'umrah', 'module', 30, NULL, NULL),
(33, 'booking_trip', 'module', 26, NULL, NULL),
(34, 'booking_umroh', 'module', 26, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acl_roles`
--

CREATE TABLE `acl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_roles`
--

INSERT INTO `acl_roles` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrator', '2011-12-27 12:00:00', NULL),
(2, 'Guest', '2011-12-27 12:00:00', NULL),
(3, 'Staf', '2012-11-12 04:30:02', '2012-11-12 04:30:39'),
(4, 'Manager', '2012-11-12 04:30:24', NULL),
(5, 'Tour Operator', NULL, NULL),
(6, 'Marketing', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acl_role_parents`
--

CREATE TABLE `acl_role_parents` (
  `role_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_role_parents`
--

INSERT INTO `acl_role_parents` (`role_id`, `parent`, `order`) VALUES
(3, 2, 0),
(4, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acl_rules`
--

CREATE TABLE `acl_rules` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `access` enum('allow','deny') NOT NULL DEFAULT 'deny',
  `priviledge` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_rules`
--

INSERT INTO `acl_rules` (`role_id`, `resource_id`, `access`, `priviledge`) VALUES
(2, 1, 'allow', NULL),
(2, 3, 'allow', NULL),
(2, 4, 'allow', NULL),
(3, 20, 'allow', NULL),
(4, 2, 'allow', NULL),
(4, 5, 'allow', NULL),
(5, 1, 'allow', NULL),
(5, 3, 'allow', NULL),
(5, 4, 'allow', NULL),
(5, 20, 'allow', NULL),
(5, 21, 'allow', NULL),
(5, 26, 'allow', NULL),
(5, 30, 'allow', NULL),
(5, 31, 'allow', NULL),
(5, 32, 'allow', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_autologin`
--

CREATE TABLE `auth_autologin` (
  `user` int(11) NOT NULL,
  `series` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_autologin`
--

INSERT INTO `auth_autologin` (`user`, `series`, `key`, `created`) VALUES
(1720, 'e249772c053abaf6ec0ddf057ea558631abd9939627ac0c855dfef011512faa3', '7214a4e8f9361bebd5e214c5b8437affc08518cb97dabfe66c3cbb3ebbe08dd8', '2018-01-03 08:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `auth_users`
--

CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL,
  `id_tour_operator` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `registered` datetime NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users`
--

INSERT INTO `auth_users` (`id`, `id_tour_operator`, `username`, `password`, `lang`, `registered`, `role_id`) VALUES
(1720, 33, 'halallocal', '$2a$08$3cnaWBtlqurq4juNu8N66.9E/MkAjswMo4aPG6JimuXwuSxTQKjme', 'id', '2017-11-21 12:55:08', 1),
(1721, 34, 'permata', '$2a$08$6WRV8/5xNsMNXyZHLWgMi.qWNC9WZMNZIaJfhV31m4VGPnzFQLdrW', 'id', '2017-11-23 13:43:41', 5),
(1722, 35, 'test', '$2a$08$076xqoWUVgQ6dhgt.JV97uzG.MLRLUP9aaJHQLjcORgxnzY8hRp/y', 'id', '2017-12-18 13:34:48', 5),
(1723, 38, 'patuna', '$2a$08$5a1VM/nwbT.IYO/OpswQ3e24PQCIp4495mbjbPx.EV3uKuXAYWhkC', 'id', '2018-01-03 15:08:39', 5),
(1726, 39, 'royaltour', '$2a$08$6kmywCnug3D7JAfTRT3hne9HCMCHovIC0SHf5dvGZOKeE0ArfGbUC', 'en', '2018-01-08 11:27:45', 5),
(1727, 40, 'khb', '$2a$08$KPhkoTUvAaoHk3MEShcwr.HYty7NbPiaG5HRKQrg7QOr4IByqhLIC', 'en', '2018-01-09 08:34:16', 5),
(1728, 41, 'daqu', '$2a$08$VD.Z./2ACr4UeZ1jBfmkPe.ak0aEN6.bFGRw5mLru53HZJpkWVQsa', 'en', '2018-01-09 08:34:45', 5),
(1729, 42, 'arminareka', '$2a$08$y0xkkkSh8pF./BmVUe7mdun6D0O2ezIuuYCtiolU55E/KPJsz66ou', 'en', '2018-01-09 08:36:54', 5),
(1730, 43, 'darussalam', '$2a$08$a/dIlLspZcvdTBbUr8xPpuIA39/QzOXrsSF6lHuT1TaUwfShn9BXW', 'en', '2018-01-09 09:12:11', 5);

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_master`
--

CREATE TABLE `auth_users_master` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `registered` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users_master`
--

INSERT INTO `auth_users_master` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `registered`) VALUES
(1002, 'Diane', 'Murphy', 'dmurphy', 'dmurphy@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1056, 'Mary', 'Patterson', 'mpatterso', 'mpatterso@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1076, 'Jeff', 'Firrelli', 'jeff.firrelli', 'jeff.firrelli@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1088, 'William', 'Patterson', 'wpatterson', 'wpatterson@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1102, 'Gerard', 'Bondur', 'gbondur', 'gbondur@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1143, 'Anthony', 'Bow', 'abow', 'abow@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1165, 'Leslie', 'Jennings', 'ljennings', 'ljennings@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1166, 'Leslie', 'Thompson', 'lthompson', 'lthompson@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1188, 'Julie', 'Firrelli', 'julie.firrelli', 'julie.firrelli@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1216, 'Steve', 'Patterson', 'spatterson', 'spatterson@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1286, 'Foon Yue', 'Tseng', 'ftseng', 'ftseng@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1323, 'George', 'Vanauf', 'gvanauf', 'gvanauf@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1337, 'Loui', 'Bondur', 'lbondur', 'lbondur@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1370, 'Gerard', 'Hernandez', 'ghernande', 'ghernande@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1401, 'Pamela', 'Castillo', 'pcastillo', 'pcastillo@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1501, 'Larry', 'Bott', 'lbott', 'lbott@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1504, 'Barry', 'Jones', 'bjones', 'bjones@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1611, 'Andy', 'Fixter', 'afixter', 'afixter@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1612, 'Peter', 'Marsh', 'pmarsh', 'pmarsh@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1619, 'Tom', 'King', 'tking', 'tking@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1621, 'Mami', 'Nishi', 'mnishi', 'mnishi@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1625, 'Yoshimi', 'Kato', 'ykato', 'ykato@classicmodelcars.com', '', '2012-03-01 05:54:30'),
(1702, 'Martin', 'Gerard', 'mgerard', 'mgerard@classicmodelcars.com', '', '2012-03-01 05:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `kode_pesanan` varchar(11) NOT NULL,
  `id_tour` int(11) NOT NULL,
  `nama` varchar(125) NOT NULL,
  `telp` varchar(32) NOT NULL,
  `email` varchar(125) NOT NULL,
  `code` varchar(125) NOT NULL,
  `approve` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `visible` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `kode_pesanan`, `id_tour`, `nama`, `telp`, `email`, `code`, `approve`, `created_at`, `updated_at`, `visible`) VALUES
(247, '97173526924', 3, 'Didik Kurniawan', '412421', 'didikkurniawannn@gmail.com', '56FBAC', 0, '2018-01-08 05:05:34', '0000-00-00 00:00:00', 1),
(248, '06783360285', 3, 'rafika', '085720239030', 'fadillahrafika@gmail.com', 'AGJL6O', 0, '2018-01-17 07:55:43', '0000-00-00 00:00:00', 1),
(249, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'GVNTLB', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(250, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'JXT93B', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(251, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'LQKSPI', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(252, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'HYB1QS', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(253, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'UMMJLG', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(254, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'N7F4HB', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(255, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'HNVMQP', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(256, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'LKUXDH', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(257, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'RRJVBD', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1),
(258, '05912841669', 4, 'Didik Kurniawan', '1312313', 'didikkurniawannn@gmail.com', 'UZWVQD', 0, '2018-01-22 07:10:34', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_umroh`
--

CREATE TABLE `booking_umroh` (
  `id` int(11) NOT NULL,
  `kode_pesanan` varchar(11) NOT NULL,
  `id_umroh` int(11) NOT NULL,
  `nama` varchar(125) NOT NULL,
  `telp` varchar(32) NOT NULL,
  `email` varchar(125) NOT NULL,
  `code` varchar(125) NOT NULL,
  `approve` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `visible` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_umroh`
--

INSERT INTO `booking_umroh` (`id`, `kode_pesanan`, `id_umroh`, `nama`, `telp`, `email`, `code`, `approve`, `created_at`, `updated_at`, `visible`) VALUES
(253, '98028431145', 3, 'Didik Kurniawan', '085722974447', 'didikkurniawannn@gmail.com', '43ZA6N', 0, '2018-01-03 08:59:52', '0000-00-00 00:00:00', 1),
(254, '39634641851', 3, 'Mahdi Rizal', '008123233', 'mahdi.gf@gmail.com', 'USGFBA', 0, '2018-01-04 06:54:28', '0000-00-00 00:00:00', 1),
(255, '39634641851', 3, 'Mahdi Rizal', '008123233', 'mahdi.gf@gmail.com', 'VGLYXH', 0, '2018-01-04 06:54:31', '0000-00-00 00:00:00', 1),
(256, '39634641851', 3, 'Mahdi Rizal', '008123233', 'mahdi.gf@gmail.com', '02VZSI', 0, '2018-01-04 06:54:33', '0000-00-00 00:00:00', 1),
(257, '81246517893', 4, 'Didik Kurniawan', '1241241', 'didikkurniawannn@gmail.com', '0YADVC', 0, '2018-01-08 06:26:49', '0000-00-00 00:00:00', 1),
(258, '10253795194', 4, 'Didik Kurniawan', '1241241', 'didikkurniawannn@gmail.com', 'PZ7USJ', 0, '2018-01-08 06:27:18', '0000-00-00 00:00:00', 1),
(259, '14305608723', 4, 'Didik Kurniawan', '1241241', 'didikkurniawannn@gmail.com', 'OVQSQU', 0, '2018-01-08 06:29:44', '0000-00-00 00:00:00', 1),
(260, '52635091710', 3, 'Didik Kurniawan', '1431241', 'didik.kurniawan@mail.unpas.ac.id', 'PRCFOG', 0, '2018-01-08 06:32:11', '0000-00-00 00:00:00', 1),
(261, '01981066245', 3, 'Didik Kurniawan', '1431241', 'didik.kurniawan@mail.unpas.ac.id', 'TJJB0W', 0, '2018-01-08 06:35:12', '0000-00-00 00:00:00', 1),
(262, '31680581270', 3, 'Didik Kurniawan', '1431241', 'didik.kurniawan@mail.unpas.ac.id', 'JP2W0D', 0, '2018-01-08 08:24:39', '0000-00-00 00:00:00', 1),
(263, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'RFW6BA', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(264, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'GE50DP', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(265, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'YFK2ZP', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(266, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'FQGMNU', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(267, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'YPWHDK', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(268, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'NLKW6Y', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(269, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'BCROWM', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(270, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'YJISO7', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(271, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'RZECSP', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(272, '40723261734', 3, 'Yodha', '07448168182', 'senoyodha@gmail.com', 'YOPTH1', 0, '2018-01-22 07:09:17', '0000-00-00 00:00:00', 1),
(273, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', '6MN1LF', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(274, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', '9VUNZW', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(275, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', '5ILSQA', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(276, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', 'ZBEKDQ', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(277, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', 'U9M7WK', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(278, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', 'MVZP4H', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(279, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', '9SFFTP', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(280, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', 'RJUI6N', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1),
(281, '89713420073', 3, 'Didik Kurniawan', '14312312', 'didikkurniawannn@gmail.com', '1IP48Q', 0, '2018-01-22 07:11:08', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_pelanggan`
--

CREATE TABLE `data_pelanggan` (
  `id` int(11) NOT NULL,
  `name_operator` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_operator` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `wa` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sipt` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ijin_trevel_umrah` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ijin_trevel_haji` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_operator` text COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_pelanggan`
--

INSERT INTO `data_pelanggan` (`id`, `name_operator`, `category`, `address`, `phone`, `email_operator`, `wa`, `website`, `sipt`, `ijin_trevel_umrah`, `ijin_trevel_haji`, `avatar_operator`, `visible`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(33, 'Halal Local', '2', 'Jalan Banda No. 40,\r\nGedung Bale Motekar Lt. 3', '086587887687', 'info@halallocal.com', '086587887687', 'halallocal.com', '-', '-', '-', 'uploads/halallocal.jpeg', 1, '2017-11-15 11:31:16', '0000-00-00 00:00:00', 1709, 0),
(38, 'Patuna Travel', '1', 'Jl. Panglima Polim Raya No. 43 A-B Kebayoran\r\nKota Jakarta Selatan, DKI Jakarta', '0217228830', 'cs@patunatravel.com', '0217228830', 'http://patunatravel.com/', '', 'D/648 TAHUN 2013', '-', 'uploads/03012018135059patuna1.png', 1, '2018-01-03 06:50:59', '0000-00-00 00:00:00', 1720, 0),
(39, 'Royal Indonesia', '2', 'The Great Saladdin Square Block C-18, No, Jalan Margonda Raya No.39, Depok, Pancoran MAS, Depok, Pancoran MAS, Depok City, West Java 16431', '022129402232', 'hello@royalindonesia.id', '', 'www.royalindonesia.id', '', '', '', 'uploads/08012018111202royal.png', 1, '2018-01-08 04:12:02', '0000-00-00 00:00:00', 1720, 0),
(40, 'KHB Travel ', '1', 'Ajwad Resto - Lantai 3\r\n\r\nJl. Condet Raya No. 50, RT.1/RW.3\r\nBatu Ampar, Kramatjati, Kota Jakarta Timur, DKI Jakarta 13520', '087771111143', 'khb@halalocal.com', '', '', '', '', '', 'uploads/09012018082842Capture.PNG', 1, '2018-01-09 01:28:42', '0000-00-00 00:00:00', 1720, 0),
(41, 'Daqu Travel', '1', 'Kawasan Bisnis CBD CIledug Blok A5 / 21\r\nJl. HOS Cokroaminoto Ciledug \r\nKarang Tengah Ciledug Tangerang Banten 15157', '02173451933', 'daqu@halallocal.com', '', '', '', '', '', 'uploads/09012018083120Capture.PNG', 1, '2018-01-09 01:31:20', '0000-00-00 00:00:00', 1720, 0),
(42, 'Arminareka Perdana', '1', 'Gedung Menara Salemba Lt.V, Jl.Salemba Raya No.05, RT.1/RW.3, Paseban, Senen, akarta Pusat, Daerah Khusus Ibukota Jakarta 10440', '087890008810', 'info@halallocal.com', '', 'www.arminarekaperdana.co.id', '', '', '', 'uploads/09012018083338Capture.PNG', 1, '2018-01-09 01:33:38', '0000-00-00 00:00:00', 1720, 0),
(43, 'Darussalam', '1', 'Gedung Wakaf Pro 99 LT.2, Jl. Sidomukti No.99H', '02220458498', 'info@halallocal.com', '', '', '', '', '', 'uploads/09012018091148Capture.PNG', 1, '2018-01-09 02:11:48', '0000-00-00 00:00:00', 1720, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_airline`
--

CREATE TABLE `m_airline` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `lunggage` text COLLATE utf8_unicode_ci NOT NULL,
  `economy_class` text COLLATE utf8_unicode_ci NOT NULL,
  `bussiness_class` text COLLATE utf8_unicode_ci NOT NULL,
  `first_class` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_airline`
--

INSERT INTO `m_airline` (`id`, `name`, `lunggage`, `economy_class`, `bussiness_class`, `first_class`, `description`, `rating`, `image`, `visible`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'awdewqe', 'sdfasefa', 'sfdsf', 'QFDSFD', 'FSFW', 'SDSDFSF', 2, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_airline_image`
--

CREATE TABLE `m_airline_image` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_airline` int(11) UNSIGNED NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_facilities`
--

CREATE TABLE `m_facilities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_facilities`
--

INSERT INTO `m_facilities` (`id`, `name`, `image`, `description`, `created_at`, `created_by`) VALUES
(3, 'Makanan', 'uploads/icon/29112017150016pref-alt-04.png', '<p>Makanan sesuai dengan trevel penyedia</p>', '2017-11-29 08:00:16', 1720);

-- --------------------------------------------------------

--
-- Table structure for table `m_hotel`
--

CREATE TABLE `m_hotel` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `facilities` text COLLATE utf8_unicode_ci NOT NULL,
  `service` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_hotel`
--

INSERT INTO `m_hotel` (`id`, `name`, `facilities`, `service`, `description`, `rating`, `image`, `visible`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'SGFSG', 'SGFSGSD', 'SEGFSGS', 'SWFGSGS', 7, '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tour_trip`
--

CREATE TABLE `tour_trip` (
  `id` int(11) UNSIGNED NOT NULL,
  `category_trip` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `depature_date` date NOT NULL,
  `return_date` date NOT NULL,
  `price` double NOT NULL,
  `price_reteal` double NOT NULL,
  `komisi` double NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `facilities_include` text COLLATE utf8_unicode_ci NOT NULL,
  `facilities_exclude` text COLLATE utf8_unicode_ci NOT NULL,
  `visible` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tour_trip`
--

INSERT INTO `tour_trip` (`id`, `category_trip`, `title`, `depature_date`, `return_date`, `price`, `price_reteal`, `komisi`, `description`, `facilities_include`, `facilities_exclude`, `visible`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3, 1, 'Halal Holidays Lombok', '2018-01-01', '2018-12-31', 4362700, 4562700, 200000, '<p>Halal Holidays Lombok tiga hari bebas pilih tanggal</p>', '\"Ac transportation,Guide assistance,Tour and meals as per above program,Donation, entrance fee, parking for tourism object\"', '\"Optional tour at Gili trawangan, Glass bottom boat, snorkeling equipment\"', 1, '2018-01-08 04:51:56', '0000-00-00 00:00:00', 1726, 0),
(4, 1, 'Halal Holidays Aceh', '2018-02-01', '2018-02-03', 4070000, 4220000, 150000, '<p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{\"1\":2,\"2\":\"Halal Holidays Aceh\"}\" data-sheets-userformat=\"{\"2\":13311,\"3\":[null,0],\"4\":[null,2,6989903],\"5\":{\"1\":[{\"1\":2,\"2\":0,\"5\":[null,2,0]},{\"1\":0,\"2\":0,\"3\":3},{\"1\":1,\"2\":0,\"4\":1}]},\"6\":{\"1\":[{\"1\":2,\"2\":0,\"5\":[null,2,0]},{\"1\":0,\"2\":0,\"3\":3},{\"1\":1,\"2\":0,\"4\":1}]},\"7\":{\"1\":[{\"1\":2,\"2\":0,\"5\":[null,2,0]},{\"1\":0,\"2\":0,\"3\":3},{\"1\":1,\"2\":0,\"4\":1}]},\"8\":{\"1\":[{\"1\":2,\"2\":0,\"5\":[null,2,0]},{\"1\":0,\"2\":0,\"3\":3},{\"1\":1,\"2\":0,\"4\":1}]},\"9\":0,\"10\":1,\"11\":3,\"12\":0,\"15\":\"Arial\",\"16\":10}\">Halal Holidays Aceh</span><br></p>', '\"Transport,Hotel,Makan sesuai program,Biaya masuk obyek wisata,Guide,Air Mineral\"', '\"Tipping guide dan driver,Porter,Biaya deviasi,Optional tour,Asuransi\"', 1, '2018-01-09 02:43:42', '0000-00-00 00:00:00', 1726, 0),
(6, 2, 'Halal Holidays Japan', '2018-02-01', '2018-02-05', 11500000, 12000000, 500000, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Halal Holidays Japan</span></p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;All Japan/*3&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\"><span style=\"font-size: 10pt; font-weight: bold;\">Maskapai :&nbsp;</span><span style=\"font-size: 10pt;\">Citilink/AirAsia</span></span></p><p><span style=\"font-family: Arial; font-size: 10pt; font-weight: bold;\">Hotel :&nbsp;</span><span style=\"font-family: Arial; font-size: 10pt;\">All Japan/*3</span></p>', '\"Penerbangan internasional kelas ekonomi PP,Akomodasi, transportasi & makan sesuai program,Visa,Pemandu wisata,Asuransi perjalanan,Tour leader dari Jakarta (min. 20 dewasa)\"', '\"Tipping Guide & Driver $5\\/pax\\/day.,Kelebihan Bagasi,Belanja pribadi,Tour tambahan di luar program\"', 1, '2018-01-09 03:39:21', '0000-00-00 00:00:00', 1726, 0),
(7, 2, 'Halal Holidays Turkey', '2018-02-01', '2018-02-10', 19240000, 19990000, 750000, '<p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Turki&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Halal Holidays Turki</span></p><p><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Turki&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">-Untuk keberangkatan April, ada penambahan program mengunjungi Taman Tulip di Istanbul.</span><br></span></p><p><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Turki&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\"><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Maskapai&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Maskapai :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Turkish Airlines</span></span></p><p><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Turki&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\"><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel :&nbsp;</span><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Tiara Hotel Bursa / *4\\nRoyal Palace / *4\\nNinova / *4\\nAltinoz / *4\\nRoyal Carina / *4\\nGolden Way / *4&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Tiara Hotel Bursa / *4<br>Royal Palace / *4<br>Ninova / *4<br>Altinoz / *4<br>Royal Carina / *4<br>Golden Way / *4</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></span><br></p>', '\"Hotel,Tiket masuk objek wisata sesuai dengan program,Makan sesuai program,Guide,Wifi didalam bus,Porter,Air mineral,Visa,Airport tax,Airport Handling (tanpa porter\"', '\"Tipping untuk tour leader\"', 1, '2018-01-09 03:59:53', '0000-00-00 00:00:00', 1726, 0),
(8, 1, 'Halal Holidays Padang ', '2018-02-01', '2018-02-03', 4549998, 4749998, 200000, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Halal Holidays Padang</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Maskapai&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Maskapai :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Lion Air/Air Asia</span></p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><p><span style=\"font-family: Arial; font-size: 10pt; font-weight: bold;\">Hotel :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Lion Air/Air Asia</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></p>', '\"Hotel,Transportasi,Makan,Extra kuliner,Tiket masuk objek wisata.,Air mineral, Driver \\/ Tour Leader\"', '\"Travel insurance,Tiket pesawat,Biaya pribadi : loundry, juice \\/ soft drink,Guide lokal lubang Jepang\"', 1, '2018-01-09 04:18:55', '0000-00-00 00:00:00', 1726, 0),
(9, 1, 'Halal Holidays Lombok', '2018-02-01', '2018-02-03', 4362700, 4562700, 200000, '<p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Lombok&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:14275305},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\">Halal Holidays Lombok</span></p><p><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Lombok&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:14275305},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\"><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Maskapai&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Maskapai :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Garuda Indonesia</span></span></p><p><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Halal Holidays Lombok&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:13311,&quot;3&quot;:[null,0],&quot;4&quot;:{&quot;1&quot;:2,&quot;2&quot;:14275305},&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10}\"><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Lombok/*4</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></span><br></p>', '\"Akomodasi,Ac transportation,Guide assistance,Tour and meals,-Donation, entrance fee, parking for tourism object,Cool face towel and mineral water during tour program\"', '\"Optional tour at Gili trawangan such as,-Diving, Glass bottom boat, snorkeling equipment\"', 1, '2018-01-09 04:25:23', '0000-00-00 00:00:00', 1726, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tour_trip_image`
--

CREATE TABLE `tour_trip_image` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_tour_trip` int(11) UNSIGNED NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tour_trip_image`
--

INSERT INTO `tour_trip_image` (`id`, `id_tour_trip`, `image`, `caption`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(7, 3, 'uploads/085c9d6c94f25d1fe3c2c57d58fb4e6e.jpg', '', '2018-01-08 04:51:56', '0000-00-00 00:00:00', 0, 1726),
(9, 5, 'uploads/6c16ec9ba694062cd8e45c27d6d8b5e2.jpg', '', '2018-01-09 03:19:37', '0000-00-00 00:00:00', 0, 1726),
(10, 6, 'uploads/b083bb55502e26d5c972b1e118f00a0c.jpg', '', '2018-01-09 03:39:24', '0000-00-00 00:00:00', 0, 1726),
(11, 7, 'uploads/6937b970367ae49250e8a4accc6570be.jpg', '', '2018-01-09 03:59:54', '0000-00-00 00:00:00', 0, 1726),
(12, 8, 'uploads/631cfac9c894a12329fc38c58b2d2314.jpg', '', '2018-01-09 04:18:55', '0000-00-00 00:00:00', 0, 1726),
(13, 9, 'uploads/cf7997001a326797ef730a5a23cea409.jpg', '', '2018-01-09 04:25:22', '0000-00-00 00:00:00', 0, 1726),
(14, 4, 'uploads/fe2042170316451bf7445d9fb79ab115.jpg', '', '2018-01-19 04:00:19', '0000-00-00 00:00:00', 0, 1726);

-- --------------------------------------------------------

--
-- Table structure for table `tour_trip_intenerary`
--

CREATE TABLE `tour_trip_intenerary` (
  `id` int(11) NOT NULL,
  `id_tour_trip` int(11) DEFAULT NULL,
  `hari` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour_trip_intenerary`
--

INSERT INTO `tour_trip_intenerary` (`id`, `id_tour_trip`, `hari`, `description`, `created_at`, `created_by`) VALUES
(6, 3, 1, '<p><font face=\"Arial\"><span style=\"font-size: 13px; white-space: pre-wrap;\">Mataram - City &amp; Shopping Tour (MS,MM)</span></font></p><p><span style=\"font-size: 13px; white-space: pre-wrap; font-family: Arial;\">Tiba di Bandara Lombok. City Tour mengungunjungi: Taman Narmada (dibangun th 1727) terkenal dengan sumber mata airnya yang dipercaya dapat membuat awet muda, Pura Lingsar (dibangun th 1714) terkenal sebagai satu-satunya Pura Hindu di dunia yang menjadi tempat beribadah lintas agama, selanjutnya mengunjungi pusat penjualan oleh-oleh makanan dan mutiara Lombok. Makan siang di Taliwang Irama dengan menu khas Lombok Ayam Taliwang, Makan Malam di Menega Rest, Transfer ke hotel, check in, acara bebas</span><br></p>', '2018-01-08 04:51:53', 1726),
(7, 3, 2, '<p><font face=\"Arial\"><span style=\"font-size: 13px; white-space: pre-wrap;\">Gili Trawangan Tour (MP, MS,MM)</span></font></p><p><span style=\"font-size: 13px; white-space: pre-wrap; font-family: Arial;\">Makan pagi di hotel, Gili Trawangan tour mengunjungi: Bukit Malimbu dengan pemandangan pantainya yang indah dari ketinggian bukit Malimbu, pelabuhan Bangsal untuk menyebrang ke Gili Trawangan (Gili dalam bahasa Sasak berarti pulau keci) dengan menggunakan perahu motor. Tiba di Gili Trawangan acara bebas, optional tour seperti: diving, snorkeling, glass bottom boat, keliling pulau dengan kereta kuda, dsb, tersedia dengan biaya sendiri. Makan siang di Black Penny Resto Gili Trawangan. Balik ke hotel singgah di Bukit Pusuk Pass yang banyak dihuni kera-kera jinak yang menunggu uluran makanan dari pengunjung, Makan malam di Square Rest, Tiba di hotel, acara bebas.\r\n</span><br><span style=\"font-family: Arial; font-size: 13px; white-space: pre-wrap;\">ATAU\r\n\r\nGili Sudak Tour (MP,MS,MM) PULAU PRIBADI\r\n\r\nKita akan pergi menuju daerah Sekotong Lombok Barat untuk menyeberang ke salah satu pulau kecil nan eksotis di Gili Sudak, dimana Anda bisa berenang dan menyelam bercengkerama dengan flora dan fauna laut sekaligus bersantai menikmati makan siang BBQ sea food yang lezat. Dalam perjalanan pulang menuju hotel, Anda akan diajak mengunjungi Pura Pengsong yang dihuni oleh ratusan kera keramat dan menunggu oleh-oleh kacang atau pisang dari Anda. Makan Malam di Square Rest, kembali ke hotel.</span><br></p>', '2018-01-08 04:51:53', 1726),
(8, 3, 3, '<p><font face=\"Arial\"><span style=\"font-size: 13px; white-space: pre-wrap;\">Bumi Gora/Sasak Tour&nbsp;</span></font></p><p><span style=\"font-size: 13px; white-space: pre-wrap; font-family: Arial;\">Full day tour mengunjungi: Desa Banyumulek dengan kerajinan keramik khas Lombok, Desa Sukarare terkenal dengan kerajinan kain tenun, desa tradisional Sasak</span><br></p><p><span style=\"font-size: 13px; white-space: pre-wrap; font-family: Arial;\">untuk melihat dari dekat adat istiadat dan bangunan suku Sasak yang unik, pantai Tanjung Aan Kuta dengan pemandangan pantainya yang indah dan pasir putihnya yang berkelikir seperti merica. Makan Siang di Tastura Rest Kuta kemudian Transfer ke Bandara untuk penerbangan&nbsp; kembali ke Jakarta</span><br></p>', '2018-01-08 04:51:53', 1726),
(9, 4, 1, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 01 : AIRPORT – BANDA ACEH TOUR</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Tiba pagi hari di bandara Sulthan Iskandar Muda Banda Aceh, bertemu dengan perwakilan Selamat Datang di Banda Aceh,. Setelah proses bagasi kita tinggal kan bandara Sulthan Iskandar Muda, kita mengunjungi kuburan massal di desa Siron, dimana terdapat ribuan korban dimakam kan di tempat tersebut , transfer makan siang kerestoran lokal. City tour mengunjungi Museum Tsunami yang banyak menyimpan dokumentasi mengenai peristiwa musibah tsunami, mengunjungi lapangan blang padang yang terdapat replica pesawat pertama Indonesia dan di sana juga terdapat monument - monument ucapan terima kasih kepada Negara - negara yang sudah membantu Aceh pasca tsunami “Monument Thanks To The World”. Mengunjungi kapal PLTD apung yang tersere tombak tsunami sejauh 5 KM dari pantai di desa Punge Blang Cut. Kemudian mengunjungi Masjid Banturrahman yang merupakan masjid kebanggaan masyarat akan aceh dan terletak di jantung kota Banda aceh. Check in dihotel dan makan malam disajikan di local restoran, malam acara bebas. { Lunch, Dinner}.</span><br></p>', '2018-01-09 02:43:42', 1726),
(10, 4, 2, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 02:  BANDA ACEH TOUR</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi dihotel. Selesai sarapan pagi city tour mengunjungi kapal nelayan yang tersangkut di atas rumah di desa Lampulo, dimana di kapal tersebut ada puluhan orang yang selamat dari bencana tsunami. Mengunjungi  Rumoh Aceh yaitu rumah adat Aceh, Museum Aceh, lonceng cakradonya yang merupakan pemberian dari dinasti cheng hou pada masa kerajaan Sulthan Iskandar Muda. Kemudian mengunjungi Kuburan Raja Sulthan Iskandar Muda, makan siang di lokal restoran. Kemudian kita mengunjungi Taman Putro Phang dan Gunongan yang merupakan bangunan yang di hadiah kanoleh sultan iskandar muda kepada permaisurinya Putri Pahang (Putroe Phang), mengunjungi kuburan massal di Ulhee Lheu disini juga ter dapan ribuan korban tsunami yang dimakamkan, kemudian mengunjungi Masjid Baiturrahim yang selamat dari terjangan tsunami, meskipun bangunan di sekitar masjid tidak ada yang selamat namun masjid ini masih tetap berdiri kokoh sampai sekarang, mengunjungi pantai lampuuk untuk menyaksikan sunset (bila cuaca bagus ), shopping di desa lamisang dan mengunjungi rumah cut yak dien.  Kembali Ke Kota Banda Aceh, Shopping jajanan khas Aceh di pasara ceh dantoko-soko souvenir. kembalike hotel dan makan malam disajikan di restoran lokal. Malam acara bebas. { breakfast, Luch, Dinner }</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></p>', '2018-01-09 02:43:42', 1726),
(11, 4, 3, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 03 :  BANDA ACEH TOUR - AIRPORT</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi dihotel. Selesai proses check out dari hotel, kemudian transfer ke bandara Sulthan Iskandar Muda untuk melanjutkan perjalanan selanjutnya, tour selesai. { breakfast }</span><br></p>', '2018-01-09 02:43:42', 1726),
(12, 5, 3, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 03 :  BANDA ACEH TOUR - AIRPORT</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi dihotel. Selesai proses check out dari hotel, kemudian transfer ke bandara Sulthan Iskandar Muda untuk melanjutkan perjalanan selanjutnya, tour selesai. { breakfast }</span><br></p>', '2018-01-09 03:19:36', 1726),
(13, 5, 1, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 01 : AIRPORT – BANDA ACEH TOUR</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Tiba pagi hari di bandara Sulthan Iskandar Muda Banda Aceh, bertemu dengan perwakilan Selamat Datang di Banda Aceh,. Setelah proses bagasi kita tinggal kan bandara Sulthan Iskandar Muda, kita mengunjungi kuburan massal di desa Siron, dimana terdapat ribuan korban dimakam kan di tempat tersebut , transfer makan siang kerestoran lokal. City tour mengunjungi Museum Tsunami yang banyak menyimpan dokumentasi mengenai peristiwa musibah tsunami, mengunjungi lapangan blang padang yang terdapat replica pesawat pertama Indonesia dan di sana juga terdapat monument - monument ucapan terima kasih kepada Negara - negara yang sudah membantu Aceh pasca tsunami “Monument Thanks To The World”. Mengunjungi kapal PLTD apung yang tersere tombak tsunami sejauh 5 KM dari pantai di desa Punge Blang Cut. Kemudian mengunjungi Masjid Banturrahman yang merupakan masjid kebanggaan masyarat akan aceh dan terletak di jantung kota Banda aceh. Check in dihotel dan makan malam disajikan di local restoran, malam acara bebas. { Lunch, Dinner}</span><br></p>', '2018-01-09 03:19:36', 1726),
(14, 5, 2, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 02:  BANDA ACEH TOUR</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi dihotel. Selesai sarapan pagi city tour mengunjungi kapal nelayan yang tersangkut di atas rumah di desa Lampulo, dimana di kapal tersebut ada puluhan orang yang selamat dari bencana tsunami. Mengunjungi  Rumoh Aceh yaitu rumah adat Aceh, Museum Aceh, lonceng cakradonya yang merupakan pemberian dari dinasti cheng hou pada masa kerajaan Sulthan Iskandar Muda. Kemudian mengunjungi Kuburan Raja Sulthan Iskandar Muda, makan siang di lokal restoran. Kemudian kita mengunjungi Taman Putro Phang dan Gunongan yang merupakan bangunan yang di hadiah kanoleh sultan iskandar muda kepada permaisurinya Putri Pahang (Putroe Phang), mengunjungi kuburan massal di Ulhee Lheu disini juga ter dapan ribuan korban tsunami yang dimakamkan, kemudian mengunjungi Masjid Baiturrahim yang selamat dari terjangan tsunami, meskipun bangunan di sekitar masjid tidak ada yang selamat namun masjid ini masih tetap berdiri kokoh sampai sekarang, mengunjungi pantai lampuuk untuk menyaksikan sunset (bila cuaca bagus ), shopping di desa lamisang dan mengunjungi rumah cut yak dien.  Kembali Ke Kota Banda Aceh, Shopping jajanan khas Aceh di pasara ceh dantoko-soko souvenir. kembalike hotel dan makan malam disajikan di restoran lokal. Malam acara bebas. { breakfast, Luch, Dinner }</span><br></p>', '2018-01-09 03:19:36', 1726),
(15, 6, 1, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 1 :   Tokyo (D)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Berkumpul di bandara international Soekarno Hatta dan memulai perjalanan dengan</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">penerbangan International menuju Tokyo International Airport, Jepang.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Tiba  at Haneda airport Bertemu dengan guide dan melanjutkan perjalanan menuju Hotel</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Tiba di Hotel (Tokyo) check in dan istirahat</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></p><p><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 2 :   Tokyo - Mt.Fuji - Oshinohakkai - Yamanakako-lake - Shizuoka - Hamanako (L/D)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi kemudian Mengunjungi Gotenba Premium Outlet  Mal        Makan siang dan Sholat Dzuhur, kemudian Mengunjungi Yokohama China Town, Kemudian mengunjungi Yamashita Park dan Yokohama Red Brick Warehouse. Setelah selesai tour tranfer ke hotel untuk istirahat di Chiba or Tokyo</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></p><p><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 3 : Hamanako - Kyoto - Kariya (B/L/D)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi kemudian Mengunjungi Daiba Park</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan siang dan Sholat Dzuhur Kemudian mengunjungi Ginza dan Imperial Palace</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Setelah selesai tour tranfer ke hotel untuk istirahat di Chiba or Tokyo</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari 4 :   Kariya - Mihonomatsubara - Gotenba premium outlet mall - Atsugi (B/L/D)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi kemudian Mengunjungi Akibahara salah satu tempat belanja yang berada di sekitar setasiun Akibahara, kemudian Makan siang dan Sholat dzuhur. Kemudian mengunjungi Asasuka</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Melanjutkan dengan Shopping di Shinjyuku tranfer ke hotel untuk istirahat di      Check in hotel di Chiba or Tokyo</span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b>Hari 5 :   Tokyo - Akihabara - Asakusa - Ginza - Haneda (B/L/D)</b></span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan kemudian transfer to airport Haneda</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">melanjutkan perjalanan dari Haneda menuju Jakarta</span><br></p>', '2018-01-09 03:39:21', 1726),
(16, 7, 1, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 1 :  Departure from Jakarta Soekarno Hatta Airport</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Berkumpul di bandara Soekarno Hatta Terminal 2D pada waktu dan tempat yang telah ditentukan untuk terbang dengan Etihad Airways menuju Istanbul dengan flight EY 471 departure jam 0015.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 2 : ARRIVAL ISTANBUL – BURSA ETA 12.40</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Setibanya di Istanbul diantar untuk makan siang dan kemudian melanjutkan kunjungan ke Blue Mosque , Masjid yang paling terkenal dan bersejarah di Istanbul dan Hippodrome Square, alun-alun bekas tempat pacuan kuda pada jaman Romawi. Kemudian perjalanan dilanjutkan menuju Kota Bursa. Setibanya di Bursa langsung melakukan check in hotel untuk kemudian menikmati makan malam di hotel dan beristirahat.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 3 : BURSA – KUSADASI (MP/MS/MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pagi hari Anda akan diajak mengunjungi Mt. Uludag untuk menikmati dan bermain dengan salju. Yang akan di lanjutkan dengan Bursa city Tour mengunjungi Grand Mosque, Green Mosque &amp; Green Tomb serta berbelanja di Silk Bazaar. Setelah itu perjalanan dilannjutkan menuju Kota Kusadasi untuk bermalam di sana.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 4: KUSADASI – EPHESUS – PAMUKKALE (MP/MS/MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pagi hari Anda akan diajak mengunjungi kota kuno Ephesus yang merupakan situs arkeologi Ibukota Romawi terbesar di Timur Asia. Kunjungan dilanjutkan dengan  melihat House of Virgin Mary, yang dipercaya umat Katolik sebagai tempat dimana bunda Maria tinggal dan menghabiskan masa hidupny. Setelahnya menuju reruntuhan kota kuno Hierapolis pada jaman Romawi untuk melihat keajaiban alam Cotton Castle yang merupakan terasering yang terbuat dari hasil endapan aliran air belerang dan batu kapur dan Cleopatra\'s Pool dimana dipercaya Cleopatra pernah menghabiskan waktunya disini. Kemudian bermalam di Pamukkale.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 5 : PAMUKKALE – KONYA – CAPPADOCIA (MP/MS/MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Perjalanan hari ini menuju Cappadocia, dengan melewati kota Konya untuk mengunjung Mevlana Mousoleum, tempat asal muasal aliran Sufi. Selanjutnya Anda akan berkesempatan mengunjungi  Sultanhani Caravanserai yang dahulu merupakan tempat persinggahan para Pedagang Silk Route dan merupakan konsep hotel pertama di dunia. Setelahnya Anda menuju Cappadocia untuk bermalam.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 6: CAPPADOCIA (MP/MS/MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pagi hari ini, Anda berkesempatan untuk menikmati keindahan Cappadocia dengan menggunakan Hot Air Baloon (OPTIONAL, dengan tambahan biaya) City Tour hari ini Anda akan melihat Underground city of Saratli yang merupakan salah satu kota bawah tanah di Cappadocia, Goreme Open - Air Museum, Pigeon Valley dan Uchisar Village. Kemudian bila waktu memungkinkan, Anda akan diajak untuk mengunjungi tempat pembuatan Karpet, Batu Turquoise dan Keramik yang merupakan oleh - oleh khas Turki. Malam hari akan menyaksikan Belly Dance Show di restaurant tradisional . Kembali ke hotel untuk beristirahat.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 7 : CAPPADOCIA – ANKARA (MP/MS/MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hari ini menuju Ankara. Setibanya disana Anda akan diajak untuk mengunjungi Ataturk Mausoleum yang merupakan tempat dimakamkan Presiden Republik Turki  dan bermalam di Ankara setelah mengunjungi Ankara Castle.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 8 : ANKARA – ISTANBUL (MP/MS/MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pagi ini Anda akan langsung kembali ke kota Istanbul untuk Kemudian dilanjutkan mengunjungi Hagia Sophia, bangunan yang terkenal dengan keindahan dan sejarah uniknya. Akhir program kita akan mengunjungi Topkapi Palace yang merupakan bekas istana Kesultanan Ottoman dengan museum bersejarah yang menyimpan benda-benda peninggalan para nabi. Acara diakhiri dengan berbelanja di Grand Bazaar.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">DAY 9 : ISTANBUL DEPARTURE ETD 1415</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Setelah makan pagi , peserta akan di ajak berlayar di Bosphorus, selat yang membatasi benua Asia dan benua Eropa untuk kemudian di antar ke airport untuk check in pada penerbangan menuju Jakarta via Abu Dhabi. (tersedia meal voucher untuk dinner pada saat transit di Abu Dhabi Airport).</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">DAY 10 : ARRIVAL JAKARTA ETA 1500</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Selamat tiba kembali di Jakarta untuk kembali berkumpul bersama keluarga. Sampai berjumpa kembali di acara tour berikutnya.</span><br></p>', '2018-01-09 03:59:53', 1726),
(17, 8, 1, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 01 : AIRPORT - PADANG CITY TOUR - BUKITTINGGI ( - / L / D )</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Jam 7.30 rombongan dijemputdi airport, langsun gmenuju Padang.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan pagi ( optional ).</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Padang city tour : Padang Kota Tua ( Pasar Mudik dan stasiun KA Pulau Air), Jembatan Siti Nurbaya, Gunung Padang, Pelabuhan Lama Muaro.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pantai Air Manis : legenda Batu si Malin Kundang, Optional ± Rp. 60.000,-/orang, peserta akan di transfer dengan mobil kecil sebab lokasi wisata tidak bisa di lalui bus.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Belanja oleh - oleh keripik di Shirley / Christine Hakim =&gt; optional</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan siang dilokal rest / ikan bakar di pinggir pantai Padang atau es duren + soto / gado2 Padang / bisa request.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Langsung menuju Bukit tinggi melalui ...</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Air Terjun Lembah Anai, terletak di kawasan hutan lindung dan jalan lintas Padang - Bukittinggi, singga huntuk photo shoot.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Padang Panjang- mengunjungi Minang Village (Pusat Dokumentasi &amp; Informasi Kebudayaan Minangkabau - PDIKM) - local guide, bisa berfoto menggunakan Pakaian Perkawinan Adat Minangkabau =&gt; tutup jam 4 sore.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Desa Pandai Sikek  : pusat tenunan kain Songket dan ukiran kayu.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Koto Baru : singgah mencicipi kuebika Talago / Simariana sambil menikmati panorama Gunung Marapi.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Jam Gadang, Pasar Atas.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan malam dilokal rest / Gon Raya Lamo ( Ayam pop ) / bisa request.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Check-in hotel Bunda -</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Acara beb</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></p><p><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 02 : BUKITTIGGI - PAYAHKUMBUH / LEMBAH HARAU - KELOK 9 - BATUSANGKAR - BUKITTINGGI ( B / L / D ).</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan di hotel, tour dimulai jam 8.00 pagi, langsung menuju ...</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Ummi Ulfa : belanja oleh2 makanan khas Bukittinggi.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Lembah Harau, di Payah kumbuh untuk menikmati keindahan bukit kapur / cadas dan air terjun( di musim hujan ) serta titik echooo.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Kelok9 : Fly over, jembatan penghubung di lembah nan indah menuju PekanBaru.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Desa Tabek Patah, tempat pembuatan kopi kawa daun dan keripik pisang salaiKiniko.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan siang di lokal rest / Pangek Situjuah / Pondok Flora / Aroma.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Batu sangkar untuk melihat Istana Raja Basa Pagarruyung.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan malam di lokal rest / RM. Kawali / Wong Semarang / bisa request.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Check in hotel.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></p><p><b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Day 03 : BUKITTINGGI CITY TOUR  – MANINJAU TOUR - AIRPORT( B / L / - ).</span></b></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Sarapan dan check out hotel, tour dimulai jam 8.00 pagi langsung menuju ...</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Bukit tinggi city tour : Taman Panorama Ngarai Sianok, Lobang Jepang, melihatGreat Wall / Janjang Koto Gadang.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Panorama Perkampungan Sungai Landia yang terkenal dengan kerajinan tangan dari kulit manis.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Desa Matur yang terkenal dengan kacang goreng yang khas, kerupuk dan es cendol dari labu kuning.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Lawang Park melihat Panorama Danau Maninjau</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan siang dilokal rest / RM. KelokSikabu / Sate MakSyukur / bisa request.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Menuju airport dan Check in ( keberangkatan jam 19.00 ).</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Tour berakhir. See you again next time ! :-)</span></p>', '2018-01-09 04:18:54', 1726),
(18, 9, 1, '<p><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">D1: Mataram - City &amp; Shopping Tour (MS,MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Tiba di Bandara Lombok. City Tour mengungunjungi: Taman Narmada (dibangun th 1727) terkenal dengan sumber mata airnya yang dipercaya dapat membuat awet muda, Pura Lingsar (dibangun th 1714) terkenal sebagai satu-satunya Pura Hindu di dunia yang menjadi tempat beribadah lintas agama, selanjutnya mengunjungi pusat penjualan oleh-oleh makanan dan mutiara Lombok. Makan siang di Taliwang Irama dengan menu khas Lombok Ayam Taliwang, Makan Malam di Menega Rest, Transfer ke hotel, check in, acara bebas</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">D2: Gili Trawangan Tour (MP, MS,MM)</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Makan pagi di hotel, Gili Trawangan tour mengunjungi: Bukit Malimbu dengan pemandangan pantainya yang indah dari ketinggian bukit Malimbu, pelabuhan Bangsal untuk menyebrang ke Gili Trawangan (Gili dalam bahasa Sasak berarti pulau keci) dengan menggunakan perahu motor. Tiba di Gili Trawangan acara bebas, optional tour seperti: diving, snorkeling, glass bottom boat, keliling pulau dengan kereta kuda, dsb, tersedia dengan biaya sendiri. Makan siang di Black Penny Resto Gili Trawangan. Balik ke hotel singgah di Bukit Pusuk Pass yang banyak dihuni kera-kera jinak yang menunggu uluran makanan dari pengunjung, Makan malam di Square Rest, Tiba di hotel, acara bebas.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">ATAU</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">D2: Gili Sudak Tour (MP,MS,MM) PULAU PRIBADI</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Kita akan pergi menuju daerah Sekotong Lombok Barat untuk menyeberang ke salah satu pulau kecil nan eksotis di Gili Sudak, dimana Anda bisa berenang dan menyelam bercengkerama dengan flora dan fauna laut sekaligus bersantai menikmati makan siang BBQ sea food yang lezat. Dalam perjalanan pulang menuju hotel, Anda akan diajak mengunjungi Pura Pengsong yang dihuni oleh ratusan kera keramat dan menunggu oleh-oleh kacang atau pisang dari Anda. Makan Malam di Square Rest, kembali ke hotel.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">D3: Bumi Gora/Sasak Tour </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"></b><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Full day tour mengunjungi: Desa Banyumulek dengan kerajinan keramik khas Lombok, Desa Sukarare terkenal dengan kerajinan kain tenun, desa tradisional Sasak</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">untuk melihat dari dekat adat istiadat dan bangunan suku Sasak yang unik, pantai Tanjung Aan Kuta dengan pemandangan pantainya yang indah dan pasir putihnya yang berkelikir seperti merica. Makan Siang di Tastura Rest Kuta kemudian Transfer ke Bandara untuk penerbangan  kembali ke Jakarta</span><br></p>', '2018-01-09 04:25:19', 1726);

-- --------------------------------------------------------

--
-- Table structure for table `tour_umrah`
--

CREATE TABLE `tour_umrah` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_hotel` int(11) UNSIGNED NOT NULL,
  `id_airline` int(11) UNSIGNED NOT NULL,
  `category_umrah` varchar(11) COLLATE utf8_unicode_ci NOT NULL COMMENT '1 = Reguler, 2 = Reguler + Ramadhan, 3 = Reguler VIP, 4 = Umrah Plus, 5 = Premium, 6 = Backpacker, 7 = Backpacker + Ramadhan',
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `depature_date` date NOT NULL,
  `return_date` date NOT NULL,
  `price` double NOT NULL,
  `price_reteal` double NOT NULL,
  `komisi` double NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `facilities_include` text COLLATE utf8_unicode_ci NOT NULL,
  `facilities_exclude` text COLLATE utf8_unicode_ci NOT NULL,
  `tokoh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tour_umrah`
--

INSERT INTO `tour_umrah` (`id`, `id_hotel`, `id_airline`, `category_umrah`, `title`, `depature_date`, `return_date`, `price`, `price_reteal`, `komisi`, `description`, `facilities_include`, `facilities_exclude`, `tokoh`, `visible`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3, 1, 1, '1', 'Umroh Paket Coklat 9 Hari', '2018-03-25', '2018-04-03', 23500000, 24000000, 500000, '<p style=\"box-sizing: inherit; margin-bottom: 1em; line-height: 1.4285em; color: rgba(0, 0, 0, 0.87); font-family: &quot;PT Sans&quot;, sans-serif; text-align: justify;\">Paket Umroh murah dari travel umroh&nbsp;<span style=\"box-sizing: inherit; font-weight: 700;\">PATUNA Travel</span>&nbsp;untuk keberangkatan&nbsp;<span style=\"box-sizing: inherit; font-weight: 700;\">25 Maret 2018</span>, biaya umroh mulai dari Rp 23.500.000.</p><p style=\"box-sizing: inherit; margin-bottom: 1em; line-height: 1.4285em; color: rgba(0, 0, 0, 0.87); font-family: &quot;PT Sans&quot;, sans-serif; text-align: justify;\">Menggunakan maskapai penerbangan&nbsp;<span style=\"box-sizing: inherit; font-weight: 700;\">Qatar Airways</span>. Maskapai yang digunakan adalah Qatar Airways, maskapai ini dinilai 5 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, kelas Bisnis dan kelas Utama (bergantung ketersediaan). Qatar Airways menyediakan berbagai kenyamanan di dalam penerbangan, seperti: koneksi internet, hiburan multi media, menu makanan dengan cita rasa timur tengah, serta minuman panas dan dingin..</p><p style=\"box-sizing: inherit; margin-bottom: 1em; line-height: 1.4285em; color: rgba(0, 0, 0, 0.87); font-family: &quot;PT Sans&quot;, sans-serif; text-align: justify;\">Hotel di mekkah yang disediakan adalah Dar Al Eiman Grand dengan fasilitas Dar Al Eiman Grand terletak di Jalan Ibrahim Al Khalil yang ramai di Mekkah, hanya 750 meter dari Masjidil Haram yang suci. Akomodasi ini memiliki kamar-kamar yang elegan dan sebuah restoran yang menawarkan layanan kamar 24 jam. Semua kamar di Dar Al Eiman Grand memiliki lantai keramik dan berkarpet biru, serta dihias dengan kain yang mewah dan perabotan ornamen. Semuanya dilengkapi dengan TV satelit, AC, dan lemari. Sejumlah restoran dan toko dapat dicapai dengan berjalan kaki dari Dar Al Eiman. Bandara King Abdul Aziz berjarak 60 menit perjalanan dengan mobil. .<br style=\"box-sizing: inherit;\"></p><p style=\"box-sizing: inherit; margin-bottom: 1em; line-height: 1.4285em; color: rgba(0, 0, 0, 0.87); font-family: &quot;PT Sans&quot;, sans-serif; text-align: justify;\">Hotel di madinah yang disediakan adalah Royal Inn Nozol dengan fasilitas Nozol Royal Inn Hotel berjarak 100 meter dari Masjid Nabawi, hanya 10 menit berkendara dari stasiun umum. Hotel ini juga memiliki restoran dengan layanan kamar 24-jam. Tersedia juga akses Wi-Fi gratis. Kamar-kamar Nozol Royal Inn Hotel yang luas menawarkan AC, minibar, dan TV layar datar. Setiap kamar memiliki kamar mandi dalam dengan pengering rambut dan produk mandi pilihan. Staf meja depan 24-jam Nozol Royal Inn Hotel dapat menyediakan layanan kamar dan mengurus permintaan binatu maupun setrika. Bandara Prince Mohamed Bin Abdul Aziz hanya berjarak 25 menit berkendara dari hotel. Ini adalah kawasan favorit tamu kami di Madinah, menurut ulasan independen. Akomodasi ini juga dinilai memiliki harga terbaik di Madinah! Tamu mendapatkan fasilitas lebih banyak untuk uang yang mereka keluarkan dibandingkan akomodasi lain di kota ini..<br style=\"box-sizing: inherit;\"></p><p style=\"box-sizing: inherit; margin-bottom: 0px; line-height: 1.4285em; color: rgba(0, 0, 0, 0.87); font-family: &quot;PT Sans&quot;, sans-serif; text-align: justify;\">Pastikan Anda memilih paket umroh dari PATUNA Travel yang menjamin kenyamanan ibadah Anda.</p>', '\"Visa Umroh,Makan 3 Kali,Sehari Air Zam-Zam ,Tiket Pesawat PP,Akomodasi Sesuai Paket\"', '\"Passport,Suntik Meningitis ,Pengeluaran Pribadi ,Tiket Domestik, Tambah Nama Menjadi 3 Kata\"', '', 1, '2018-01-03 08:43:40', '0000-00-00 00:00:00', 1720, 0),
(5, 1, 1, '3', 'Umroh Paket Coklat', '2018-03-25', '2018-04-03', 23250000, 24000000, 750000, '<p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style></p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Paket Umroh murah dari travel umroh <b>PATUNA </b>Travel untuk keberangkatan 25 Maret 2018, biaya umroh mulai dari Rp 24.000.000.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Menggunakan maskapai penerbangan Qatar Airways. Maskapai yang digunakan adalah Qatar Airways, maskapai ini dinilai 5 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, kelas Bisnis dan kelas Utama (bergantung ketersediaan). Qatar Airways menyediakan berbagai kenyamanan di dalam penerbangan, seperti: koneksi internet, hiburan multi media, menu makanan dengan cita rasa timur tengah, serta minuman panas dan dingin.</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel Madinah&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel Madinah :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Royal Inn Nozol/*5</span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b> </b></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><b>Hotel Mekah : </b></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Dar Al Eiman Grand/*4</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></p>', '\"Makanan,Visa,tiket pesawat pp,Akomodasi sesuai paket,Air Zam-zam\"', '\"Passport,Suntik Meningitis,Tiket Domestik,Tambah Nama 3 kata\"', '', 1, '2018-01-09 02:26:19', '0000-00-00 00:00:00', 1723, 0),
(7, 1, 1, '4', 'Umroh Plus Turki 13 Hari', '2018-02-05', '2018-02-14', 23500000, 24500000, 1000000, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Paket Umroh murah dari travel umroh ARMINAREKA untuk keberangkatan 5 Februari 2018, biaya umroh mulai dari Rp 245000.000.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Menggunakan maskapai penerbangan Turkish Airlines. Maskapai yang digunakan adalah Turkish Airlines, maskapai ini dinilai 4 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, kelas Bisnis dan kelas Utama (bergantung ketersediaan). Turkish Airlines menyediakan berbagai kenyamanan di dalam penerbangan, seperti: hiburan multimedia dan berbagai menu makanan dengan standar halal yang dapat dipesan 24 jam sebelum penerbangan..</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hotel di mekkah yang disediakan adalah Grand Zam zam dengan fasilitas Grand Zam zam Makkah menawarkan akomodasi di Mekkah, dengan menyediakan Wi-Fi gratis di seluruh areanya. Hotel ini memiliki restoran dan menyediakan parkir pribadi gratis di lokasinya. Setiap kamarnya dilengkapi AC dan TV. Terdapat juga ketel di kamar. Masing-masing kamar menyediakan kamar mandi pribadi. Tersedia juga mantel mandi dan perlengkapan mandi gratis untuk kenyamanan Anda. Hotel ini menawarkan layanan antar-jemput gratis selama musim Umrah..</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hotel di madinah yang disediakan adalah Movenpick dengan fasilitas Movenpick Hotel berjarak 100 meter dari Masjid Nabawi, hanya 10 menit berkendara dari stasiun umum. Hotel ini juga memiliki restoran dengan layanan kamar 24-jam. Tersedia juga akses Wi-Fi gratis. Kamar-kamar Movenpick Hotel yang luas menawarkan AC, minibar, dan TV layar datar. Setiap kamar memiliki kamar mandi dalam dengan pengering rambut dan produk mandi pilihan. Staf meja depan 24-jam Movenpick Hotel dapat menyediakan layanan kamar dan mengurus permintaan binatu maupun setrika. Bandara Prince Mohamed Bin Abdul Aziz hanya berjarak 25 menit berkendara dari hotel. Ini adalah kawasan favorit tamu kami di Madinah, menurut ulasan independen. Akomodasi ini juga dinilai memiliki harga terbaik di Madinah! Tamu mendapatkan fasilitas lebih banyak untuk uang yang mereka keluarkan dibandingkan akomodasi lain di kota ini..</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pastikan Anda memilih paket umroh dari ARMINAREKA yang menjamin kenyamanan ibadah Anda.</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Maskapai&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Maskapai :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Turkish Airlines</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel Madinah&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel Madinah :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Grand Zam zam/setaraf*5</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><b><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel Mekah&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:{&quot;1&quot;:0},&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:0}\">Hotel Mekah :&nbsp;</span></b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Movenpick Hotel/setaraf*5</span><b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></b><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></p>', '\"Koper,Kain,Baju Batik,Slempang,Tas Umroh\"', '\"Biaya Airport Tax and Handling,Biaya Pembuatan Passport dan Suntik Maningitis, dokumen perjalanan lainnya,Pengeluaran pribadi spt laundry, telepon, tips,Tour-tour\\/ acara diluar program atas permintaan sendiri,Kelebihan bagasi (overweight)\"', '', 1, '2018-01-09 04:51:26', '0000-00-00 00:00:00', 1729, 0),
(8, 1, 1, '3', 'Umroh Akbar UYM (Riyadhoh)', '2018-03-28', '2018-04-05', 24700000, 25500000, 800000, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Paket Umroh murah dari travel umroh Daqu Travel untuk keberangkatan 28 Maret 2018, biaya umroh mulai dari Rp 25.500.000.</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Menggunakan maskapai penerbangan Saudi Airways. Maskapai yang digunakan adalah Saudi Airways, maskapai ini dinilai 5 oleh SKYTRAX, tersedia layanan untuk kelas Ekonomi, kelas Bisnis dan kelas Utama (bergantung ketersediaan). Saudi Airways menyediakan berbagai kenyamanan di dalam penerbangan, seperti: koneksi internet, hiburan multi media, menu makanan dengan cita rasa timur tengah, serta minuman panas dan dingin..</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hotel di mekkah yang disediakan adalah Shafwa Orchid dengan fasilitas Shafwa Orchid terletak di Jalan yang ramai di Mekkah, hanya 750 meter dari Masjidil Haram yang suci. Akomodasi ini memiliki kamar-kamar yang elegan dan sebuah restoran yang menawarkan layanan kamar 24 jam. </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Hotel di madinah yang disediakan adalah Mirage Salam dengan fasilitas Hotel berjarak 100 meter dari Masjid Nabawi, hanya 10 menit berkendara dari stasiun umum. Hotel ini juga memiliki restoran dengan layanan kamar 24-jam. Tersedia juga akses Wi-Fi gratis. .</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Pastikan Anda memilih paket umroh dari Daqu Travel yang menjamin kenyamanan ibadah Anda.</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Maskapai&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Maskapai :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Saudi Arabia++/ Setaraf</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel Madinah&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel Madinah :&nbsp;</span><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Mirage Salam/setaraf*3&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:8401791,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;26&quot;:400}\">Mirage Salam/setaraf*3</span></p><p><span style=\"font-size: 10pt; font-family: Arial;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Mirage Salam/setaraf*3&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:8401791,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,6989903],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;26&quot;:400}\"><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel Mekah&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel Mekah :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Shafwa Orchid</span><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></p>', '\"Koper,Kain,Baju Batik,Slempang,Tas Umrah\"', '\"Biaya Airport Tax and Handling ,Biaya Pembuatan Passport dan Suntik Maningitis, dokumen perjalanan lainnya,Pengeluaran pribadi spt laundry, telepon, tips,Tour-tour\\/ acara diluar program atas permintaan sendiri,Kelebihan bagasi (overweight)\"', '', 1, '2018-01-09 04:58:59', '0000-00-00 00:00:00', 1728, 0),
(9, 1, 1, '3', 'Paket Umroh Platinum', '2018-02-19', '2018-02-28', 46001500, 47001500, 1000000, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Paket Umroh Platinum Februari 2018</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Platinum Triple $ 3.500 + Perlengkapan 3.000.000</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Platinum Double $ 3.800 + Perlengkapan 3.000.000</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Anak dibawah 5 tahun $ 2.200</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Keberangkatan tanggal 19 Februari 2018 dengan jumlah Quota sebanyak 50 orang</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Manasik dan ramah tamah bersama Ustadz Khalid Basalamah</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Insya Allah keberangkatan bersama Ustadz Khalid Basalamah</span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Keberangkatan menggunakan pesawat Garuda Indonesia</span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Maskapai&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Maskapai :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Saudi Arabia++/ Setaraf</span></p><p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><span style=\"font-size: 10pt; font-family: Arial; font-weight: bold;\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;Hotel Madinah&quot;}\" data-sheets-userformat=\"{&quot;2&quot;:29695,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,13228792],&quot;5&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;6&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;7&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;8&quot;:{&quot;1&quot;:[{&quot;1&quot;:2,&quot;2&quot;:0,&quot;5&quot;:[null,2,0]},{&quot;1&quot;:0,&quot;2&quot;:0,&quot;3&quot;:3},{&quot;1&quot;:1,&quot;2&quot;:0,&quot;4&quot;:1}]},&quot;9&quot;:0,&quot;10&quot;:1,&quot;11&quot;:3,&quot;12&quot;:0,&quot;15&quot;:&quot;Arial&quot;,&quot;16&quot;:10,&quot;17&quot;:1}\">Hotel Madinah :&nbsp;</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Grand Zam zam/setaraf*5</span></p><style type=\"text/css\"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style><p><span style=\"font-family: Arial; font-size: 10pt; font-weight: bold;\">Hotel Mekah</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"> : </span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">Movenpick Hotel/setaraf*5</span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span></p><p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><br></span><br></p>', '\"Koper,Kain,Baju Batik,Slempang,Tas Umrah\"', '\"Biaya Airport Tax and Handling,Biaya Pembuatan Passport dan Suntik Maningitis, dokumen perjalanan lainnya,Pengeluaran pribadi spt laundry, telepon, tips,Tour-tour\\/ acara diluar program atas permintaan sendiri,Kelebihan bagasi (overweight)\"', '', 1, '2018-01-09 05:05:22', '0000-00-00 00:00:00', 1727, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tour_umrah_image`
--

CREATE TABLE `tour_umrah_image` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_tour_umrah` int(11) UNSIGNED NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tour_umrah_image`
--

INSERT INTO `tour_umrah_image` (`id`, `id_tour_umrah`, `image`, `caption`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(2, 3, 'uploads/9355110ece1bb1582009ca96eb8e852d.jpg', '', '2018-01-03 08:43:40', '0000-00-00 00:00:00', 0, 1720),
(3, 3, 'uploads/aa705e6eada577a61b2badbdf5a1a929.jpg', '', '2018-01-03 08:43:40', '0000-00-00 00:00:00', 0, 1720),
(4, 4, 'uploads/8cdd16661c7b3a309fbd0c858ee95f61.jpg', '', '2018-01-08 03:00:23', '0000-00-00 00:00:00', 0, 1724),
(5, 5, 'uploads/3a8cc28a861390b0c5cee627e11d1e3c.jpg', '', '2018-01-09 02:26:20', '0000-00-00 00:00:00', 0, 1723),
(6, 5, 'uploads/45bbf8ca5bdbf21886401c14f280f8e4.jpg', '', '2018-01-09 02:35:42', '0000-00-00 00:00:00', 0, 1730),
(7, 7, 'uploads/cb82c6ca6fc23876d1bfd9ad758c911d.jpg', '', '2018-01-09 04:51:27', '0000-00-00 00:00:00', 0, 1729),
(8, 8, 'uploads/7ff79e0b849034ad9eda02a18bdbdeaa.jpg', '', '2018-01-09 04:59:00', '0000-00-00 00:00:00', 0, 1728),
(9, 9, 'uploads/fc525be4b9b66af9412e48439a0808a2.jpg', '', '2018-01-09 05:05:23', '0000-00-00 00:00:00', 0, 1727);

-- --------------------------------------------------------

--
-- Table structure for table `tour_umrah_intenerary`
--

CREATE TABLE `tour_umrah_intenerary` (
  `id` int(11) NOT NULL,
  `id_tour_umrah` int(11) DEFAULT NULL,
  `hari` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tour_umrah_intenerary`
--

INSERT INTO `tour_umrah_intenerary` (`id`, `id_tour_umrah`, `hari`, `description`, `created_at`, `created_by`) VALUES
(3, 3, 4, '<p><span lang=\"FI\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline; text-align: justify;\">Setelah Sholat Subuh berjamaah di Masjid Nabawi dilanjutkan persiapan check out hotel. Setelah Sholat Dzuhur, perjalanan dilanjutkan menuju Makkah untuk melaksanakan ibadah Umrah, dengan mengambil Miqot di Bir Ali. Setibanya di Makkah, check in hotel,&nbsp;</span><span lang=\"NL\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline; text-align: justify;\">Selanjutnya melaksanakan Umrah (thawaf sa’i dan tahalul).</span><br></p>', '2018-01-03 08:38:01', 1720),
(4, 3, 1, '<p><span lang=\"SV\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">Jamaah&nbsp; berkumpul di Bandara Internasional Soekarno-Hatta, Jakarta &gt; Perjalanan menuju&nbsp;</span><span lang=\"IN\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">Jeddah&nbsp;</span><span lang=\"IN\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">&nbsp;</span><span lang=\"SV\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">&nbsp;Insya Allah tiba di Bandara&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">King&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">A</span><span lang=\"IN\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">bdul&nbsp;</span><span style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">A</span><span lang=\"IN\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">ziz&nbsp;</span><span lang=\"SV\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">Jeddah, proses Keimgrasian&nbsp; dan pengambilan Bagasi (Baggage&nbsp; Claim),&nbsp; setelah itu perjalanan menuju Madinah dengan menggunakan bus. Setibanya di Madinah, &nbsp;&nbsp;check in&nbsp;&nbsp; hotel dan istirahat. Shalat subuh berjama`ah di Masjid Nabawi, dilanjutkan acara bebas untuk memperbanyak ibadah di Mesjid Nabawi</span><br></p>', '2018-01-03 08:38:01', 1720),
(5, 3, 2, '<p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px;\">Shalat subuh berjama`ah di Masjid Nabawi, selesai sarapan pagi jam 7.30 jama`ah berkumpul di Lobby Hotel untuk persiapan Ziarah Pertama di Madinah (Mengunjungi Jabal Baidha/Jabal Magnet dan mengunjungi Percetakan Al Qur`an) kembali ke hotel, Selesai Shalat Ashar Jama`ah diajak berziarah&nbsp;</span><b style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">Musium; Asma `ul Husna, Sirah Nabawi dan Musium Masjid Madinah</b><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px;\">. Selesai Makan Malam 09.00, Jama`ah wanita bersiap-siap untuk diajak berziarah&nbsp;</span><b style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">Raudhah, Maqbarah Rasulullah, Abubakar As Shidiiq dan Umar bin Khattab</b><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px;\">, untuk Jama`ah laki-laki berziarhah Raudhah &amp; Rasulullah, Abubakar dan Umar bin Khattab jam 03.00 dan sekaligus shalat Tahajjud/Qiyamul-Lail sampai shalat subuh berjamaah.</span><br style=\"font-family: Arial; font-size: 13.3333px;\"><br></p>', '2018-01-03 08:38:01', 1720),
(6, 3, 3, '<p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px;\">Shalat subuh berjamaah di Mesjid Nabawi, selesai sarapan pagi 07.30 dilanjutkan Ziarah Kota Madinah mengunjungi&nbsp;</span><b style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\"><i style=\"margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">Mesjid Quba (dalam keadaan berwudhu dan melaksanakan shalat Sunnah min. 2 rakaat), Kebun Kurma, Jabal Uhud,&nbsp; Qiblatain, Khandaq Qur’an, dan Kebun Kurma</i></b><i style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline;\">,</i><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px;\">Kembali ke hotel dan istirahat. Selesai shalat Asar jama`ah diajak berziarah ke Makam Rasulullah, Abubakar As Shidiq, Umar bin Khattab dan Pekuburan Baqi(dari luar) dan bertadabur tentang Rasullulah dan Para Sahabat Rasulullah. Acara bebas memperbanyak ibadah di Masjid Nabawi.</span><br></p>', '2018-01-03 08:38:01', 1720),
(7, 3, 5, '<p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px; text-align: justify;\">Shalat subuh bejamaah di Masjidil Haram, dilanjutkan acara bebas untuk memperbanyak Ibadah di Masjidil Haram.</span><br></p>', '2018-01-03 08:38:02', 1720),
(8, 3, 9, '<p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px; text-align: justify;\">Bertolak menuju Bandara Internasional Soekarno-Hatta, Jakarta Insya Allah&nbsp; tiba kembali di tanah air Indonesia di Bandara Internasional Soekarno-Hatta, Jakarta. acara Ibadah Umrah selesai dan semoga Ibadah Umrah anda diterima Allah SWT dan terima kasih telah bergabung bersama Kami. Sampai Jumpa di Program2 yang akan datang.</span></p><p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px; text-align: justify;\"><br></span></p><p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px; text-align: justify;\"><span style=\"background-color: rgb(255, 255, 0);\"><u>INTENRARY DAPAT BERUBAH SETIAP SAAT</u></span><br></span><br></p>', '2018-01-03 08:38:04', 1720),
(9, 3, 6, '<div class=\"MsoNormal\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline; text-align: justify;\"><span lang=\"FI\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: inherit; vertical-align: baseline;\">Shalat subuh bejamaah di Masjidil Harom. Hari ini jamaah akan kami ajak Ziarah Kota Makkah mengunjungi&nbsp;<b style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">Jabal&nbsp;<i style=\"margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">Tsur, Arafah, Jabal Rahmah, Muzdalifah, Mina, dan Jabal Nur (Gua Hira )</i>.&nbsp;</b>Bagi jamaah yg akan mengambil umrah ke 2, mengambil miqot di<b style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><i style=\"margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">&nbsp;Ji`ronah</i></b>. Kembali ke hotel, istirahat dan melaksanakan ibadah umrah ke 2 (Thowaf, Said an Tahalul).<b style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><u style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><o:p style=\"margin: 0px; padding: 0px;\"></o:p></u></b></span></div><div><span lang=\"FI\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: inherit; vertical-align: baseline;\"><br></span></div>', '2018-01-03 08:38:04', 1720),
(10, 3, 8, '<div class=\"MsoNormal\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline; text-align: justify;\"><span lang=\"FI\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: inherit; vertical-align: baseline;\">Shalat subuh berjamaah di Masjidil Haram. Dilanjutkan&nbsp;<b style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><i style=\"margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">Tawaf Wada`</i></b>, kemudian persiapan check out hotel. Perjalanan dilanjutkan menuju Jeddah.Tiba di Jeddah,&nbsp; City Tour dengan mengunjungi&nbsp;<b style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><i style=\"margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">Laut Merah dengan Mesjid terapungnya, Makam Siti Hawa dan berbelanja di Balad Shoping Centre</i></b>.&nbsp;<b style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><u style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\"><o:p style=\"margin: 0px; padding: 0px;\"></o:p></u></b></span></div><div class=\"MsoNormal\" style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline; text-align: justify;\"><span lang=\"FI\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: inherit; vertical-align: baseline;\">Bertolak menuju&nbsp;</span><span lang=\"SV\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 9pt; line-height: 13.8px; font-family: inherit; vertical-align: baseline;\">Airport King Abdul Aziz jaddah</span></div>', '2018-01-03 08:38:04', 1720),
(11, 3, 7, '<p><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px; text-align: justify;\">Shalat subuh bejamaah di Masjidil Haram, selesai sarapan pagi jam 07.30 berkumpul di Lobby bersiap-siap untuk acara Ziarah kedua/City Tour ke&nbsp;</span><b style=\"margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 12px; line-height: inherit; font-family: Verdana, Geneva, sans-serif; vertical-align: baseline; text-align: justify;\"><i style=\"margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">Perternakan Unta, Hudaibiyah(bisa mengambil Umrah Ketiga bagi yang mau atau hanya melaksanakan shalat sunnah) dan mengunjungi Mathaf/Musium Kiswah, Musium Makkah(situs Ka`bah dan Sumur Zamzam) dan Musium Madinah</i></b><span style=\"font-family: Verdana, Geneva, sans-serif; font-size: 12px; text-align: justify;\">&nbsp;kembali ke hotel dan acara bebas untuk memperbanyak Ibadah di Masjidil Haram.</span><br></p>', '2018-01-03 08:38:04', 1720),
(22, 4, 5, '<p>Ibadah Masjidil Haram</p>', '2018-01-08 03:00:19', 1724),
(23, 4, 6, '<p>Ziarah Makkah (Umrah Kedua)</p>', '2018-01-08 03:00:19', 1724),
(24, 4, 3, '<p>Ziarah Madinah</p>', '2018-01-08 03:00:19', 1724),
(25, 4, 2, '<p>Ziarah Nabawi</p>', '2018-01-08 03:00:19', 1724),
(26, 4, 1, '<p><span style=\"font-family: Arial; font-size: 13px; white-space: pre-wrap;\">Jakarta - Kuala Lumpur - Madinah</span><br></p><p><br></p>', '2018-01-08 03:00:19', 1724),
(27, 4, 7, '<p>Umroh Ketiga (Miqat di Tan\'im)&nbsp;<br></p>', '2018-01-08 03:00:19', 1724),
(28, 4, 9, '<p>Jeddah - Jakarta<br></p>', '2018-01-08 03:00:19', 1724),
(29, 4, 8, '<p>Makkah - Jeddah (masjid terapung)&nbsp;<br></p>', '2018-01-08 03:00:19', 1724),
(30, 7, 0, '', '2018-01-09 04:51:26', 1729),
(31, 8, 1, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 1 : jkt-kl-madinah </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 2 : ziarah nabawi </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 3 : ziarah madinah </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 4 : madinah - makkah </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 5 : ibadah masjidil harom </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 6 : ziarah makkah (umroh kedua) </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 7 : umroh ketiga (miqat di tan\'im) </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 8 : Makkah - Jeddah (masjid terapung) </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 9 : jeddah - jkt</span><br></p>', '2018-01-09 04:58:59', 1728),
(32, 9, 1, '<p><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 1 : jkt-kl-madinah </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 2 : ziarah nabawi </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 3 : ziarah madinah </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 4 : madinah - makkah </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 5 : ibadah masjidil harom </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 6 : ziarah makkah (umroh kedua) </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 7 : umroh ketiga (miqat di tan\'im) </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 8 : Makkah - Jeddah (masjid terapung) </span><br style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\"><span style=\"font-family: arial, sans, sans-serif; font-size: 13px; white-space: pre-wrap;\">hari 9 : jeddah - jkt</span><br></p>', '2018-01-09 05:05:22', 1727);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_resources`
--
ALTER TABLE `acl_resources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `acl_roles`
--
ALTER TABLE `acl_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD PRIMARY KEY (`role_id`,`parent`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD PRIMARY KEY (`role_id`,`resource_id`),
  ADD KEY `resource_id` (`resource_id`);

--
-- Indexes for table `auth_autologin`
--
ALTER TABLE `auth_autologin`
  ADD PRIMARY KEY (`user`,`series`);

--
-- Indexes for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `auth_users_master`
--
ALTER TABLE `auth_users_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_umroh`
--
ALTER TABLE `booking_umroh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `data_pelanggan`
--
ALTER TABLE `data_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_airline`
--
ALTER TABLE `m_airline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_airline_image`
--
ALTER TABLE `m_airline_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_airline_image_id_airline_foreign` (`id_airline`);

--
-- Indexes for table `m_facilities`
--
ALTER TABLE `m_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_hotel`
--
ALTER TABLE `m_hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_trip`
--
ALTER TABLE `tour_trip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_trip_image`
--
ALTER TABLE `tour_trip_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tour_trip_image_id_tour_trip_foreign` (`id_tour_trip`);

--
-- Indexes for table `tour_trip_intenerary`
--
ALTER TABLE `tour_trip_intenerary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_umrah`
--
ALTER TABLE `tour_umrah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_umrah_image`
--
ALTER TABLE `tour_umrah_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tour_umrah_image_id_tour_trip_foreign` (`id_tour_umrah`);

--
-- Indexes for table `tour_umrah_intenerary`
--
ALTER TABLE `tour_umrah_intenerary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_resources`
--
ALTER TABLE `acl_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `acl_roles`
--
ALTER TABLE `acl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `auth_users`
--
ALTER TABLE `auth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1731;
--
-- AUTO_INCREMENT for table `auth_users_master`
--
ALTER TABLE `auth_users_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1703;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;
--
-- AUTO_INCREMENT for table `booking_umroh`
--
ALTER TABLE `booking_umroh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;
--
-- AUTO_INCREMENT for table `data_pelanggan`
--
ALTER TABLE `data_pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `m_airline`
--
ALTER TABLE `m_airline`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_airline_image`
--
ALTER TABLE `m_airline_image`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_facilities`
--
ALTER TABLE `m_facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_hotel`
--
ALTER TABLE `m_hotel`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tour_trip`
--
ALTER TABLE `tour_trip`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tour_trip_image`
--
ALTER TABLE `tour_trip_image`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tour_trip_intenerary`
--
ALTER TABLE `tour_trip_intenerary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tour_umrah`
--
ALTER TABLE `tour_umrah`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tour_umrah_image`
--
ALTER TABLE `tour_umrah_image`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tour_umrah_intenerary`
--
ALTER TABLE `tour_umrah_intenerary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD CONSTRAINT `acl_role_parents_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_role_parents_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD CONSTRAINT `acl_rules_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_rules_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `acl_resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD CONSTRAINT `auth_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `m_airline_image`
--
ALTER TABLE `m_airline_image`
  ADD CONSTRAINT `m_airline_image_id_airline_foreign` FOREIGN KEY (`id_airline`) REFERENCES `m_airline` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
