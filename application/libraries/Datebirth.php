<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 11/24/16
 * Time: 14:12
 */
class Datebirth
{

    public function calculate_age($date_of_birth)
    {
        $dob = new DateTime($date_of_birth);
        $now = new DateTime();
        $age = $now->diff($dob);
        return $age->y;
    }

}