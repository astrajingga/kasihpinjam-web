<?php

/**
 * Created by PhpStorm.
 * User: agungrizkyana
 * Date: 1/18/17
 * Time: 06:13
 */
class Numbering
{

    protected $t_sktm_kesehatan = 't_sktm_kesehatan';
    protected $t_sktm_pendidikan = 't_sktm_pendidikan';
    protected $t_sktm_serbaguna = 't_sktm_serbaguna';
    protected $t_sktm_bpjs_kesehatan = 't_sktm_bpjs_kesehatan';
    protected $t_sk_tidak_mampu = 't_sk_tidak_mampu';
    protected $t_sk_domisili = 't_sk_domisili';
    protected $t_sk_ormas = 't_sk_domisili_ormas';
    protected $t_sk_perusahaan = 't_sk_domisili_perusahaan';
    protected $t_sk_wna = 't_sk_domisili_wna';
    protected $t_sk_belum_menikah = 't_sk_belum_menikah';
    protected $t_sk_belum_punya_rumah = 't_sk_belum_punya_rumah';
    protected $t_sk_kelakuan_baik = 't_sk_kelakuan_baik';
    protected $t_sk_serbaguna = 't_sk_serbaguna';
    protected $t_sk_orang_yang_sama = 't_sk_orang_yang_sama';

    private $CI;

    function __construct()
    {
        // Get the instance
        $this->CI = &get_instance();
    }

    
     public function get_no_sktm_kesehatan()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sktm_kesehatan);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 401;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }


    // GET NO SURAT SKTM PENDIDIKAN

    public function get_no_sktm_pendidikan()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sktm_pendidikan);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 402;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SKTM SERBAGUNA

    public function get_no_sktm_serbaguna()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sktm_serbaguna);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 403;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SKTM BPJS KESEHATAN

    public function get_no_sktm_bpjs_kesehatan()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sktm_bpjs_kesehatan);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 404;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }


    // GET NO SURAT SKTM BPJS KESEHATAN

    public function get_no_sktm()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_tidak_mampu);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 405;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK DOMISILI

    public function get_no_sk_domisili()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_domisili);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 101;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK ORMAS

    public function get_no_sk_ormas()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_ormas);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 102;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }


    // GET NO SURAT SK PERUSAHAAN

    public function get_no_sk_perusahaan()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_perusahaan);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 103;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK DOMISILI WNA

    public function get_no_sk_wna()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_wna);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 104;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }
     // GET NO SURAT SK BELUM MENIKAH

    public function get_no_sk_belum_menikah()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_belum_menikah);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 201;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK BELUM PUNYA RUMAH

    public function get_no_sk_belum_punya_rumah()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_belum_punya_rumah);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 202;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK KELAKUAN BAIK

    public function get_no_sk_kelakuan_baik()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_kelakuan_baik);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 203;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK SERBAGUNA

    public function get_no_sk_serbaguna()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_serbaguna);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 204;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

    // GET NO SURAT SK ORANG YANG SAMA

    public function get_no_sk_orang_yang_sama()
    {

        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);
        $date = date('d', $time);

        $this->CI->db->where("DATE(created_at) BETWEEN '".date('m-01-Y')."' AND '". date('m-t-Y') ."'");

        $no_diklat = $this->CI->db->count_all($this->t_sk_orang_yang_sama);
        $no_diklat++;
        $no_diklat_text = "";
        $id_surat = 205;

        if ($no_diklat < 10) {
            $no_diklat_text = "00" . $no_diklat;
        } else if ($no_diklat >= 10 && $no_diklat < 100) {
            $no_diklat_text = "0" . $no_diklat;
        } else {
            $no_diklat_text = $no_diklat;
        }

        switch ($month) {
            case '01':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '02':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '03':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '04':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '05':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '06':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '07':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '08':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '09':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '10':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '11':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
            case '12':
                $no_diklat_text = $id_surat . $no_diklat_text . $date . $month . $year;
                break;
        }

        return $no_diklat_text;
    }

}