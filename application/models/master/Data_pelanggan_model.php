<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Didik Kurniawan
 */
class Data_pelanggan_model extends MY_Model {

  protected $table       = 'data_pelanggan';
  protected $table_relation       = 'acl_roles';
  private $ci;

  function __construct()
  {
    parent::__construct();
  }

  private function _get_select(){
    $select = $this->table.".id, ";
    $select .= $this->table.".website, ";
    $select .= $this->table.".address, ";
    $select .= $this->table.".email_operator, ";
    $select .= $this->table.".name_operator, ";
    $select .= $this->table.".avatar_operator, ";
    $select .= $this->table.".phone, ";
    $select .= $this->table.".created_at, ";
    // $select .= $this->operator_table.".avatar_operator,";
    
    return $select;
}

  function datatables()
  {
    $this->datatables->select($this->_get_select());
    $this->datatables->from($this->table);
    return $this->datatables->generate();
  }
  
  public function add($data)
  {
      
      $this->db->insert($this->table, $data);
      $id = $this->db->insert_id();
      $inserted = $this->db->get_where($this->table, array('id' => $id))->row();
      
      return $inserted;
  }

  public function get_all($limit = 0, $offset = 0, $where = "")
  {
      $this->db->select('*');
      $this->db->from($this->table_relation);
      
      if($where != ""){
          $this->db->where($where);
      }
      
      $this->db->limit($limit);
      $this->db->offset($offset);
      $data = $this->db->get()->result();
      return $data;
  }

  public function get_by_like($term, $where = "")
  {
      $this->db->select('*');
      $this->db->from($this->table_relation);
      
      if($where != ""){
          $this->db->where($where);
      }
      
      $this->db->where($this->table_relation . ".name LIKE '%" . $term . "%' ");
      $data = $this->db->get()->result();
      return $data;
      
  }
  

}
