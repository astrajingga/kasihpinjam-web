<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Didik Kurniawan
 */
class Gambar_model extends MY_Model {

  protected $table                  = 'tour_umrah_image';
  protected $table_intenerary       = 'tour_umrah_intenerary';
//   protected $table_relation         = 'acl_roles';
//   protected $table_operator         = 'data_pelanggan';
//   protected $table_users            = 'auth_users';
  private $ci;

  function __construct()
  {
    parent::__construct();
		$this->ci = & get_instance();
  }

  private function _get_select(){
      $select = $this->table.".id, ";
      $select .= $this->table.".id_tour_umrah, ";
      $select .= $this->table.".image, ";
      $select .= $this->table.".caption, ";
      $select .= $this->table.".updated_at, ";
      $select .= $this->table.".created_at, ";
      $select .= $this->table.".created_by, ";
      $select .= $this->table.".updated_by, ";
  // $select .= $this->operator_table.".avatar_operator,";
      
      return $select;
  }

  public function save($data)
  {
      $this->db->insert($this->table_intenerary, $data);
          $id = $this->db->insert_id();
          $data = $this->db->get_where($this->table_intenerary, array('id' => $id))->row();
          return $data;
  }

  public function deleteImage($id)
  {
    $this->db->delete($this->table, array('id' => $id));
    $deleted = $this->db->get_where($this->table, array('id' => $id))->row();
    return $deleted;
      
  }

  public function deleteIntenerary($id)
  {
    $this->db->delete($this->table_intenerary, array('id' => $id));
    $deleted = $this->db->get_where($this->table_intenerary, array('id' => $id))->row();
    return $deleted;
      
  }

  

}


