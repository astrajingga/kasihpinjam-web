<?php

/**
* Created by PhpStorm.
* User: Didik Kurniawan
* Date: 12/11/17
* Time: 13:52
*/
class Booking_umroh_model extends MY_Model
{
    protected $table = 'tour_umrah';
    protected $table_image = 'tour_umrah_image';
    protected $table_intenerary = 'tour_umrah_intenerary';
    protected $table_booking = 'booking_umroh';


    public function view_all_trip(){
        $this->db->select("*");
        $this->db->from($this->table);
        $data = $this->db->get()->result();
        return $data;
    }

    public function view_images($id){
        $this->db->select("*");
        $this->db->from($this->table_image);
        $this->db->where($this->table_image . ".id_tour_umroh", $id);
        $this->db->limit('1');
        $data = $this->db->get()->row();
        return $data;
    }

    public function view_all_images($id){
        $this->db->select("*");
        $this->db->from($this->table_image);
        $this->db->where($this->table_image . ".id_tour_umroh", $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function detail_trip($id){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }

    public function detail_booking($code){
        $this->db->select("*");
        $this->db->from($this->table_booking);
        $this->db->where($this->table_booking . ".kode_pesanan", $code);
        $this->db->limit('1');
        $data = $this->db->get()->row();
        return $data;
    }

    public function sum_booking($code){
        $this->db->select("*,count(kode_pesanan) as qty");
        $this->db->from($this->table_booking);
        $this->db->where($this->table_booking . ".kode_pesanan", $code);
        $data = $this->db->get()->row();
        return $data;
    }

    public function send_email($id){
        $this->db->select("tour_umroh.*, data_pelanggan.name_operator,
                         data_pelanggan.address, data_pelanggan.phone,
                         data_pelanggan.avatar_operator, auth_users.username");
        $this->db->from($this->table);
        $this->db->join("auth_users", "auth_users.id = tour_umroh.created_by");
        $this->db->join("data_pelanggan", "data_pelanggan.id = auth_users.id_tour_operator");
        $this->db->where($this->table . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }

    public function intenerary($id){
        $this->db->select("*");
        $this->db->from($this->table_intenerary);
        $this->db->where($this->table_intenerary . ".id_tour_umroh", $id);
        $data = $this->db->get()->result();
        return $data;
    }


}