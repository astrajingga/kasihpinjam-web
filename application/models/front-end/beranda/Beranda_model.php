<?php

/**
* Created by PhpStorm.
* User: Didik Kurniawan
* Date: 12/11/17
* Time: 13:52
*/
class Beranda_model extends MY_Model
{
    protected $table = 'tour_trip';
    protected $table_image = 'tour_trip_image';
    protected $table_intenerary = 'tour_trip_intenerary';

    protected $table_booking = 'booking';
    protected $table_booking_umroh = 'booking_umroh';

    protected $table_umroh = 'tour_umrah';
    protected $table_image_umroh = 'tour_umrah_image';
    protected $table_intenerary_umroh = 'tour_umrah_intenerary';

    function view($num, $offset)  {
    
    /*variable num dan offset digunakan untuk mengatur jumlah
      data yang akan dipaging, yang kita panggil di controller*/
    $this->db->where($this->table_umroh.'.visible',1);
    $this->db->order_by($this->table_umroh.'.title','ASC');
    $query = $this->db->get($this->table_umroh,$num, $offset);
    return $query->result();
    
    }

    function viewTrip($num, $offset)  {
        
        /*variable num dan offset digunakan untuk mengatur jumlah
          data yang akan dipaging, yang kita panggil di controller*/
        $this->db->where($this->table.'.visible',1);
        $this->db->order_by($this->table.'.title','ASC');
        $query = $this->db->get($this->table,$num, $offset);
        return $query->result();
        
        }

    // View Data Trip
    public function view_all_trip(){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table.'.visible',1);
        $this->db->limit('6');
        $data = $this->db->get()->result();
        return $data;
    }

    // View Data Trip
    public function view_allTrip(){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table.'.visible',1);
        $data = $this->db->get()->result();
        return $data;
    }

    // Cari Data Trip
    public function search($data){
        $this->db->select("*");
        $this->db->from($this->table);
        if($data['category'] == "" && $data['destinasi']==""){
            $this->db->like($this->table.'.depature_date',$data['tanggal']);
        }elseif($data['category'] == "" && $data['tanggal']==""){
            $this->db->like($this->table.'.title',$data['destinasi']);
        }elseif($data['destinasi'] == "" && $data['tanggal']==""){
            $this->db->like($this->table.'.category_trip',$data['category']);
        }else{
            $this->db->like($this->table.'.category_trip',$data['category']);
            $this->db->like($this->table.'.title',$data['destinasi']);
            $this->db->like($this->table.'.depature_date',$data['tanggal']);
        }
        
        $this->db->where($this->table.'.visible',1);
        $data['halaman'] = null;
        $data['jml'] = 0;
		/*membuat variable halaman untuk dipanggil di view nantinya*/
		$data['offset'] = 0;
        $data['data'] = $this->db->get()->result();
        $this->template->build('front-end/trip/index', $data);
    }

     // Cari Data Umroh
     public function searchUmroh($data){
        $this->db->select("*");
        $this->db->from($this->table_umroh);
        if($data['category'] == "" && $data['destinasi']==""){
            $this->db->like($this->table_umroh.'.depature_date',$data['tanggal']);
        }elseif($data['category'] == "" && $data['tanggal']==""){
            $this->db->like($this->table_umroh.'.title',$data['destinasi']);
        }elseif($data['destinasi'] == "" && $data['tanggal']==""){
            $this->db->like($this->table_umroh.'.category_umrah',$data['category']);
        }else{
            $this->db->like($this->table_umroh.'.category_umrah',$data['category']);
            $this->db->like($this->table_umroh.'.title',$data['destinasi']);
            $this->db->like($this->table_umroh.'.depature_date',$data['tanggal']);
        }
        
        $this->db->where($this->table_umroh.'.visible',1);
        $data['halaman'] = null;
        $data['jml'] = 0;
		/*membuat variable halaman untuk dipanggil di view nantinya*/
		$data['offset'] = 0;
        $data['data'] = $this->db->get()->result();
        $this->template->build('front-end/umroh/index', $data);
    }

    public function view_images($id){
        $this->db->select("*");
        $this->db->from($this->table_image);
        $this->db->where($this->table_image . ".id_tour_trip", $id);
        $this->db->limit('1');
        $data = $this->db->get()->row();
        return $data;
    }

    public function view_all_images($id){
        $this->db->select("*");
        $this->db->from($this->table_image);
        $this->db->where($this->table_image . ".id_tour_trip", $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function detail_trip($id){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }

    public function send_email($id){
        $this->db->select("tour_trip.*, data_pelanggan.name_operator,data_pelanggan.website,data_pelanggan.email_operator as email,
        data_pelanggan.address, data_pelanggan.phone,data_pelanggan.wa,data_pelanggan.sipt,
        data_pelanggan.avatar_operator, auth_users.username");
        $this->db->from($this->table);
        $this->db->join("auth_users", "auth_users.id = tour_trip.created_by");
        $this->db->join("data_pelanggan", "data_pelanggan.id = auth_users.id_tour_operator");
        $this->db->where($this->table . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }

    public function intenerary($id){
        $this->db->select("*");
        $this->db->from($this->table_intenerary);
        $this->db->where($this->table_intenerary . ".id_tour_trip", $id);
        $this->db->order_by('hari', 'ASC');
        $data = $this->db->get()->result();
        return $data;
    }
    // End View Data Trip


    public function view_all_umroh(){
        $this->db->select("*");
        $this->db->from($this->table_umroh);
        $this->db->where($this->table_umroh.'.visible',1);
        $this->db->limit('6');
        $data = $this->db->get()->result();
        return $data;
    }

    public function view_allUmroh(){
        $this->db->select("*");
        $this->db->from($this->table_umroh);
        $this->db->where($this->table_umroh.'.visible',1);
        $data = $this->db->get()->result();
        return $data;
    }

    public function view_images_umroh($id){
        $this->db->select("*");
        $this->db->from($this->table_image_umroh);
        $this->db->where($this->table_image_umroh . ".id_tour_umrah", $id);
        $this->db->limit('1');
        $data = $this->db->get()->row();
        return $data;
    }

    public function intenerary_umroh($id){
        $this->db->select("*");
        $this->db->from($this->table_intenerary_umroh);
        $this->db->where($this->table_intenerary_umroh . ".id_tour_umrah", $id);
        $this->db->order_by('hari', 'ASC');
        $data = $this->db->get()->result();
        return $data;
    }
    public function view_all_images_umroh($id){
        $this->db->select("*");
        $this->db->from($this->table_image_umroh);
        $this->db->where($this->table_image_umroh . ".id_tour_umrah", $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function detail_umroh($id){
        $this->db->select("*");
        $this->db->from($this->table_umroh);
        $this->db->where($this->table_umroh . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }
    public function send_email_umroh($id){
        $this->db->select("tour_umrah.*, data_pelanggan.name_operator,data_pelanggan.website,data_pelanggan.email_operator as email,
        data_pelanggan.address, data_pelanggan.phone,data_pelanggan.wa,data_pelanggan.ijin_trevel_umrah,
        data_pelanggan.ijin_trevel_haji,data_pelanggan.avatar_operator, auth_users.username");
        $this->db->from($this->table_umroh);
        $this->db->join("auth_users", "auth_users.id = tour_umrah.created_by");
        $this->db->join("data_pelanggan", "data_pelanggan.id = auth_users.id_tour_operator");
        $this->db->where($this->table_umroh . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }

    public function tour_operator($id){
        $this->db->select("tour_umrah.*, data_pelanggan.name_operator,data_pelanggan.website,data_pelanggan.email_operator as email,
                         data_pelanggan.address, data_pelanggan.phone,data_pelanggan.wa,data_pelanggan.sipt,
                         data_pelanggan.avatar_operator, auth_users.username");
        $this->db->from($this->table_umroh);
        $this->db->join("auth_users", "auth_users.id = tour_umrah.created_by");
        $this->db->join("data_pelanggan", "data_pelanggan.id = auth_users.id_tour_operator");
        $this->db->where($this->table_umroh . ".id", $id);
        $data = $this->db->get()->row();
        return $data;
    }
    
    
    public function qty_booking($code,$qty){
        $this->db->select("*");
        $this->db->from($this->table_booking);
        $this->db->where($this->table_booking . ".kode_pesanan", $code);
        $this->db->order_by('created_at', 'asc');
        $this->db->limit($qty);
        $data = $this->db->get()->result();
        return $data;
    }

    public function qty_booking_umroh($code,$qty){
        $this->db->select("*");
        $this->db->from($this->table_booking_umroh);
        $this->db->where($this->table_booking_umroh . ".kode_pesanan", $code);
        $this->db->order_by('created_at', 'asc');
        $this->db->limit($qty);
        $data = $this->db->get()->result();
        return $data;
    }

    //Perjalanan Seru lainnya trip

    public function tren_trip($id){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table.'.visible',1);
        $this->db->where($this->table.'.id !='.$id);
        $this->db->limit('3');
        $data = $this->db->get()->result();
        return $data;
    }

    //Perjalanan Seru lainnya trip

    public function tren_umroh($id){
        $this->db->select("*");
        $this->db->from($this->table_umroh);
        $this->db->where($this->table_umroh.'.visible',1);
        $this->db->where($this->table_umroh.'.id !='.$id);
        $this->db->limit('3');
        $data = $this->db->get()->result();
        return $data;
    }

    //Harga diskon terbanyak trip
    public function diskon_trip(){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table.'.visible',1);
        $this->db->order_by($this->table.'.komisi','DESC');
        $this->db->limit('3');
        $data = $this->db->get()->result();
        return $data;
    }

    //Harga diskon terbanyak Umroh
    public function diskon_umroh(){
        $this->db->select("*");
        $this->db->from($this->table_umroh);
        $this->db->where($this->table_umroh.'.visible',1);
        $this->db->order_by($this->table_umroh.'.komisi','DESC');
        $this->db->limit('3');
        $data = $this->db->get()->result();
        return $data;
    }
}