
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Didik Kurniawan
 */
class Booking_model extends MY_Model {

  protected $table       = 'booking';
  protected $booking_umroh      = 'booking_umroh';
  protected $table_tour_operator = 'auth_users';
  protected $table_trip = 'tour_trip';
  protected $table_umroh = 'tour_umrah';
  private $ci;

  function __construct()
  {
    parent::__construct();
  }

  private function _get_select(){
    $select = $this->table.".id, ";
    $select .= $this->table.".nama, ";
    $select .= $this->table.".telp, ";
    $select .= $this->table.".email, ";
    $select .= $this->table.".code, ";
    $select .= $this->table.".approve, ";
    $select .= $this->table.".created_at, ";
    $select .= $this->table_tour_operator.".username, ";
    $select .= $this->table_trip.".title, ";

    // $select .= $this->operator_table.".avatar_operator,";
    
    return $select;
}

  function datatables()
  {
    $query = $this->db->query("SELECT role_id FROM auth_users WHERE auth_users.id =". $this->auth->userid())->row();
    $this->datatables->select($this->_get_select());
    $this->datatables->from($this->table);
    $this->datatables->join('tour_trip','tour_trip.id = booking.id_tour');
    $this->datatables->join('auth_users','auth_users.id = tour_trip.created_by');
      if($query->role_id == 1){
      }else{
        $this->datatables->where('auth_users.id',$this->auth->userid());
      }
    return $this->datatables->generate();
  }

  function datatables_umroh()
  {
    $this->datatables->select($this->_get_select());
    $this->datatables->from($this->table);
    $this->datatables->join('tour_umrah','tour_umrah.id = booking.id_tour');
    $this->datatables->join('auth_users','auth_users.id = tour_umrah.created_by');
    if($this->datatables->where('auth_users.role_id', 1)){
    }
    $this->datatables->where('auth_users.id',$this->auth->userid());
    return $this->datatables->generate();
  }
  
  public function add($data)
  {
      
      $this->db->insert($this->table, $data);
      $id = $this->db->insert_id();
      $inserted = $this->db->get_where($this->table, array('id' => $id))->row();
      
      return $inserted;
  }

  public function add_umroh($data)
  {
      
      $this->db->insert($this->booking_umroh, $data);
      $id = $this->db->insert_id();
      $inserted = $this->db->get_where($this->booking_umroh, array('id' => $id))->row();
      
      return $inserted;
  }

  public function accept($id,$data){
    $this->db->update($this->table, $data, array('id' => $id));
        
    $id = $this->db->insert_id();
    $updated = $this->db->get_where($this->table, array('id' => $id))->row();
        
    return $updated;
    
  }

  public function reject($id,$data){
    $this->db->update($this->table, $data, array('id' => $id));
        
    $id = $this->db->insert_id();
    $updated = $this->db->get_where($this->table, array('id' => $id))->row();
        
    return $updated;
    
  }

  public function get_all($limit = 0, $offset = 0, $where = "")
  {
      $this->db->select('*');
      $this->db->from($this->table_relation);
      
      if($where != ""){
          $this->db->where($where);
      }
      
      $this->db->limit($limit);
      $this->db->offset($offset);
      $data = $this->db->get()->result();
      return $data;
  }

  public function get_by_like($term, $where = "")
  {
      $this->db->select('*');
      $this->db->from($this->table_relation);
      
      if($where != ""){
          $this->db->where($where);
      }
      
      $this->db->where($this->table_relation . ".name LIKE '%" . $term . "%' ");
      $data = $this->db->get()->result();
      return $data;
      
  }
  

}
