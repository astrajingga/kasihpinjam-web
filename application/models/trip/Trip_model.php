<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 *
 * @package App
 * @category Model
 * @author Didik Kurniawan
 */
class Trip_model extends MY_Model {

  protected $table                  = 'tour_trip';
  protected $table_intenerary       = 'tour_trip_intenerary';
  protected $table_relation         = 'acl_roles';
  protected $table_operator         = 'data_pelanggan';
  protected $table_users            = 'auth_users';
  private $ci;

  function __construct()
  {
    parent::__construct();
		$this->ci = & get_instance();
  }

  private function _get_select(){
      $select = $this->table.".id, ";
      $select .= $this->table.".price, ";
      $select .= $this->table.".komisi, ";
      $select .= $this->table.".price_reteal, ";
      $select .= $this->table.".title, ";
      $select .= $this->table.".created_at, ";
      $select .= $this->table.".created_by, ";
      $select .= $this->table.".description, ";
      $select .= $this->table.".visible, ";
      $select .= $this->table_operator.".name_operator, ";
  // $select .= $this->operator_table.".avatar_operator,";
      
      return $select;
  }

  public function datatables()
  {
        $query = $this->db->query("SELECT role_id FROM auth_users WHERE auth_users.id =". $this->auth->userid())->row();
        $this->datatables->select($this->_get_select());
        $this->datatables->from($this->table);
        $this->datatables->join($this->table_users, $this->table_users.'.id = '.$this->table.'.created_by');
        $this->datatables->join($this->table_operator, 'data_pelanggan.id = auth_users.id_tour_operator');
        
            if($query->role_id == 1){
            }else{
                $this->datatables->where($this->table.".visible", 1);
                $this->datatables->where('auth_users.id',$this->auth->userid());
            }
        return $this->datatables->generate();
  }
  
  public function add($data)
  {
      
      $this->db->insert($this->table, $data);
      $id = $this->db->insert_id();
      $inserted = $this->db->get_where($this->table, array('id' => $id))->row();
      
      return $inserted;
  }

  public function save($data)
  {
      $this->db->insert($this->table_intenerary, $data);
          $id = $this->db->insert_id();
          $data = $this->db->get_where($this->table_intenerary, array('id' => $id))->row();
          return $data;
  }

  public function deleteIntenerary($id)
  {
    $this->db->delete($this->table_intenerary, array('id_tour_trip' => $id));
  }
  public function delete($id)
  {
    $this->db->update($this->table, array('visible' => 0), array('id' => $id));
    $id = $this->db->insert_id();
    $updated = $this->db->get_where($this->table, array('id' => $id))->row();
    
    return $updated;
  }

  public function restore($id)
  {
    $this->db->update($this->table, array('visible' => 1), array('id' => $id));
    $id = $this->db->insert_id();
    $updated = $this->db->get_where($this->table, array('id' => $id))->row();
    
    return $updated;
  }

  public function update($id, $data)
  {
      $this->db->update($this->table, $data, array('id' => $id));
      $id = $this->db->insert_id();
      $updated = $this->db->get_where($this->table, array('id' => $id))->row();
      
      return $updated;
      
  }

  

}
