<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Forms controller.
*
* @package Application
* @category Controller
* @author Didik Kurniawan
*/
class List_trip extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->load->vars(array(
        'page_title' => '<strong>Tour Trip</strong> | Data Tour Trip',
        'page_icon' => "
            <a href='" . site_url('trip/list_trip/add') . "' class='btn btn-primary' > <i class='fa fa-plus'></i> Tambah </a>
        "
        ));
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js(bower_url('moment/moment'))
        ->set_js('configs/datatables')
        ->set_js('app/controller/trip/index')
        ->build('trip/index');
    }
    
    public function add()
    {
        $this->load->vars(array(
        'page_title' => '<strong>Tour Trip</strong> | Tambah Tour Trip',
        ));
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-tagsinput/dist/bootstrap-tagsinput'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js(bower_url('sweetalert/dist/sweetalert.min'))
        ->set_js('configs/datatables')
        ->set_js('app/controller/trip/add')
        ->build('trip/add');
    }

    public function update($id)
    {
        $this->load->vars(array(
        'page_title' => '<strong>Tour Trip</strong> | Update Tour Trip',
        'id' => $id
        ));
        $this->load->model('trip/trip_model');
        $data['trip'] = $this->db->get_where('tour_trip', ['id' => $id])->row();
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-tagsinput/dist/bootstrap-tagsinput'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js(bower_url('sweetalert/dist/sweetalert.min'))
        ->set_js('configs/datatables')
        ->set_js('app/controller/trip/update')
        ->build('trip/update',$data);
    }

    public function approve($id)
    {
        $this->load->vars(array(
        'page_title' => '<strong>Tour Trip</strong> | Approve Tour Trip',
        'id' => $id
        ));
        $this->load->model('trip/trip_model');
        $data['trip'] = $this->db->get_where('tour_trip', ['id' => $id])->row();
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-tagsinput/dist/bootstrap-tagsinput'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js(bower_url('sweetalert/dist/sweetalert.min'))
        ->set_js('configs/datatables')
        ->set_js('app/controller/trip/approve')
        ->build('trip/approve',$data);
    }

    function upload()
    {
     sleep(1);
    //  if(!empty($_FILES["files"]["name"])){
        if($_FILES["files"]["name"] != ''){
            $output = '';
            $config['encrypt_name']   = TRUE;
            $config["upload_path"]    = './uploads/';
            $config["allowed_types"]  = 'gif|jpg|png';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
            {
            $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
            $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
            $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
            $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
            $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
            if($this->upload->do_upload('file'))
            {  
                
                
                $data = $this->upload->data();
    
                $fotobaru = $data["file_name"];
    
                $this->input->method('post');
    
                $data1 = array(
                    'image' => 'uploads/'.$fotobaru,
                    'id_tour_trip' => $this->input->post('id') ,
                    'updated_by' => $this->auth->userid(),
                );
                
                $this->db->insert('tour_trip_image', $data1);
            
            }
            }
            echo $output;   
         }
    //  }
     
    }
}