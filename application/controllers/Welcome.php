<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->language('welcome');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$this->load->model('front-end/beranda/beranda_model');
		$data['trip'] = $this->beranda_model->view_all_trip();
		$data['umroh'] = $this->beranda_model->view_all_umroh();
		$this->template
		->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-tagsinput/dist/bootstrap-tagsinput'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
		->set_js('app/controller/front-end/beranda/search')
		->build('welcome/welcome_message',$data);
	}
	
	
	

}
