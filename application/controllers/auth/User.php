<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User management controller.
 * 
 * @package App
 * @category Controller
 * @author Didik Kurniawan
 */
class User extends Admin_Controller 
{
	/**
	 * User form definition.
	 * 
	 * @var array
	 */
	protected $user_form = array(
		'id_tour_operator' => array(
			'label' => 'lang:Trevel',
			'rules' => 'trim',
			'helper' => 'form_dropdownlabel'
		),
		'id' => array(
			'helper' => 'form_hidden'
		),
		'username' => array(
			'label' => 'lang:Username',
			'rules' => 'trim|required|max_length[255]|callback_unique_username',
			'helper' => 'form_inputlabel'
		),
		
		// 'email' => array(
		// 	'label' => 'lang:email',
		// 	'rules' => 'trim|required|max_length[255]|valid_email|callback_unique_email',
		// 	'helper' => 'form_emaillabel'
		// ),
		'password' => array(
			'label' => 'lang:password',
			'rules' => 'trim|required|matches[confirm-password]',
			'helper' => 'form_passwordlabel',
			'value' => ''
		),
		'confirm-password' => array(
			'label' => 'lang:confirm_password',
			'rules' => 'trim|required',
			'helper' => 'form_passwordlabel',
			'value' => ''
		),
		'role_id' => array(
			'label' => 'lang:Role',
			'rules' => 'trim',
			'helper' => 'form_dropdownlabel'
		),
		'lang' => array(
			'label'	=> 'lang:language',
			'rules' => 'trim',
			'helper' => 'form_dropdownlabel'
		)
	);
	
	/**
	 * Redirect to index if cancel-button clicked.
	 */
	function __construct()
	{
		parent::__construct();
		
		if ($this->input->post('cancel-button'))
			redirect ('auth/user/index');
		
		$this->load->language('auth');
	}
	
	/**
	 * Display User list. 
	 */
	function index()
	{
		$this->load->vars(array(
			'page_title' => '<strong>Data User</strong>',
			'page_icon' => "
			<a href='" . site_url('auth/user/add') . "' class='btn btn-primary' > <i class='fa fa-plus'></i> Tambah </a>
			"
        ));
		$this->template
				->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
				->set_css(bower_url('select2/dist/css/select2.min'))
				->set_css(assets_url('css/custom'))
				->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
				->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
				->set_js(bower_url('select2/dist/js/select2.full.min'))
				->set_js(bower_url('summernote/dist/summernote'))
				->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
				->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
				->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
				->set_js('configs/datatables')
            	->set_js('app/controller/user/index')
				->build('auth/index');
	}

	
	/**
	 * Edit User
	 * 
	 * @param integer $id 
	 */
	function edit($id)
	{
		$this->update_data_user($id);
	}
	
	/**
	 * Add a new User. 
	 */
	function add()
	{
		$this->_updatedata();
	}
	
	/**
	 * Update profile.
	 */
	function profile()
	{
		$this->data['redirect'] = 'auth/user/profile';
		$this->edit($this->auth->userid());
	}
	
	/**
	 * Update user data
	 * 
	 * @param int $id
	 */
	function _updatedata($id = 0)
	{
		$this->load->vars(array(
			'page_title' => '<strong>Data User</strong> | Tambah User',
        ));
		$this->load->library('form_validation');
		$user_form = $this->user_form;
		
		// Update rules for update data
		if ($id > 0)
		{
			$user_form['username']['rules']	= "trim|required|max_length[255]|callback_unique_username[$id]";
			// $user_form['email']['rules']	= "trim|required|max_length[255]|valid_email|callback_unique_email[$id]";
			$user_form['password']['rules']	= "trim|matches[confirm-password]";
			$user_form['confirm-password']['rules']	= "trim";
		}
		
		$option = $this->role_model->get_option();
		foreach($option as $options)
			$user_form['id_tour_operator']['options'][$options->id] = $options->name_operator;
		// Add language options

		$languages = $this->config->item('languages', 'template');
		foreach($languages as $code => $language)
			$user_form['lang']['options'][$code] = $language['name'];
		
		// Add role options
		$role_tree = $this->role_model->get_tree();
		$user_form['role_id']['options'] = array(0 => '(' . lang('none') . ')') + $this->role_model->generate_options($role_tree);
			
		$this->form_validation->init($user_form);
		// Set default value for update data
		if ($id > 0)
			$this->form_validation->set_default($this->user_model->get_by_id($id));
		if ($this->form_validation->run())
		{
			$data_post                      = $this->input->post();
            
			if ($id > 0)
			{
				$this->user_model->update($id, $this->form_validation->get_values());
				$this->template->set_flashdata('info', lang('user_updated'));
			}
			else
			{
				$this->user_model->insert($this->form_validation->get_values());
				$this->template->set_flashdata('info', lang('Data Berhasil Ditambahkan'));
			}
			
			if (isset($this->data['redirect']))
				redirect($this->data['redirect']);
			else
				redirect('auth/user');
		}
		
		$this->data['form'] = $this->form_validation;
		$this->template->build('auth/user-form', $this->data);
	}


	// update data user
	function update_data_user($id = 0)
	{
		$this->load->vars(array(
			'page_title' => '<strong>Data User</strong> | Ubah Data User',
        ));
		$this->load->library('form_validation');
		$user_form = $this->user_form;
		
		// Update rules for update data
		if ($id > 0)
		{
			$user_form['username']['rules']	= "trim|required|max_length[255]|callback_unique_username[$id]";
			$user_form['password']['rules']	= "trim|matches[confirm-password]";
			$user_form['confirm-password']['rules']	= "trim";
		}
		
		// Add language options
		$languages = $this->config->item('languages', 'template');
		foreach($languages as $code => $language)
			$user_form['lang']['options'][$code] = $language['name'];
		
		// Add role options
		$role_tree = $this->role_model->get_tree();
		$user_form['role_id']['options'] = array(0 => '(' . lang('none') . ')') + $this->role_model->generate_options($role_tree);
			
		$this->form_validation->init($user_form);
		// Set default value for update data
		if ($id > 0)
			$this->form_validation->set_default($this->user_model->get_by_id($id));
		if ($this->form_validation->run())
		{
			if ($id > 0)
			{
				$this->user_model->update($id, $this->form_validation->get_values());
				$this->template->set_flashdata('info', lang('user_updated'));
			}
			else
			{
				$this->user_model->insert($this->form_validation->get_values());
				$this->template->set_flashdata('info', lang('user_added'));
			}
			
			if (isset($this->data['redirect']))
				redirect($this->data['redirect']);
			else
				redirect('auth/user');
		}
		
		$this->data['form'] = $this->form_validation;
		$this->template->build('auth/update-form', $this->data);
	}

	
	/**
	 * Delete a User
	 * 
	 * @param integer $id 
	 */
	function delete($id)
	{

		$user = $this->user_model->get_by_id($id);
		if ($user)
			$this->user_model->delete($id);
		
		redirect('auth/user');
	}
	
	/**
	 * Validation callback function to check whether the username is unique
	 * 
	 * @param string $value Username to check
	 * @param int $id Don't check if the username has this ID
	 * @return boolean
	 */
	function unique_username($value, $id = 0)
	{
		if ($this->user_model->is_username_unique($value, $id))
			return TRUE;
		else
		{
			$this->form_validation->set_message('unique_username', lang('already_taken'));
			return FALSE;
		}
	}
	
	/**
	 * Validation callback function to check whether the email is unique
	 * 
	 * @param string $value Email to check
	 * @param int $id Don't check if the email has this ID
	 * @return boolean
	 */
	
	
}

/* End of file user.php */
/* Location: ./application/modules/auth/controllers/user.php */