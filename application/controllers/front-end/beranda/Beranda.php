<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->language('welcome');
		$this->load->model('front-end/beranda/beranda_model');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	// user is already logged in
        if ($this->auth->loggedin()) 
		{
            redirect($this->config->item('dashboard_uri', 'template'));
        }
		
		$this->load->language('auth');
		$username = $this->input->post('username', FALSE);
		$password = $this->input->post('password', FALSE);
		$remember = $this->input->post('remember') ? TRUE : FALSE;
		$redirect = $this->input->get_post('redirect');
		
        // form submitted
        if ($username && $password) 
		{
            // get user from database
			$user = $this->user_model->get_by_username($username);
			if ($user && $this->user_model->check_password($password, $user->password))
			{
				// mark user as logged in
				$this->auth->login($user->id, $remember);
				
				// Add session data
				$this->session->set_userdata(array(
					'lang'		=> $user->lang,
					'role_id'	=> $user->role_id,
					'role_name'	=> $user->role_name
				));
				
				if ($redirect)
					redirect($redirect);
				else
					redirect($this->config->item('dashboard_uri', 'template'));
			}
			else
				$this->template->add_message ('error', lang('login_attempt_failed'));
        }
		else
		{
			if (($username === '') || ($password === ''))
				$this->template->add_message('error', lang('username_or_password_empty'));
		}
		
		$data = array();
		if ($username)
			$data['username'] = $username;
		if ($remember)
			$data['remember'] = $remember;
        
        // show login form
        $this->load->helper('form');
		
		$this->template->build('welcome/welcome_message', $data);
	}
	
		// $this->template->build('welcome/welcome_message');
	
	

}
