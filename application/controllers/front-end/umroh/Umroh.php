
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Umroh extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->language('welcome');
        $this->load->model('front-end/beranda/beranda_model');
		$this->load->library('pagination');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	// public function index()
	// {
	// 	$data['trip'] = $this->beranda_model->view_allUmroh();
	// 	$this->template->build('front-end/umroh/index', $data);
	// }

	
	
	public function index($offset=0){
		$jml = $this->db->get_where('tour_umrah',array('visible'=>1));
		
		$config['base_url'] = base_url().'front-end/umroh/umroh/index';
		
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = 9; /*Jumlah data yang dipanggil perhalaman*/ 
		$config['uri_segment'] = 2; /*data selanjutnya di parse diurisegmen 3*/
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);

		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['cur_tag_open'] = "<a href='". base_url() ."front-end/umroh/umroh/index'>";
		$config['cur_tag_close'] = "</a>";

		$this->pagination->initialize($config);
		$data['jml'] = $jml->num_rows();
		
		$data['halaman'] = $this->pagination->create_links();
		/*membuat variable halaman untuk dipanggil di view nantinya*/
		$data['offset'] = $offset;
	 
		$data['data'] = $this->beranda_model->view($config['per_page'], $offset);
		
		
		
		$this->template
		// ->set_css(bower_url('bootstrap/dist/js/bootstrap.min.js'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
		->set_css(bower_url('font-awesome/css/font-awesome.min'))
		// ->assets_url('js/jquery-1.11.3.min.js')
		->build('front-end/umroh/index', $data);
	}
    
    public function detail($id) {
		$this->load->model('front-end/beranda/beranda_model');
		$data['detail_umroh'] = $this->beranda_model->detail_umroh($id);
		$data['images'] = $this->beranda_model->view_images_umroh($id);
		$data['all_image'] = $this->beranda_model->view_all_images_umroh($id);
		$data['intenerary'] = $this->beranda_model->intenerary_umroh($id);
		$data['tour_operator'] = $this->beranda_model->send_email_umroh($id);
		$data['trip_lainnya'] = $this->beranda_model->tren_umroh($id);
		$this->template
		->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
		->set_css(bower_url('font-awesome/css/font-awesome.min'))
		->build('front-end/umroh/detail',$data);
    }
	
	
	

}
