<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_umroh_detail extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->language('welcome');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('front-end/beranda/beranda_model');
		$this->template->build('front-end/booking/booking_detail');
	}

	public function detail($id,$code) {
		$uid = str_replace('%20','',$id);
		
		$this->load->model('front-end/booking/booking_umroh_model');
		$data['detail_trip'] = $this->booking_umroh_model->detail_trip($uid);
		$data['booking'] = $this->booking_umroh_model->detail_booking($code);
		$data['sum'] = $this->booking_umroh_model->sum_booking($code);
		$this->template->build('front-end/booking/booking_umroh_detail',$data);
	}
}
