<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Forms controller.
*
* @package Application
* @category Controller
* @author Didik Kurniawan
*/
class Data_pelanggan extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->load->vars(array(
        'page_title' => '<strong>Master</strong> | Data Travel Agent',
        'page_icon' => "
            <a href='" . site_url('master/data_pelanggan/add') . "' class='btn btn-primary' > <i class='fa fa-plus'></i> Tambah </a>
        "
        ));
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js(bower_url('moment/moment'))
        ->set_js('configs/datatables')
        ->set_js('app/controller/master/pelanggan/index')
        ->build('master/data_pelanggan/index');
    }
    
    public function add()
    {
        $this->load->vars(array(
        'page_title' => '<strong>Master</strong> | Travel Agent',
        ));
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js(bower_url('sweetalert/dist/sweetalert.min'))
        ->set_js('configs/datatables')
        ->set_js('app/controller/master/pelanggan/add')
        ->build('master/data_pelanggan/add');

        
    }
    public function update($id)
    {
        $this->load->vars(array(
        'page_title' => '<strong>Master</strong> | Update Pelanggan',
        'id' => $id,
        'data' => $this->db->get_where('m_pelanggan', array('id' => $id))->row()
        ));
        
        $this->load->model('master/Pelanggan_model');
        
        $pelanggan = $this->db->get_where('m_pelanggan', ['id' => $id])->row();
        
        $data['view_prov'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->provinsi])->row();
        $data['view_kab'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kota])->row();
        $data['view_kec'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kecamatan])->row();
        $data['view_kel'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kelurahan])->row();
        $this->db->select('kodepos,id');
        $data['kodepos'] = $this->db->get_where('m_wilayah', ['kodepos' => $pelanggan->kodepos])->row();
        
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js('configs/datatables')
        ->set_js('controllers/master/pelanggan/update')
        ->build('master/pelanggan/update',$data);
    }
    
    public function view($id)
    {
        $this->load->vars(array(
        'page_title' => '<strong>Master</strong> | Detail',
        'id' => $id,
        'data' => $this->db->get_where('m_pelanggan', array('id' => $id))->row()
        ));
        $this->load->model('master/Pelanggan_model');
        
        $pelanggan = $this->db->get_where('m_pelanggan', ['id' => $id])->row();
        
        $data['view_prov'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->provinsi])->row();
        $data['view_kab'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kota])->row();
        $data['view_kec'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kecamatan])->row();
        $data['view_kel'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kelurahan])->row();
        $this->db->select('kodepos');
        $data['kodepos'] = $this->db->get_where('m_wilayah', ['kodepos' => $pelanggan->kodepos])->row();
        
        
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js('configs/datatables')
        ->set_js('controllers/master/pelanggan/view')
        ->build('master/pelanggan/view',$data);
    }
    
    public function delete($id)
    {
        $this->load->vars(array(
        'page_title' => '<strong>Pelanggan</strong> | Delete',
        'id' => $id,
        'data' => $this->db->get_where('m_pelanggan', array('id' => $id))->row()
        ));
        $this->load->model('master/Pelanggan_model');
        
        $pelanggan = $this->db->get_where('m_pelanggan', ['id' => $id])->row();
        
        $data['view_prov'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->provinsi])->row();
        $data['view_kab'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kota])->row();
        $data['view_kec'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kecamatan])->row();
        $data['view_kel'] = $this->db->get_where('m_wilayah', ['nama' => $pelanggan->kelurahan])->row();
        $this->db->select('kodepos');
        $data['kodepos'] = $this->db->get_where('m_wilayah', ['kodepos' => $pelanggan->kodepos])->row();
        $this->template
        ->set_css(assets_url('css/plugins/angular-datatables/angular-bootstrap.datatables'))
        ->set_css(bower_url('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min'))
        ->set_css(bower_url('select2/dist/css/select2.min'))
        ->set_css(assets_url('css/custom'))
        ->set_css(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('datatables/media/js/jquery.dataTables.min'))
        ->set_js(bower_url('datatables/media/js/dataTables.bootstrap4.min'))
        ->set_js(bower_url('select2/dist/js/select2.full.min'))
        ->set_js(bower_url('summernote/dist/summernote'))
        ->set_js(bower_url('bootstrap-datepicker/dist/js/bootstrap-datepicker.min'))
        ->set_js(bower_url('bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min'))
        ->set_js(bower_url('jquery.inputmask/dist/jquery.inputmask.bundle'))
        ->set_js('configs/datatables')
        ->set_js('controllers/master/pelanggan/delete')
        ->build('master/pelanggan/delete',$data);
    }
}