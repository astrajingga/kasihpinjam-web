<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class User extends Admin_Controller 
{
	  public function __construct()
    {
        parent::__construct();
         $this->load->model('auth/user_model');
    }

	public function index()
    {
        $data = $this->user_model->datatables();
        echo $data;
    }
}