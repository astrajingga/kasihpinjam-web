<?php

/**
 * User: Didik Kurniawan
 * Date: 11/14/17
 * Time: 07:26
 */
class Search extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('front-end/beranda/beranda_model');
    }
    public function index()
    {
        $this->beranda_model->search(array(
            'category' => $this->input->get('category'),
            'destinasi' => $this->input->get('destinasi'),
            'tanggal' => $this->input->get('keberangkatan')
        ));
        // echo $this->input->get('keberangkatan');
    }
    public function searchUmroh()
    {
        $this->beranda_model->searchUmroh(array(
            'category' => $this->input->get('category_umroh'),
            'destinasi' => $this->input->get('destinasi_umroh'),
            'tanggal' => $this->input->get('keberangkatan_umroh')
        ));
    }
}