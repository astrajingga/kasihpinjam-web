<?php

/**
 * User: Didik Kurniawan
 * Date: 11/14/17
 * Time: 07:26
 */
class Pelanggan extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->load->model('master/data_pelanggan_model');
    }
     public function index()
    {
        $data = $this->data_pelanggan_model->datatables();
        echo $data;
    }

    public function add()
    {
        
        if ($this->input->method('post')) {
            $foto = $_FILES['inputfile']['name'];
            $tmp = $_FILES['inputfile']['tmp_name'];
            // Rename nama fotonya dengan menambahkan tanggal dan jam upload
            $fotobaru = date('dmYHis').$foto;
            // Set path folder tempat menyimpan fotonya
            $path = "uploads/".$fotobaru;
            if(move_uploaded_file($tmp, $path)){
                $data = $this->data_pelanggan_model->add(array(
                    'category' => $this->input->post('category'),
                    'name_operator' => $this->input->post('nama_operator'),
                    'phone' => $this->input->post('phone'),
                    'email_operator' => $this->input->post('email_operator'),
                    'wa' => $this->input->post('wa'),
                    'website' => $this->input->post('website'),
                    'address' => $this->input->post('alamat'),
                    'sipt' => $this->input->post('sipt'),
                    'ijin_trevel_umrah' => $this->input->post('ijin_trevel_umrah'),
                    'ijin_trevel_haji' => $this->input->post('ijin_trevel_haji'),
                    'avatar_operator' => $path,
                    'visible' => 1,
                    'created_by' => $this->auth->userid()
                    ));
                    echo json_encode($data);
            }
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }

    public function update($id)
    {
        if ($this->input->method('post')) { 

            // echo json_encode($this->input->post('kodepos')); exit;

            $provinsi = $this->db->get_where('m_wilayah', ['id' => $this->input->post('provinsi')])->row();
            $kota = $this->db->get_where('m_wilayah', ['id' => $this->input->post('kota')])->row();
            $kecamatan = $this->db->get_where('m_wilayah', ['id' => $this->input->post('kecamatan')])->row();
            $kelurahan = $this->db->get_where('m_wilayah', ['id' => $this->input->post('kelurahan')])->row();

            $this->db->select('kodepos');
            $kodepos = $this->db->get_where('m_wilayah', ['id' => $this->input->post('kodepos')])->row();

            echo json_encode($this->pelanggan_model->update($id, array(
             'nama' => $this->input->post('nama'),
            'telepon' => $this->input->post('telepon'),
            'fax' => $this->input->post('fax'),
            'alamat' => $this->input->post('alamat'),
            'alamat_invoice' => $this->input->post('alamat_invoice'),
            'email' => $this->input->post('email'),
            'up' => $this->input->post('up'),
            'contact_person' => $this->input->post('contact_person'),
            'provinsi' => $provinsi->nama,
            'kota' => $kota->nama,
            'kecamatan' => $kecamatan->nama,
            'kelurahan' => $kelurahan->nama,
            'kodepos' => $kodepos->kodepos,
            'updated_by' => $this->auth->userid(),
            'updated_at' => date('Y-m-d h:i:s')
            )));
        } else {
            throw new Exception('Method not Allowed');
        }
    }
    
    /**
    * Dik, method search ini tolong di jaga ya. Kalo master pelanggan butuh, coba ganti nama fungsi dan proses nya aja.
    **/

    public function search()
    {
        $response = $this->pelanggan_model->get_all(10, 0);
        if ($this->input->get('q') || $this->input->post('q')) {
            $term = ($this->input->get('q')) ? $this->input->get('q') : $this->input->post('q');
            $response = $this->pelanggan_model->get_by_like($term);
        }
        echo json_encode(['items' => $response]);
    }


    public function delete($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->pelanggan_model->delete($id));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

    public function search_select()
    {
        $response = $this->data_pelanggan_model->get_all(10, 0, ""); // Cek kalo order sudah selesai tapi belum bayar
        if ($this->input->get('q') || $this->input->post('q')) {
            $term = ($this->input->get('q')) ? $this->input->get('q') : $this->input->post('q');
            $response = $this->data_pelanggan_model->get_by_like(trim($term), "");
        }
        echo json_encode(['items' => $response]);
    }

  

}