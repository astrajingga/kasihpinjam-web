<?php

/**
 * User: Didik Kurniawan
 * Date: 11/14/17
 * Time: 07:26
 */
class Fasilitas extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->load->model('master/data_fasilitas_model');
    }
     public function index()
    {
        $data = $this->data_fasilitas_model->datatables();
        echo $data;
    }
    public function add()
    {
        
        if ($this->input->method('post')) {
            $foto = $_FILES['inputfile']['name'];
            $tmp = $_FILES['inputfile']['tmp_name'];
            // Rename nama fotonya dengan menambahkan tanggal dan jam upload
            $fotobaru = date('dmYHis').$foto;
            // Set path folder tempat menyimpan fotonya
            $path = "uploads/icon/".$fotobaru;
            if(move_uploaded_file($tmp, $path)){
                $data = $this->data_fasilitas_model->add(array(
                    'name' => $this->input->post('nama'),
                    'description' => $this->input->post('description'),
                    'image' => $path,
                    'created_by' => $this->auth->userid()
                    ));
                    echo json_encode($data);
            }
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }


}