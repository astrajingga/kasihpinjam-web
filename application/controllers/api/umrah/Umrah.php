<?php

/**
 * User: Didik Kurniawan
 * Date: 11/14/17
 * Time: 07:26
 */
class Umrah extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->load->model('umrah/umrah_model');
         $this->load->model('umrah/gambar_model');
    }
     public function index()
    {
        $data = $this->umrah_model->datatables();
        echo $data;
    }

    public function add()
    {
        if ($this->input->method('post')) {
            $data = $this->umrah_model->add(array(
                'category_umrah' => $this->input->post('category'),
                'id_hotel' => $this->input->post('id_hotel'),
                'id_airline' => $this->input->post('id_airline'),
                'title' => $this->input->post('title'),
                'depature_date' => $this->input->post('tanggal_berangkat'),
                'return_date' => $this->input->post('tanggal_kepulangan'),
                'price' => $this->input->post('harga'),
                'price_reteal' => $this->input->post('harga_eceran'),
                'komisi' => $this->input->post('komisi'),
                'description' => $this->input->post('description'),
                'facilities_include' => json_encode($this->input->post('include')),
                'facilities_exclude' => json_encode($this->input->post('exclude')),
                'tokoh' => $this->input->post('tokoh'),
                'visible' => 2,
                'created_by' => $this->auth->userid()
            ));
            echo json_encode($data);
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }

    public function save(){
        if ($this->input->method('post')) {
                
                $data = $this->umrah_model->save(array(
                    'hari' => $this->input->post('hari'),
                    'id_tour_umrah' => $this->input->post('id'),
                    'description' => $this->input->post('intenerary'),
                    'created_by' => $this->auth->userid()
                ));
                echo json_encode($data);
            
        } else {
            throw new Exception('Method not Allowed');
        }
    }
    public function update($id)
    {
        if ($this->input->method('post')) {
            $data = $this->umrah_model->update($id,array(
                'category_umrah' => $this->input->post('category'),
                'title' => $this->input->post('title'),
                'depature_date' => $this->input->post('tanggal_berangkat'),
                'return_date' => $this->input->post('tanggal_kepulangan'),
                'price' => $this->input->post('harga'),
                'price_reteal' => $this->input->post('harga_eceran'),
                'komisi' => $this->input->post('komisi'),
                'description' => $this->input->post('description'),
                'facilities_include' => json_encode($this->input->post('include')),
                'facilities_exclude' => json_encode($this->input->post('exclude')),
                'visible' => 1,
                'created_by' => $this->auth->userid()
            ));
            echo json_encode($data);
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }
    public function approve($id)
    {
        if ($this->input->method('post')) {
            $data = $this->umrah_model->update($id,array(
                'price' => $this->input->post('harga'),
                'price_reteal' => $this->input->post('harga_eceran'),
                'komisi' => $this->input->post('komisi'),
                'visible' => 1,
                'created_by' => $this->auth->userid()
            ));
            echo json_encode($data);
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }
    
    public function addHotel(){
        if ($this->input->method('post')) {
            $foto = $_FILES['inputfile']['name'];
            $tmp = $_FILES['inputfile']['tmp_name'];
            // // Rename nama fotonya dengan menambahkan tanggal dan jam upload
            $fotobaru = date('dmYHis').$foto;
            // // Set path folder tempat menyimpan fotonya
            $path = "uploads/hotel/".$fotobaru;
            if(move_uploaded_file($tmp, $path)){
                $data = $this->umrah_model->addHotel(array(
                    'name' => $this->input->post('nama_hotel'),
                    'facilities' => $this->input->post('fasilities_hotel'),
                    'service' => $this->input->post('service_hotel'),
                    'description' => $this->input->post('description_hotel'),
                    'rating' => $this->input->post('rating_hotel'),   
                    'image' => $path,
                    'visible' => 1,
                    'created_by' => $this->auth->userid()
                    ));
                $this->umrah_model->addMasterHotel(array(
                    'name' => $this->input->post('nama_hotel'),
                    'facilities' => $this->input->post('fasilities_hotel'),
                    'service' => $this->input->post('service_hotel'),
                    'description' => $this->input->post('description_hotel'),
                    'rating' => $this->input->post('rating_hotel'),   
                    'image' => $path,
                    'visible' => 1,
                    'created_by' => $this->auth->userid()
                ));
                    echo json_encode($data);
            }
        } else {
            throw new Exception('Method not Allowed');
        }
    }


    public function addAirline(){
        if ($this->input->method('post')) {
            $foto = $_FILES['inputfile']['name'];
            $tmp = $_FILES['inputfile']['tmp_name'];
            // // Rename nama fotonya dengan menambahkan tanggal dan jam upload
            $fotobaru = date('dmYHis').$foto;
            // // Set path folder tempat menyimpan fotonya
            $path = "uploads/airline/".$fotobaru;
            if(move_uploaded_file($tmp, $path)){
                $data = $this->umrah_model->addAirline(array(
                    'name' => $this->input->post('nama_pesawat'),
                    'lunggage' => $this->input->post('kapasitas_bagasi'),
                    'economy_class' => $this->input->post('economy_class'),
                    'bussiness_class' => $this->input->post('business_class'),
                    'first_class' => $this->input->post('first_class'),  
                    'description' => $this->input->post('description_pesawat'),   
                    'image' => $path,
                    'visible' => 1,
                    'created_by' => $this->auth->userid()
                    ));
                $this->umrah_model->addMasterAirline(array(
                    'name' => $this->input->post('nama_pesawat'),
                    'lunggage' => $this->input->post('kapasitas_bagasi'),
                    'economy_class' => $this->input->post('economy_class'),
                    'bussiness_class' => $this->input->post('business_class'),
                    'first_class' => $this->input->post('first_class'),  
                    'description' => $this->input->post('description_pesawat'),   
                    'image' => $path,
                    'visible' => 1,
                    'created_by' => $this->auth->userid()
                ));
                    echo json_encode($data);
            }
        } else {
            throw new Exception('Method not Allowed');
        }
    }


    public function search()
    {
        $response = $this->umrah_model->get_all(10, 0);
        if ($this->input->get('q') || $this->input->post('q')) {
            $term = ($this->input->get('q')) ? $this->input->get('q') : $this->input->post('q');
            $response = $this->umrah_model->get_by_like($term);
        }
        echo json_encode(['items' => $response]);
    }


    public function delete($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->umrah_model->delete($id));
        } else {
            throw new Exception('Method not Allowed');
        }
        redirect('trip/list_umrah');
    }

    public function restore($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->umrah_model->restore($id));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

    public function search_select()
    {
        $response = $this->data_umrah_model->get_all(10, 0, ""); // Cek kalo order sudah selesai tapi belum bayar
        if ($this->input->get('q') || $this->input->post('q')) {
            $term = ($this->input->get('q')) ? $this->input->get('q') : $this->input->post('q');
            $response = $this->data_umrah_model->get_by_like(trim($term), "");
        }
        echo json_encode(['items' => $response]);
    }

    public function deleteImage($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->gambar_model->deleteImage($id));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

    public function deleteIntenerary($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->gambar_model->deleteIntenerary($id));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

  

}