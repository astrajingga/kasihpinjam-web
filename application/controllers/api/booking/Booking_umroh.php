<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require APPPATH . '/libraries/REST_Controller.php';
// use Restserver\Libraries\REST_Controller;
/** 
 * User: Didik Kurniawan
 * Date: 11/14/17
 * Time: 07:26
 */
class Booking_umroh extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->load->model('booking/booking_umroh_model');
    }
     public function index()
    {
        $data = $this->booking_umroh_model->datatables();
        echo $data;
    }


    public function save_umroh()
    {       
        $this->load->helper('string');
        $numeral = random_string('numeric',11);
        for ($i = 0; $i < $this->input->get('qty'); $i++) { 
            $random = random_string('alnum',6);
            $data =  $this->booking_model->add_umroh(array(
                'kode_pesanan' => strtoupper($numeral),
                'id_umroh' => $this->input->get('id_umroh'),
                'nama' => $this->input->get('nama'),
                'telp' => $this->input->get('telp'),
                'email' => $this->input->get('email'),
                'code' => strtoupper($random),
                'approve' => 0,
                'visible' => 1
            ));

            $this->load->model('front-end/beranda/beranda_model');
            $dataMessage['input'] = array(
                'kode_pesanan' => strtoupper($numeral),
                'id_umroh' => $this->input->get('id_umroh'),
                'nama' => $this->input->get('nama'),
                'telp' => $this->input->get('telp'),
                'email' => $this->input->get('email'),
                'qty' => $this->input->get('qty'),
                'code' => strtoupper($random));
            $dataMessage['trip'] =  $this->beranda_model->send_email_umroh($this->input->get('id_umroh'));
            $dataMessage['voucher'] =  $this->beranda_model->qty_booking_umroh(strtoupper($numeral),$this->input->get('qty'));

            $message = $this->load->view('email/booking',$dataMessage,TRUE);
                $uri = "https://docs.google.com/forms/d/e/1FAIpQLSejRocQcbRo-6Z0w9MLstz4rSFOS3OQxkFJZSgqo62SYYnwyQ/formResponse?usp=pp_url&entry.305304378=".$this->input->get('email')."&entry.313188184=Your Code Voucher ". strtoupper($random) ."&entry.982842264=<b>title</b>";
                $email = $this->input->get('email');
                $subject = "Your Code Voucher ". strtoupper($random);
                $content = str_replace("++","+",str_replace("++","+",str_replace("++","+",str_replace("++","+",str_replace("++","+",str_replace("++","+",str_replace("++","+",urlencode($message))))))));
                $var = file_get_contents('https://docs.google.com/forms/d/e/1FAIpQLSejRocQcbRo-6Z0w9MLstz4rSFOS3OQxkFJZSgqo62SYYnwyQ/formResponse?usp=pp_url&entry.305304378='.$email.'&entry.313188184='.urlencode($subject).'&entry.982842264='.$content);

                $id = $this->input->get('id_umroh');
                header( 'Location:'.base_url("front-end/booking/booking_detail/detail/").$id."/".$numeral ) ;
        }
        
    }
    

    public function accept($id){
        if ($this->input->method('post')) { 
            echo json_encode($this->booking_umroh_model->accept($id, array(
            'approve' => 1,
            'updated_at' => date('Y-m-d h:i:s')
            )));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

    public function reject($id){
        if ($this->input->method('post')) { 
            echo json_encode($this->booking_umroh_model->reject($id, array(
            'approve' => 2,
            'updated_at' => date('Y-m-d h:i:s')
            )));
        } else {
            throw new Exception('Method not Allowed');
        }
    }


}