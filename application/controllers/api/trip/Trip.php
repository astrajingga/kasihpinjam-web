<?php

/**
 * User: Didik Kurniawan
 * Date: 11/14/17
 * Time: 07:26
 */
class Trip extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('trip/gambar_model');
        $this->load->model('trip/trip_model');
    }
     public function index()
    {
        $data = $this->trip_model->datatables();
        echo $data;
    }

    public function add()
    {
        if ($this->input->method('post')) {
            $data = $this->trip_model->add(array(
                'category_trip' => $this->input->post('category'),
                'title' => $this->input->post('title'),
                'depature_date' => $this->input->post('tanggal_berangkat'),
                'return_date' => $this->input->post('tanggal_kepulangan'),
                'price' => $this->input->post('harga'),
                'price_reteal' => $this->input->post('harga_eceran'),
                'komisi' => $this->input->post('komisi'),
                'description' => $this->input->post('description'),
                'facilities_include' => json_encode($this->input->post('include')),
                'facilities_exclude' => json_encode($this->input->post('exclude')),
                'visible' => 2,
                'created_by' => $this->auth->userid()
            ));
            echo json_encode($data);
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }

    public function update($id)
    {
        if ($this->input->method('post')) {
            $data = $this->trip_model->update($id,array(
                'category_trip' => $this->input->post('category'),
                'title' => $this->input->post('title'),
                'depature_date' => $this->input->post('tanggal_berangkat'),
                'return_date' => $this->input->post('tanggal_kepulangan'),
                'price' => $this->input->post('harga'),
                'price_reteal' => $this->input->post('harga_eceran'),
                'komisi' => $this->input->post('komisi'),
                'description' => $this->input->post('description'),
                'facilities_include' => json_encode($this->input->post('include')),
                'facilities_exclude' => json_encode($this->input->post('exclude')),
                'visible' => 1,
                'created_by' => $this->auth->userid()
            ));
            echo json_encode($data);
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }
    public function approve($id)
    {
        if ($this->input->method('post')) {
            $data = $this->trip_model->update($id,array(
                'price' => $this->input->post('harga'),
                'price_reteal' => $this->input->post('harga_eceran'),
                'komisi' => $this->input->post('komisi'),
                'visible' => 1,
                'created_by' => $this->auth->userid()
            ));
            echo json_encode($data);
        } else {
            throw new Exception('Method not Allowed');
        }
        
    }

    public function save(){
        if ($this->input->method('post')) {
                $data = $this->trip_model->save(array(
                    'hari' => $this->input->post('hari'),
                    'id_tour_trip' => $this->input->post('id'),
                    'description' => $this->input->post('intenerary'),
                    'created_by' => $this->auth->userid()
                ));
                echo json_encode($data);
            
        } else {
            throw new Exception('Method not Allowed');
        }
    }


    public function search()
    {
        $response = $this->trip_model->get_all(10, 0);
        if ($this->input->get('q') || $this->input->post('q')) {
            $term = ($this->input->get('q')) ? $this->input->get('q') : $this->input->post('q');
            $response = $this->trip_model->get_by_like($term);
        }
        echo json_encode(['items' => $response]);
    }


    public function delete($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->trip_model->delete($id));
        } else {
            throw new Exception('Method not Allowed');
        }
        redirect('trip/list_trip');
    }

    public function restore($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->trip_model->restore($id));
        } else {
            throw new Exception('Method not Allowed');
        }
        // redirect('trip/list_trip');
    }

    public function deleteImage($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->gambar_model->deleteImage($id));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

    public function deleteIntenerary($id)
    {
        if ($this->input->method('post')) {
            echo json_encode($this->gambar_model->deleteIntenerary($id));
        } else {
            throw new Exception('Method not Allowed');
        }
    }

    public function search_select()
    {
        $response = $this->data_trip_model->get_all(10, 0, ""); // Cek kalo order sudah selesai tapi belum bayar
        if ($this->input->get('q') || $this->input->post('q')) {
            $term = ($this->input->get('q')) ? $this->input->get('q') : $this->input->post('q');
            $response = $this->data_trip_model->get_by_like(trim($term), "");
        }
        echo json_encode(['items' => $response]);
    }

  

}