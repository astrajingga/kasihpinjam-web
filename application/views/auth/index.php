<div class="panel panel-primary hide">
  <div class="panel-heading">
    <strong>Advance Search</strong>
  </div>
  <div class="panel-body">
  </div>
</div>

<div class="panel panel-default">

  <div class="panel-body no-padder">
        <table class="table table-striped" id="table_user">
            <thead>
            <tr>
                <th>Registered</th>
                <th>Tour Agent</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</div>