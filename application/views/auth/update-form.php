<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal box" method="post">
			<fieldset>
				<?php echo $form->fields(); ?>
			</fieldset>
			<?php
			echo form_actions(array(
				array(
					'id' => 'save-button',
					'value' => lang('save'),
					'class' => 'btn-primary'
				),
				array(
					'id' => 'cancel-button',
					'value' => lang('cancel')
				)
			));
			?>
		</form>
	</div>
</div>