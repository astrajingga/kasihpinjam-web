
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--setup export pdf / excel-->
<form name="form_add_trip" id="form_add_trip" novalidate>
<div class="panel panel-default">
    <div class="panel-heading no-overflow">
        <strong>Form Tour Trip</strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Kategori Umrah</label>
                        <div class="col-lg-9">
                            <select name="category" id="category" class="form-control">
                                <option value="0">--Pilih Katagori Umrah--</option>
                                <option value="1">Backpecker</option>
                                <option value="2">Premium</option>
                                <option value="3">Reguler</option>
                                <option value="4">Umrah Plus</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Tanggal Berangkat</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" name="tanggal_berangkat" id="tanggal_berangkat" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Tanggal Kepulangan</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" name="tanggal_kepulangan" id="tanggal_kepulangan" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3">Upload Foto</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="files" id="files" multiple required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="uploaded_images"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Title</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="title" id="title" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Harga Reteal</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="harga_eceran" id="harga_eceran" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Komisi</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="komisi" id="komisi" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Harga Publish</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="harga" id="harga" required disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <div class="row tokoh">
                    <label class="col-lg-3">Tokoh</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="tokoh" id="tokoh" required/>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                        <div class="row">
                        <label class="col-lg-12"><b>Rencana Perjalanan</b></label>
                        <?php 
                            $query = $this->db->query('SELECT id FROM tour_umrah ORDER BY id DESC LIMIT 1');
                            $row = $query->row_array();
                        ?>
                        <input type="hidden" id="id_tour_umrah" value="<?php echo $row['id']+1; ?>">
                            <div id="view_intenerary">
                            
                            </div>
                                
                            <div class="col-lg-12">
                                <a onclick="addIntenerary()" id="click" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Tambahkan
                                </a>
                            </div>
                            
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-12">Description</label>
                    <br>
                        <div class="col-lg-12">
                            <textarea required name="description" class="form-control summernote" id="description" cols="20" rows="15"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                        <div class="panel panel-default"> 
                            <div class="panel-heading"><b>Fasilitas</b></div> 
                                <div class="panel-body"> 
                                    <div class="flot-chart">
                                        <div class="row col-lg-6">
                                            <label class="col-lg-12"><b>Fasilitas Yang Termasuk</b></label>
                                            <br>
                                            <div class="col-lg-12 example-two">
                                                <input class="form-control" id="fasilities_include" size="23" value="Makanan,Passport,Visa" data-role="tagsinput"  />
                                            </div>
                                        </div>

                                        <div class="row col-lg-6">
                                            <label class="col-lg-12"><b>Fasilitas Tidak Termasuk</b></label>
                                            <br>
                                            <div class="col-lg-12 example-two">
                                                <input class="form-control" id="fasilities_exclude" size="23" value="Makanan,Passport,Visa" data-role="tagsinput"  />
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <i>Ketik fasilitas yang akan diinput lalu tekan "Enter"</i>  
                            </div>
                        </div>
                </div>
            <!-- <hr> -->
            <div class="col-lg-12">
                <div class="form-group">
                        <div class="panel panel-default"> 
                            <div class="panel-heading"><b>Pilih Hotel</b></div> 
                                <div class="panel-body"> 
                                    <div class="flot-chart">
                                    <?php 
                                        $query = $this->db->query('SELECT id FROM tour_umrah_hotel ORDER BY id DESC LIMIT 1');
                                        $row = $query->row_array();
                                    ?>
                                    <input type="hidden" id="id_hotel" value="<?php echo $row['id']+1; ?>">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Nama Hotel</label>
                                                <div class="col-lg-4">
                                                    <input type="text" class="form-control" name="nama_hotel" id="nama_hotel" required/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Fasilitas Hotel</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="fasilities_hotel" size="23" value="Makanan,Passport,Visa" data-role="tagsinput"  />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Layanan Hotel</label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="service_hotel" size="23" value="Makanan,Passport,Visa" data-role="tagsinput"  />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Deskripsi</label>
                                                <div class="col-lg-10">
                                                    <textarea required name="description_hotel" class="form-control summernote" id="description_hotel" cols="20" rows="15"></textarea>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Rating Hotel</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" name="rating_hotel" id="rating_hotel" required/>   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                            <label class="col-lg-2">Upload Foto</label>
                                                <div class="col-lg-10">
                                                    <input type="file" class="form-control" name="uploadhotel" id="uploadhotel" />       
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div> 
                            </div>
                        </div>
                </div>
            </div>
        
            <div class="col-lg-13">
                <div class="form-group">
                        <div class="panel panel-default"> 
                            <div class="panel-heading"><b>Pilih Pesawat</b></div> 
                                <div class="panel-body"> 
                                    <div class="flot-chart">
                                        <div class="form-group">
                                        <?php 
                                            $query = $this->db->query('SELECT id FROM tour_umrah_airline ORDER BY id DESC LIMIT 1');
                                            $row = $query->row_array();
                                        ?>
                                        <input type="hidden" id="id_airline" value="<?php echo $row['id']+1; ?>">
                                            <div class="row">
                                                <label class="col-lg-2">Nama Pesawat</label>
                                                <div class="col-lg-4">
                                                    <input type="text" class="form-control" name="nama_pesawat" id="nama_pesawat" required/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Kapasitas Bagasi</label>
                                                <div class="col-lg-10">
                                                    <textarea required name="kapasitas_bagasi" class="form-control summernote" id="kapasitas_bagasi" cols="20" rows="15"></textarea>    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Fasilitas Economy Class</label>
                                                <div class="col-lg-10">
                                                    <textarea required name="economy_class" class="form-control summernote" id="economy_class" cols="20" rows="15"></textarea>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Fasilitas Business Class</label>
                                                <div class="col-lg-10">
                                                    <textarea required name="business_class" class="form-control summernote" id="business_class" cols="20" rows="15"></textarea>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Fasilitas First Class</label>
                                                <div class="col-lg-10">
                                                    <textarea required name="first_class" class="form-control summernote" id="first_class" cols="20" rows="15"></textarea>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-lg-2">Deskripsi</label>
                                                <div class="col-lg-10">
                                                    <textarea required name="description_pesawat" class="form-control summernote" id="description_pesawat" cols="20" rows="15"></textarea>    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                            <label class="col-lg-2">Upload Foto</label>
                                                <div class="col-lg-10">
                                                    <input type="file" class="form-control" name="uploadline" id="uploadline" />  
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div> 
                            </div>
                        </div>
            </div>
     </div>
    
    <div class="panel-footer text-right">
        <button type="submit" id="submit" class="btn btn-primary">
            Simpan
        </button>

        <button type="button" id="back" class="btn btn-danger">
            Kembali
        </button>
    </div>
</div>
</form>
