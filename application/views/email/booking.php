
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>SEND EMAIL</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <style>
            html, body {
                height: 80%;
                /* background-image:'http://apps.halallocal.com/storage/app/emails/header.jpg'; */
            }
            .background {
                background-color:#8b93ff; 
                min-height: 780px;   
                width:600px; 
                position:relative; 
                margin:0 auto;
                border-radius: 25px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="col-md-12 background">
                    <div class="col-lg-2" style="
                                                float:left;
                                                background-color:rgb(255, 255, 255); 
                                                border-radius: 15px; 
                                                width:251px;
                                                height:80%;
                                                margin:50px 10px 0px 40px;">
                        <div class="col-lg-2" style="
                                                    background-color: #ffbf2f;
                                                    width: 251px;
                                                    margin: 0 auto;
                                                    
                                                    height: 44px;
                                                    border-radius: 15px 15px 0px 0px; 
                                                    color:#ffffff">
                            <img src="http://halallocal.com/assets/img/Invoice-03.png" alt="" width="221px" height="45px">
                            
                        </div> 
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12" style="color:gainsboro; margin: 5px 15px;" >Nama</div>
                                    <div class="col-lg-12">
                                        <div style=" border-bottom: 1px solid gainsboro; padding: 2px 0px; margin: 5px 15px;"><b><?php echo $input['nama']; ?></b></div>  
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12" style="margin: 5px 15px; color:gainsboro ">No.Telp</div>
                                    <div class="col-lg-12">
                                        <div style=" border-bottom: 1px solid gainsboro; padding: 2px 0px; margin: 5px 15px;"><b><?php echo $input['telp']; ?></b></div>  
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12" style="margin: 5px 15px; color:gainsboro">Email</div>
                                    <div class="col-lg-12">
                                        <div style=" border-bottom: 1px solid gainsboro; padding: 2px 0px; margin: 5px 15px;"><b><?php echo $input['email']; ?></b></div>  
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12" style="
                                                    border-bottom: 2px dashed gainsboro;
                                                    margin: 2px -14px;
                                                    width: 250px;
                                                    margin: 20px 0px 0px 0px; ">
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div style="  
                                                    padding-top: 10px; margin-bottom: 3px;
                                                    font-size: 20px;
                                                    margin: 5px 15px;">
                                                    <b><?php echo $trip->title; ?></b>
                                        </div>
                                        <div style=" color:gainsboro; margin-bottom: 5px;  margin: 5px 15px;">Waktu Keberangkatan-pulang</div>  
                                        <div style=" margin: 5px 15px;"><b><?php echo date('d',strtotime($trip->depature_date));?>-<?php echo date('d F Y',strtotime($trip->return_date));?></b></div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div style=" text-align:center;  padding: 26px 0px; ">
                                                <?php $price = $input['qty'] * $trip->price; ?>
                                                <p><center> Harga Keseluruhan </center></p>
                                                <b style="font-size: 30px;  color: #ffbf2f;">Rp <?php echo number_format($price,0,',','.'); ?></b>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-lg-12" style="
                                                border-bottom: 2px dashed gainsboro;
                                                margin: 2px -14px;
                                                width: 250px; ">
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div style=" text-align:center;  padding: 10px 0px 5px 0px; ">
                                            <div style="font-size: 17px;">KODE KUPON</div>
                                            <?php foreach($voucher as $voucher){ ?>
                                                <b style="font-size: 20px;"><?php echo $voucher->code; ?></b><br>
                                            <?php } ?>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div style="margin-right: 15px; text-align:right; ">
                                                <!-- <div style="font-size: 17px;">KODE KUPON</div> -->
                                                <i style="font-size: 12px;"><?php echo date("d F Y") ?></i>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-md-2" style="color:white; width:220px; margin:25px 40px 0px 10px; position:relative; float:right;">
                            <h3><b> Hello,<?php echo $input['nama']; ?>!</b></h3>
                            <p>
                                Your reservation for <br>
                                <b style="font-size:19px;"><?php echo $trip->title; ?></b><br>
                                is confirmed
                            </p>
                            <p>
                                Look forward to having you back<br>
                                for you other trevel plans<br>
                                Have fun!
                            </p>
                            <br><br><br><br><br><br><br><br><br>
                            <p>
                                <img src="http://halallocal.com/<?php echo $trip->avatar_operator;?> " alt="" width="220px" height="100px">
                                <div style="font-size:10px; text-align:center;"><?php echo $trip->address; ?></div>
                                <div style="font-size:10px; text-align:center;"><b><?php echo $trip->name_operator; ?> | <?php echo $trip->phone; ?></b></div>
                            </p>
                        </div>
            
                </div>
                
            </div>
        </div>
    </body>
</html>





