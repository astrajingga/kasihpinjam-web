
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--setup export pdf / excel-->
<form name="form_add_pelanggan" id="form_add_pelanggan" novalidate>
<div class="panel panel-default">
    <div class="panel-heading no-overflow">
        <strong>Form travel Agent</strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Kategori Agent</label>
                        <div class="col-lg-9">
                            <select name="category" id="category" class="form-control">
                                <option value="">--Pilih Katagori Agent--</option>
                                <option value="1">Tour Umrah</option>
                                <option value="2">Tour travel </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Nama Operator</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="nama" id="nama_operator" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">No Telp</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control" name="phone" id="phone" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">No WhatsApps</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control" name="wa" id="wa"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Email</label>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" name="email_operator" id="email_operator" required/>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Website</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="website" id="website"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Alamat</label>
                        <div class="col-lg-9">
                            <textarea cols="2" rows="2" class="form-control" name="alamat" id="alamat" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Surat Ijin Penyelenggara Travel</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="sipt" id="sipt" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Ijin Umrah</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="ijin_trevel_umrah" id="ijin_trevel_umrah" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Ijin Haji</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="ijin_trevel_haji" id="ijin_trevel_haji" />
                        </div>
                    </div>
                  </div>
                
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3">Upload Logo</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="upload" id="upload" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="btn btn-primary">
            Simpan
        </button>

        <button type="button" id="back" class="btn btn-danger">
            Kembali
        </button>
    </div>
</div>
</form>
