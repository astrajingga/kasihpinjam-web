
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--setup export pdf / excel-->
<form name="form_add_fasilitas" id="form_add_fasilitas" novalidate>
<div class="panel panel-default">
    <div class="panel-heading no-overflow">
        <strong>Form Add Fasilitas</strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Nama Fasilitas</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="nama" id="nama" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3">Upload Icon</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="upload" id="upload" required/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-12">Description</label>
                    <br>
                        <div class="col-lg-12">
                            <textarea required name="description" class="form-control summernote" id="description" cols="20" rows="15"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="panel-footer text-right">
        <button type="submit" class="btn btn-primary">
            Simpan
        </button>

        <button type="button" id="back" class="btn btn-danger">
            Kembali
        </button>
    </div>
</div>
</form>
