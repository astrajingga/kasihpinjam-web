
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--setup export pdf / excel-->
<form name="form_add_trip" id="form_add_trip" novalidate>
<div class="panel panel-default">
    <div class="panel-heading no-overflow">
        <strong>Form Tour Trip</strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Kategori Trip</label>
                        <div class="col-lg-9">
                            <select name="category" id="category" class="form-control">
                                <option value="">--Pilih Katagori Trip--</option>
                                <option value="1">Domestik</option>
                                <option value="2">International</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Tanggal Berangkat</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" name="tanggal_berangkat" id="tanggal_berangkat" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Tanggal Kepulangan</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" name="tanggal_kepulangan" id="tanggal_kepulangan" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3">Upload Foto</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="files" id="files" multiple required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="uploaded_images"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Title</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="title" id="title" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Harga Reteal</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="harga_eceran" id="harga_eceran" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Komisi</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="komisi" id="komisi" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Harga Publish</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="harga" id="harga" required disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3"></label>
                        <div class="col-lg-9">
                            <!-- <input type="file" class="form-control" name="upload" id="upload" required/> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                        <div class="row">
                        <label class="col-lg-12"><b>Rencana Perjalanan</b></label>
                        <?php 
                            $query = $this->db->query('SELECT id FROM tour_trip ORDER BY id DESC LIMIT 1');
                            $row = $query->row_array();
                        ?>
                        <input type="hidden" id="id_tour_trip" value="<?php echo $row['id']+1; ?>">
                            <div id="view_intenerary">
                            
                            </div>
                                
                            <div class="col-lg-12">
                                <a onclick="addIntenerary()" id="click" class="btn btn-success">
                                        <i class="fa fa-plus"></i> Tambahkan
                                </a>
                            </div>
                            
                        </div>
                    </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-12">Description</label>
                    <br>
                        <div class="col-lg-12">
                            <textarea required name="description" class="form-control summernote" id="description" cols="20" rows="15"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                        <div class="panel panel-default"> 
                            <div class="panel-heading">Fasilitas</div> 
                                <div class="panel-body"> 
                                    <div class="flot-chart">

                                        <div class="row col-lg-6">
                                            <label class="col-lg-12"><b>Fasilitas Yang Termasuk</b></label>
                                            <br>
                                            <div class="col-lg-12 example-two">
                                                <input class="form-control" id="fasilities_include" size="23" value="Makanan,Passport,Visa" data-role="tagsinput"  />
                                            </div>
                                        </div>

                                        <div class="row col-lg-6">
                                            <label class="col-lg-12"><b>Fasilitas Tidak Termasuk</b></label>
                                            <br>
                                            <div class="col-lg-12 example-two">
                                                <input class="form-control" id="fasilities_exclude" size="23" value="Makanan,Passport,Visa" data-role="tagsinput"  />
                                            </div>
                                        </div>
                                    </div>  
                                </div> 
                                <i>Ketik fasilitas yang akan diinput lalu tekan "Enter"</i>  
                            </div>
                        </div>
                </div>
            <br>
            
        </div>

    </div>
    <div class="panel-footer text-right">
        <button type="submit" id="submit" class="btn btn-primary">
            Simpan
        </button>

        <button type="button" id="back" class="btn btn-danger">
            Kembali
        </button>
    </div>
</div>
</form>
