
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--setup export pdf / excel-->
<form name="form_update_trip" id="form_update_trip" novalidate>
<input type="hidden" value="<?php echo $id; ?>" name="id" id="id"/>
<div class="panel panel-default">
    <div class="panel-heading no-overflow">
        <strong>Form Tour Trip</strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Kategori Trip</label>
                        <div class="col-lg-9">
                            <select name="category" id="category" class="form-control">
                                <?php if($trip->category_trip == 1){?> 
                                        <option value="1">Domestik</option>
                                        <option value="2">International</option>
                                <?php  }else{ ?>
                                        <option value="2">International</option>
                                        <option value="1">Domestik</option>
                                <?php } ?>
                                
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Tanggal Berangkat</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" name="tanggal_berangkat" id="tanggal_berangkat" value="<?php echo $trip->depature_date;?>" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Tanggal Kepulangan</label>
                        <div class="col-lg-9">
                            <input type="date" class="form-control" name="tanggal_kepulangan" id="tanggal_kepulangan" value="<?php echo $trip->return_date;?>" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3">Tambah Foto</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control" name="files" id="files" multiple />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="uploaded_images"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Title</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="title" id="title" value="<?php echo $trip->title;?>" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Harga Retail</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="harga_eceran" id="harga_eceran" value="<?php echo $trip->price_reteal;?>" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Komisi</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="komisi" id="komisi" value="<?php echo $trip->komisi;?>" required/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3">Harga Publish</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="harga" id="harga" value="<?php echo $trip->price;?>" required disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-3"></label>
                        <div class="col-lg-9">
                            <!-- <input type="file" class="form-control" name="upload" id="upload" required/> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="panel panel-default"> 
                        <div class="panel-heading">Gambar Yang Dipublish</div> 
                            <div class="panel-body"> 
                                <div class="flot-chart">
                                <?php 
                                    $id = $trip->id;
                                    $query = $this->db->query('SELECT * FROM tour_trip_image WHERE id_tour_trip ='.$id);
                                    $image = $query->result_array();
                                ?>
                                    <?php foreach($image as $image){?>
                                    
                                    <div class="col-lg-4"><a class="btn btn-danger" style="margin: 83px 122px; position: absolute;" onclick="deleteImage(<?php echo $image['id'];?>,'<?php echo $id?>')">Delete </a>
                                        <img src="<?php echo base_url($image['image']); ?>" class="img-thumbnail" alt="Cinque Terre" width="304px" height="250px" style="height:200px;width:300px"> 
                                    </div>
                                    <?php } ?>

                                </div>  
                            </div>   
                        </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-12"><b>Rencana Perjalanan</b></label>
                        <?php 
                            $id = $trip->id;
                            $query = $this->db->query('SELECT * FROM tour_trip_intenerary WHERE id_tour_trip ='.$id. ' ORDER BY hari');
                            $intenerary = $query->result_array();
                        ?>
                        <?php 
                            foreach($intenerary as $int){
                        ?>
                            <input type="hidden" id="id_tour_trip" value="<?php echo $id; ?>">
                            <div id="view_intenerary">
                                <div class="col-lg-6"><a class="pull-right" onclick="deleteAlat(<?php echo $int['id']; ?>,'<?php echo $id;?>')"> <i style="margin: 13px;" class="fa fa-close"></i> </a>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Hari Ke - <?php echo $int['hari'];?>
                                        </div>
                                        <div class="panel-body">
                                            <p><?php echo $int['description'];?></p>
                                        </div>
                                    </div>
                                </div>           
                            </div>
                        <?php } ?>
                        <div id="update_view_intenerary">
                        </div>
                        <div class="col-lg-12">
                            <a onclick="addIntenerary()" id="click" class="btn btn-success">
                                <i class="fa fa-plus"></i> Tambahkan
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                    <label class="col-lg-12">Description</label>
                    <br>
                        <div class="col-lg-12">
                            <textarea required name="description" class="form-control summernote" id="description" cols="20" rows="15"><?php echo $trip->description;?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                        <div class="panel panel-default"> 
                            <div class="panel-heading">Fasilitas</div> 
                                <div class="panel-body"> 
                                    <div class="flot-chart">

                                        <div class="row col-lg-6">
                                            <label class="col-lg-12"><b>Fasilitas Yang Termasuk</b></label>
                                            <br>
                                            <div class="col-lg-12 example-two">
                                                <input class="form-control" id="fasilities_include" size="23" value=<?php echo $trip->facilities_include;?> data-role="tagsinput"/>
                                            </div>
                                        </div>

                                        <div class="row col-lg-6">
                                            <label class="col-lg-12"><b>Fasilitas Tidak Termasuk</b></label>
                                            <br>
                                            <div class="col-lg-12 example-two">
                                                <input class="form-control" id="fasilities_exclude" size="23" value=<?php echo $trip->facilities_exclude;?> data-role="tagsinput"  />
                                            </div>
                                        </div>
                                    </div>  
                                </div> 
                                <i>Ketik fasilitas yang akan diinput lalu tekan "Enter"</i>  
                            </div>
                        </div>
                </div>
            <br>
            
        </div>

    </div>
    <div class="panel-footer text-right">
        <button type="submit" id="submit" class="btn btn-primary">
            Simpan
        </button>

        <button type="button" id="back" class="btn btn-danger">
            Kembali
        </button>
    </div>
</div>
</form>

<script>
    $(document).ready(function () {
        $('.upload_intenerary').summernote({
            height: 150
        });
        $('#upload_intenerary').summernote('disable');
    });
        

</script>
