<!DOCTYPE html>
<html lang="<?php echo $this->session->userdata('lang') ?>">

<head>

	<!-- // metadata // -->
	<?php echo $template['metas']; ?>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- \\ metadata \\ -->

    <title><?php echo $template['title']; ?></title>
    <link rel="icon" href="<?php echo assets_url('img/logo-sm-b.png') ?>" />
   
	<!-- // css // -->
	<?php echo $template['css']; ?>
    <link rel="stylesheet" href="<?php echo css_url('home/jquery-ui.css') ?>">
	<link rel="stylesheet" href="<?php echo css_url('home/idangerous.swiper.css') ?>" />
	<link rel="stylesheet" href="<?php echo css_url('home/owl.carousel.css') ?>" />
	<link rel="stylesheet" href="<?php echo css_url('home/style.css') ?>" />

	<!-- // bootstrap // -->
	<link rel="stylesheet" href="<?php echo css_url('home/bootstrap.min.css') ?>">
	<!-- \\ bootstrap \\ -->

	<!-- \\ css \\ -->

	<!-- // fonts // -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,600,700,800' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,600' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css' />
    <!-- \\ fonts \\ -->
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo bower_url('animate.css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('simple-line-icons/css/simple-line-icons.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('select2/dist/css/select2.min.css') ?>">
    <link href="<?php echo bower_url('bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo bower_url('bootstrap-tagsinput/src/bootstrap-tagsinput.css') ?>">

    <!-- Custom CSS -->
	<!-- Custom Fonts -->
    <link href="<?php echo bower_url('font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
          type="text/css">


    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo bower_url('jquery/dist/jquery.min.js') ?>"></script>

            <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo bower_url('jquery/dist/jquery.min.js') ?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo bower_url('bootstrap/dist/js/bootstrap.min.js') ?>"></script>

        <script src="<?php echo assets_url('js/login/ui-nav.js') ?>"></script>
        <script src="<?php echo assets_url('js/login/ui-toggle.js') ?>"></script>
        <script src="<?php echo assets_url('js/login/ui-client.js') ?>"></script>

        <!--vue js-->
        <script src="<?php echo bower_url('vue/dist/vue.js'); ?>"></script>

        <!--blockUI-->
        <script src="<?php echo bower_url('blockUI/jquery.blockUI.js'); ?>"></script>

        <!--jquery validation-->
        <script src="<?php echo bower_url('jquery-validation/dist/jquery.validate.min.js') ?>"></script>
        <script src="<?php echo bower_url('jquery-validation/src/localization/messages_id.js') ?>"></script>

        <!--toastr-->
        <link rel="stylesheet" href="<?php echo bower_url('toastr/toastr.min.css'); ?>">
        <script src="<?php echo bower_url('toastr/toastr.min.js'); ?>"></script>

		<script type="text/javascript">
			var site_url = '<?php echo site_url()?>/';
			var base_url = '<?php echo base_url()?>/';
		</script>
	
	<?php echo $template['js_header']; ?>
</head>

<body class="index-page">

	<!-- // header // -->
	<header id="top">

		<!-- // header b // -->
		<div class="header-b">

			<!-- // mobile menu // -->
			<div class="mobile-menu">
				<nav>
					<ul>
						<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
						<li><a href="<?php echo base_url('front-end/trip/trip') ?>">Trip</a></li>						
						<li><a href="<?php echo base_url('front-end/umroh/umroh') ?>">Umroh</a></li>
						<li><a href="http://media.halallocal.com">Blog</a></li>
						<li><a href="#">Tentang Kami</a></li>
					</ul>
				</nav>	
			</div>
			<!-- \\ mobile menu \\ -->
			
			<!-- // navigation bar // -->
			<div class="wrapper-padding">

				<!-- // logo // -->
				<div class="header-logo">
					<a href="<?php echo base_url('/') ?>">
						<img alt="halal-local-logo" src="<?php echo assets_url('img/logo-c.png') ?>" />
					</a>
				</div>
				<!-- \\ logo \\ -->

				<!-- // navigation items // -->
				<div class="header-right">
					<a href="#" class="menu-btn"></a>
					<nav class="header-nav">
						<ul>
							<li><a href="<?php echo base_url('/') ?>">Beranda</a></li>
							<li><a href="<?php echo base_url('front-end/trip/trip') ?>">Trip</a></li>
							<li><a href="<?php echo base_url('front-end/umroh/umroh') ?>">Umroh</a></li>
							<li><a href="http://media.halallocal.com" target="_blank" >Blog</a></li>
							<li><a href="#">Tentang Kami</a></li>
						</ul>
					</nav>
				</div>
				<div class="clear"></div>
				<!-- \\ navigation items \\ -->

			</div>
			<!-- \\ navigation bar \\ -->

		</div>
		<!-- \\ header b \\ -->

	</header>
	<!-- \\ header \\ -->
	
	<?php echo $template['content']; ?>

    <!-- // footer // -->
	<footer class="footer-a">
		<div class="wrapper-padding">

			<!-- // alamat // -->
			<div class="section">
				<div class="footer-lbl">Kontak Kami</div>
				<div class="footer-adress">
					Alamat: Jalan Banda No. 40,<br />
					Gedung Bale Motekar Lt. 3
				</div>
				<div class="footer-email">E-mail: info@halallocal.com</div>

				<!-- //
				<div class="footer-phones">Telephones: +1 777 55-32-21</div>
				<div class="footer-skype">Skype: angelotours</div>
				\\ -->

			</div>
			<!-- \\ alamat \\ -->

			<!-- // rekomendasi trip // -->
			<div class="section">
				<div class="footer-lbl">Rekomendasi Trip</div>
				<div class="footer-tours">
					<?php 
						$data = $this->beranda_model->diskon_trip();
						foreach($data as $diskon){
					
						$images = $this->beranda_model->view_images($diskon->id);	
						$firstImage = "";
						if(!isset($images->image)){
							$firstImage = assets_url('img/no-image.jpg');
						}else{
							$firstImage = base_url($images->image);
						}
					?>
					<!-- // -->
					<div class="footer-tour">
						<div class="footer-tour-l">
							<a href="<?php echo base_url('front-end/trip/trip/detail/').$diskon->id; ?>">
								<img alt="perjalanan-lain-image" style="width: 80px; height: 60px;" src="<?php echo $firstImage; ?>">
							</a>
						</div>
						<div class="footer-tour-r">
							<div class="footer-tour-a"><?php echo $diskon->title; ?></div>
							<div class="footer-tour-c"><?php echo $diskon->komisi; ?></div>
						</div>
						<div class="clear"></div>
					</div>
					<?php
						}
					?>
					<!-- \\ -->
				</div>		
			</div>
			<!-- \\ rekomendasi trip \\ -->

			<!-- // rekomendasi umroh // -->
			<div class="section">
				<div class="footer-lbl">Rekomendasi Umroh</div>
				<div class="footer-tours">
					<?php 
						$data = $this->beranda_model->diskon_umroh();
						foreach($data as $diskon){
					
						$images = $this->beranda_model->view_images_umroh($diskon->id);	
						$firstImage = "";
						if(!isset($images->image)){
							$firstImage = assets_url('img/no-image.jpg');
						}else{
							$firstImage = base_url($images->image);
						}
					?>
					<!-- // -->
					<div class="footer-tour">
						<div class="footer-tour-l">
							<a href="<?php echo base_url('front-end/umroh/umroh/detail/').$diskon->id; ?>">
								<img alt="perjalanan-lain-image" style="width: 80px; height: 60px;" src="<?php echo $firstImage; ?>">
							</a>
						</div>
						<div class="footer-tour-r">
							<div class="footer-tour-a"><?php echo $diskon->title; ?></div>
							<div class="footer-tour-c"><?php echo $diskon->komisi; ?></div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- \\ -->
					<?php
						}
					?>
				</div>
			</div>
			<!-- \\ rekomendasi umroh \\ -->

			<!-- // berlangganan // -->
			<div class="section">
				<div class="footer-lbl">Dapatkan Promo Terbaru Kami </div>
				<div class="footer-subscribe">
					<div class="footer-subscribe-a">
						<input type="text" placeholder="email anda" value="" />
					</div>
				</div>
				<button class="footer-subscribe-btn">Berlangganan</button>
			</div>
			<!-- \\ berlangganan \\ -->

		</div>
		<div class="clear"></div>
	</footer>

	<footer class="footer-b">
		<div class="wrapper-padding">
			<div class="footer-left">© Copyright 2017 by PT. Astrajingga Inovasi Digital. All rights reserved.</div>
			<div class="footer-social">
				<a href="#" class="footer-twitter"></a>
				<a href="https://www.facebook.com/halalocals/" class="footer-facebook" target="_blank"></a>
				<a href="#" class="footer-vimeo"></a>
				<a href="#" class="footer-pinterest"></a>
				<a href="https://www.instagram.com/halallocals/" class="footer-instagram" target="_blank"></a>
			</div>
			<div class="clear"></div>
		</div>
	</footer>
	<!-- \\ footer \\ -->

	<?php echo $template['js_footer']; ?>

	<!-- // script // -->
	<!-- // bootstrap // -->
	<script src="<?php echo assets_url('js/jquery-3.2.1.min.js')?>"></script>
    <script src="<?php echo assets_url('js/popper.min.js') ?>"></script>
    <script src="<?php echo assets_url('js/bootstrap.min.js') ?>"></script>
	<!-- \\ bootstrap \\ -->

	<script src="<?php echo assets_url('js/jquery-1.11.3.min.js') ?>"></script>
    <script src="<?php echo assets_url('js/idangerous.swiper.js') ?>"></script>
    <script src="<?php echo assets_url('js/slideInit.js') ?>"></script>
	<script src="<?php echo assets_url('js/owl.carousel.min.js') ?>"></script>
	<script src="<?php echo assets_url('js/bxSlider.js') ?>"></script>
	<script src="<?php echo assets_url('js/jqeury.appear.js') ?>"></script>
    <script src="<?php echo assets_url('js/custom.select.js') ?>"></script>
    <script src="<?php echo assets_url('js/jquery-ui.min.js') ?>"></script>
	<script src="<?php echo assets_url('js/script.js') ?>"></script>
	

    <!-- // gallery slider // -->
    <script>
		$(document).ready(function(){
			'use strict';
			$('.review-ranger').each(function(){    
			var $this = $(this);
			var $index = $(this).index();
			if ( $index=='0' ) {
				var $val = '3.0'
			} else if ( $index=='1' ) {
				var $val = '3.8'
			} else if ( $index=='2' ) {
				var $val = '2.8'
			} else if ( $index=='3' ) {
				var $val = '4.8'
			} else if ( $index=='4' ) {
				var $val = '4.3'
			} else if ( $index=='5' ) {
				var $val = '5.0'
			}
			$this.find('.slider-range-min').slider({
				range: "min",
				step: 0.1,
				value: $val,
				min: 0.1,
				max: 5.1,
				create: function( event, ui ) {
				$this.find('.ui-slider-handle').append('<span class="range-holder"><i></i></span>');
				},
				slide: function( event, ui ) {
				$this.find(".range-holder i").text(ui.value);
				}
			});
			$this.find(".range-holder i").text($val);
			});

			$('#reasons-slider').bxSlider({
				infiniteLoop: true,
				speed: 500,
			mode:'fade',
				minSlides: 1,
				maxSlides: 1,
				moveSlides: 1,
				auto: true,
				slideMargin: 0      
			});
		
			$('#gallery').bxSlider({
				infiniteLoop: true,
				speed: 500,
				slideWidth: 108,
				minSlides: 1,
				maxSlides: 6,
				moveSlides: 1,
				auto: false,
				slideMargin: 7      
			});
		});
	</script>
	<!-- \\ gallery slider \\ -->

	<!-- // alert // -->
	<script>
		$(document).ready(function() {
			$('#book-now').click(function() {
				alert("Terima kasih telah memilih Halal Local. Tunggu follow-up dari kami melalui WhatsApp.");
			});
		});
	</script>
	<!-- \\ script \\ -->
	
</body>
</html>
