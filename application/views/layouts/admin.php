<?php
$name = '';
$role = '';
if ($this->auth->loggedin()) {
    $name = $auth_user['username'];
    $image = $auth_user['image'];
    $role = $auth_user['role_name'];
    if (strlen(trim($name)) == 0) {
        $name = $auth_user['username'];
    }
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->session->userdata('lang') ?>">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $template['metas']; ?>

    <title><?php echo $template['title']; ?></title>


    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo bower_url('animate.css/animate.min.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('simple-line-icons/css/simple-line-icons.css') ?>">
    <link rel="stylesheet" href="<?php echo bower_url('select2/dist/css/select2.min.css') ?>">
    <link href="<?php echo bower_url('bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo bower_url('bootstrap-tagsinput/src/bootstrap-tagsinput.css') ?>">

    <!-- Custom CSS -->

    <link href="<?php echo css_url('login/font') ?>" rel="stylesheet">
    <link href="<?php echo css_url('login/app') ?>" rel="stylesheet">

    <?php echo $template['css']; ?>

    <!-- Custom Fonts -->
    <link href="<?php echo bower_url('font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
          type="text/css">


    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo bower_url('jquery/dist/jquery.min.js') ?>"></script>

            <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo bower_url('jquery/dist/jquery.min.js') ?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo bower_url('bootstrap/dist/js/bootstrap.min.js') ?>"></script>

        <script src="<?php echo assets_url('js/login/ui-nav.js') ?>"></script>
        <script src="<?php echo assets_url('js/login/ui-toggle.js') ?>"></script>
        <script src="<?php echo assets_url('js/login/ui-client.js') ?>"></script>

        <!--vue js-->
        <script src="<?php echo bower_url('vue/dist/vue.js'); ?>"></script>

        <!--blockUI-->
        <script src="<?php echo bower_url('blockUI/jquery.blockUI.js'); ?>"></script>

        <!--jquery validation-->
        <script src="<?php echo bower_url('jquery-validation/dist/jquery.validate.min.js') ?>"></script>
        <script src="<?php echo bower_url('jquery-validation/src/localization/messages_id.js') ?>"></script>

        <!--toastr-->
        <link rel="stylesheet" href="<?php echo bower_url('toastr/toastr.min.css'); ?>">
        <script src="<?php echo bower_url('toastr/toastr.min.js'); ?>"></script>
    <script type="text/javascript">
        var site_url = '<?php echo site_url()?>/';
        var base_url = '<?php echo base_url()?>/';
    </script>
    <?php echo $template['js_header']; ?>
</head>

<body>
<div class="app app-header-fixed app-aside-fixed">
    <header id="header" class="app-header navbar" role="menu">
        <!-- navbar header -->
        <div class="navbar-header bg-dark">
            <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
                <i class="glyphicon glyphicon-cog"></i>
            </button>
            <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
                <i class="glyphicon glyphicon-align-justify"></i>
            </button>
            <!-- brand -->
            <a href="<?php echo site_url('/') ?>" class="navbar-brand text-lt">


                <small>Halal Local</small>
            </a>
            <!-- / brand -->
        </div>
        <!-- / navbar header -->

        <!-- navbar collapse -->
        <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
            <!-- buttons -->
            <div class="nav navbar-nav hidden-xs">
                <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
                    <i class="fa fa-dedent fa-fw text"></i>
                    <i class="fa fa-indent fa-fw text-active"></i>
                </a>
                <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
                    <i class="icon-user fa-fw"></i>
                </a>
            </div>
            <!-- / buttons -->

            <!-- link and dropdown -->
            <ul class="nav navbar-nav hidden-sm">

            </ul>
            <!-- / link and dropdown -->

            <!-- search form -->

            <!-- / search form -->

            <!-- nabar right -->
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="<?= base_url($image); ?>" alt="...">
              </span>
                        <span class="hidden-sm hidden-md"><?= $name ?></span>
                    </a>
                    <!-- dropdown -->
                    <ul class="dropdown-menu animated fadeInRight w">
                       
                       
                        <li>
                            <a href="<?php echo site_url('auth/user/edit/' . $this->session->userdata('id'))?>">Profile</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url('auth/logout') ?>">Logout</a>
                        </li>
                    </ul>
                    <!-- / dropdown -->
                </li>
            </ul>
            <!-- / navbar right -->
        </div>
        <!-- / navbar collapse -->
    </header>
    <aside id="aside" class="app-aside hidden-xs bg-dark">
        <div class="aside-wrap">
            <div class="navi-wrap">
                <!-- user -->
                <div class="clearfix hidden-xs text-center hide" id="aside-user">
                    <div class="dropdown wrapper">
                        <a href="app.page.profile">
                <span class="thumb-lg w-auto-folded avatar m-t-sm">
                  <img src="<?= assets_url('img/a0.jpg') ?>" class="img-full" alt="...">
                </span>
                        </a>
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
                <span class="clear">
                  <span class="block m-t-sm">
                    <strong class="font-bold text-lt"><?= $name; ?></strong>
                    <b class="caret"></b>
                  </span>
                  <span class="text-muted text-xs block"><?php echo $_SESSION['role_name']; ?></span>
                </span>
                        </a>
                        <!-- dropdown -->
                        <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                            <li>
                                <a href="<?php echo site_url('auth/user/edit/' . $this->session->userdata('id'))?>">Profile</a>
                            </li>
                            
                            <li class="divider"></li>
                            <li>
                                <a href="page_signin.html">Logout</a>
                            </li>
                        </ul>
                        <!-- / dropdown -->
                    </div>
                    <div class="line dk hidden-folded"></div>
                </div>
                <!-- / user -->

                <!-- nav -->
                <nav ui-nav class="navi clearfix">
                    <ul class="nav">
                        <?php

                        function set_active($menus, $curr_uri, $acl)
                        {
                            foreach ($menus as $index => $menu) {
                                $is_active = false;
                                $is_allowed = false;
                                $has_children = isset($menu['children']) and is_array($menu['children']);
                                if ($has_children) {
                                    $menus[$index]['children'] = set_active($menus[$index]['children'], $curr_uri, $acl);
                                    foreach ($menus[$index]['children'] as $menu_item) {
                                        if ($menu_item['is_active']) {
                                            $is_active = $is_active || true;
                                        }
                                        if ($menu_item['is_allowed']) {
                                            $is_allowed = $is_allowed || true;
                                        }
                                    }
                                } else {
                                    $is_active = strpos($curr_uri, $menu['uri']) === 0;
                                    $is_allowed = !isset($menu['uri']) || $acl->is_allowed($menu['uri']);
                                }
                                $menus[$index]['is_active'] = $is_active;
                                $menus[$index]['is_allowed'] = $is_allowed;
                            }
                            return $menus;
                        }

                        function display_menu_item($from_children = false, $menu, $curr_uri)
                        {
                            if (empty($curr_uri))
                                $curr_uri = 'home';
                            $is_active = (isset($menu['is_active']) && $menu['is_active']);
                            $is_allowed = (isset($menu['is_allowed']) && $menu['is_allowed']);

                            $has_children = isset($menu['children']) and is_array($menu['children']);

                            if ($is_allowed) {

                                echo '<li' . ($is_active ? ' class="active"' : ' ') . '>';

                                echo '<a class="auto" href="' . ((isset($menu['uri']) and !empty($menu['uri'])) ? site_url($menu['uri']) : '#') . '"' . '>';
                                echo '<span class="pull-right text-muted">' .
                                    '<i class="fa fa-fw fa-angle-right text"></i>' .
                                    '<i class="fa fa-fw fa-angle-down text-active"></i>' .
                                    '</span>';
                                if (isset($menu['icon']) && !empty($menu['icon']))
                                    echo '<i class="' . $menu['icon'] . '"></i>';
                                echo '<span>' . $menu['title'] . '</span>';
                                echo '</a>';
                                if ($has_children) {
                                    echo '<ul class="nav nav-sub dk">';
                                    foreach ($menu['children'] as $menu_item) {
                                        display_menu_item($has_children, $menu_item, $curr_uri);
                                    }
                                    echo '</ul>';
                                }
                            }
                        }

                        $curr_uri = $this->uri->uri_string();
                        if (empty($curr_uri))
                            $curr_uri = 'home';
                        $this->load->config('navigation');
                        $navigation = set_active($this->config->item('navigation'), $curr_uri, $this->acl);
                        foreach ($navigation as $menu) {
                            display_menu_item(false, $menu, $curr_uri);
                        }
                        ?>
                    </ul>
                </nav>
                <!-- nav -->


            </div>
        </div>
    </aside>
    <div id="content" class="app-content" role="main">
        <div class="bg-default app-header-fixed lter b-b wrapper-md" style="overflow: auto">
            <h1 class="m-n font-thin h3 pull-left">
                <div><?php echo (isset($page_title)) ? $page_title : $template['title']; ?> <br>
                    <small><?php echo (isset($page_sub_title)) ? $page_sub_title : ''; ?></small>
                </div>
            </h1>
            <div class="pull-right">
              <?php echo (isset($page_icon)) ? $page_icon : ''; ?>
            </div>
            <div class="pull-right">
            </div>
        </div>
        <div class="app-content-body " style="padding:0;">
            <div class="lter b-b wrapper-md ">
                <?php echo $template['content']; ?>
            </div>
        </div>
    </div>
</div>

<!-- /#wrapper -->

<script src="<?php echo bower_url('lodash/dist/lodash.min.js') ?>"></script>

    <script src="<?php echo bower_url('moment/min/moment.min.js') ?>"></script>
    <script src="<?php echo bower_url('moment/min/moment-with-locales.min.js') ?>"></script>
    <script src="<?php echo bower_url('moment/locale/id.js') ?>"></script>

    <script src="<?php echo bower_url('numeral/min/numeral.min.js') ?>"></script>
    <script src="<?php echo bower_url('jquery.inputmask/dist/min/inputmask/inputmask.min.js') ?>"></script>
    <script src="<?php echo bower_url('jquery-maskmoney/dist/jquery.maskMoney.min.js')?>"></script>
    <!--custom utils-->
    <!-- <script src="<?php echo js_url('utils/upload')?>" type="text/javascript"></script> -->
    <script src="<?php echo bower_url('bluebird/js/browser/bluebird.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo bower_url('sweetalert/dist/sweetalert.min.js')?>"></script>
    <script type="text/javascript">
      moment().locale('id');
    </script>


    <script type="text/javascript">

      
      // toastr options
      toastr.options.closeButton = true;
      toastr.options.preventDuplicates = true;
      // toastr.options.timeOut = 30;
      // toastr.options.extendedTimeOut = 50;

      function redirectAfterToastr(link) {
        toastr.options.onHidden = function() {
          window.location.href = site_url + link;
        }
        toastr.options.onclick = function() {
          window.location.href = site_url + link;
        }
        toastr.options.onCloseClick = function() {
          window.location.href = site_url + link;
        }
      }

      $('iframe').load(function(){
        $('.progress').hide();
      });


      // load component block UI
         $(document).ajaxStart(function () {
             $.blockUI({
                 message: '<span class="text-semibold">Loading...</span>',
                 overlayCSS: {
                     backgroundColor: '#000',
                     opacity: 0.6,
                     cursor: 'wait'
                 },
                 css: {
                     border: 0,
                     padding: '10px 15px',
                     color: '#fff',
                     '-webkit-border-radius': 2,
                     '-moz-border-radius': 2,
                     backgroundColor: '#333',
                     'z-index': 2000
                 }
             });
         }).ajaxStop(function () {
             $.unblockUI();
         });


      // auto validate email
      $.validator.methods.email = function(value, element) {
        return this.optional(element) || /[a-z]+@[a-z]+\.[a-z]+/.test(value);
      };
    </script>

    <?php echo $template['js_footer']; ?>

  </body>

  </html>