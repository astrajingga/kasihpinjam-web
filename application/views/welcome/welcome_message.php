<!-- // main-cont // -->
<div class="main-cont">
	<div class="mp-slider search-only">

		<!-- // slider // -->
		<div class="mp-slider-row slim-slider">
			<div class="swiper-container">
				<a href="#" class="arrow-left"></a>
				<a href="#" class="arrow-right"></a>
				<div class="swiper-pagination"></div>				
				<div class="swiper-wrapper">
					<div class="swiper-slide"> 
						<div class="slide-section" style="background:url(<?php echo assets_url('img/slider-irwansyah.jpg') ?>) center top no-repeat;">
							<div class="mp-slider-lbl">Bersama Membangun Usaha Kecil Menengah</div>
							<!-- <div class="mp-slider-lbl-a">Bersama Membangun Usaha Kecil Menengah</div> -->
							<div class="mp-slider-btn">
								<a href="#" class="btn-a">Daftar</a>
								<a href="#" class="btn-a">Masuk</a>
							</div>
						</div>      
					</div>
					<div class="swiper-slide"> 
						<div class="slide-section slide-b" style="background:url(<?php echo assets_url('img/slider-ricky.jpg') ?>) center top no-repeat;">
							<div class="mp-slider-lbl">UKM Berkualitas Imbal Balik Sampai 20%</div>
							<!-- <div class="mp-slider-lbl-a">UKM Berkualitas Imbal Balik Sampai 20%</div> -->
							<div class="mp-slider-btn">
								<a href="#" class="btn-a">Daftar</a>
								<a href="#" class="btn-a">Masuk</a>
							</div>		
						</div>
					</div>
					<div class="swiper-slide"> 
						<div class="slide-section slide-b" style="background:url(<?php echo assets_url('img/slider-ricky.jpg') ?>) center top no-repeat;">
							<div class="mp-slider-lbl">Proses Pinjaman Bisnis Mudah, Cepat, Efisien</div>
							<!-- <div class="mp-slider-lbl-a"></div> -->
							<div class="mp-slider-btn">
								<a href="#" class="btn-a">Daftar</a>
								<a href="#" class="btn-a">Masuk</a>
							</div>		
						</div>
					</div>
				</div>
			</div>
		<!-- \\ slider \\ -->
		</div>	
	</div>	
		<div class="clear"></div>	
	 </div>
	</div>
	
	
	<!-- \\ featured in \\ -->
	<!-- <div class="logo-web">
		<div class="partners-wrapper">
			<header class="fly-in page-lbl">
				<b>Featured In</b>
				<p>
					Kami telah diliput oleh :
				</p>
			</header>  
			<div class="partners fly-in">
				<div class="inline">
					<div class="block">
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/1.jpg'); ?>" /></a>
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/2.jpg'); ?>"/></a>
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/3.jpg'); ?>"/></a>
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/4.jpg'); ?>"/></a>
					</div>
					<div class="block">
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/5.jpg'); ?>"/></a>
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/6.jpg'); ?>"/></a>
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/7.jpg'); ?>"/></a>
							<a href="#"><img alt="featured-in-image" src="<?php echo assets_url('img/Featured In/8.jpg'); ?>"/></a>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<!-- // featured in // -->
</div>
<!-- \\ main-cont \\ -->

