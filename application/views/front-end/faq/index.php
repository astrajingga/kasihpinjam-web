<div class="main-cont">  	
	
		<div class="inner-page">	
			<!-- <div class="inner-breadcrumbs"> -->
				<!-- <div class="content-wrapper">
					<div class="page-title">Bantuan</div>
					<div class="breadcrumbs">
						<a href="#">Home</a> / <span>Bantuan</span>
					</div>
					<div class="clear"></div>
				</div>		 -->
			<!-- </div> -->
			
		</div>
	
		<div class="typography">
			<div class="content-wrapper">
				<div class="shortcodes-block">
					<!-- // tabs // -->
					<div class="tabs">
						<div class="offer-slider">
							<div class="tabs-type-a tabs-block">
								<nav class="tabs-nav">
									<ul>
										<li><a class="active" href="#">TENTANG HALAL LOCAL</a></li>
										<li><a href="#">FITUR - FITUR HALAL LOCAL</a></li>
										<li><a href="#">PENCARIAN PAKET</a></li>
										<li><a href="#">CARA MENDAPATKAN KUPON</a></li>
										<li><a href="#">PEMBATALAN DAN PERUBAHAN DATA</a></li>
									</ul>
									<div class="clear"></div>
								</nav>
								<div class="tabs-content">
									<div class="tabs-content-i">
										<!-- // toggle // -->
										<div class="toggle">
										<div class="typography-heading">TENTANG HALAL LOCAL</div>	
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Apa itu Halal Local ?</b></div>
															<div class="toggle-txt" style="display: block;">Halal Local adalah sebuah website marketplace yang memberikan informasi dan kebutuhan data secara spesifik hampir ke seluruh tujuan wisata di dunia kepada penggunanya. Kami berusaha untuk memberikan informasi dan layanan yang dibutuhkan bagi wisatawan muslim, mulai dari Restoran Halal hingga Lokasi Masjid terdekat.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										<!-- // -->
										<!-- <div class="toggle-i">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl">crambled it to make a type specimen</div>
															<div class="toggle-txt">Voluptatem quia voluptas sit aspern atur aut odit aut fugit, sed quia cons equuntur magni dolores eos qui voluptatem sequi nesciunt.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div> -->
										<!-- \\ -->	
										</div>
									<!-- \\ toggle \\ -->
									</div>
									<div class="tabs-content-i">
										<!-- // toggle // -->
										<div class="toggle">
										<div class="typography-heading">FITUR-FITUR HALAL LOCAL</div>
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Fitur apa saja yang ada di aplikasi Halal local?</b></div>
															<div class="toggle-txt" style="display: block;">Halal Local berusaha menciptakan fitur - fitur unggulan untuk membantu para wisatawan muslim merencanakan perjalannya seperti Paket Tour Wisata dan Umrah yang dilengkapi fitur pendukung berupa Restoran Halal dan lokasi mesjid beserta jadwal adzan dan Hotel halal di dekat area tempat wisata.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Apa itu Tour Operator ?</b></div>
															<div class="toggle-txt" style="display: block;">User yang khusus mengatur dan menyelenggarakan perjalanan dan persinggahan orang – orang termasuk kelengkapan perjalanan, dari suatu tempat ke tempat lain, baik dari dalam negeri  ke luar negeri atau dalam negeri itu sendiri.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Siapa saja yang bisa menjadi Tour Opertor ?</b></div>
															<div class="toggle-txt" style="display: block;">Jika Anda tertarik untuk bergabung menjadi Tour operator dengan sistem-sistem seperti keagenan, silakan melakukan pendaftaran pada Form Registrasi. Kami akan mengirimkan kabar-kabar terbaru kepada Anda.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->	
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Bagaimana cara menghubungi Halal Local ?</b></div>
															<div class="toggle-txt" style="display: block;">Untuk pertanyaan yang belum terjawab di cara pemakaian maupun FAQ ini, Anda bisa menghubungi Ihram.asia melalui berbagai jalur komunikasi resmi kami di: <b>info@halallocal.com</b> untuk diresponi secara cepat atau melalui telepon ke <b>022-20521999.</b> Lengkapnya dapat dilihat di halaman Hubungi Kami.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->	
										</div>
									<!-- \\ toggle \\ -->
									</div>
									<div class="tabs-content-i">
										<!-- // toggle // -->
										<div class="toggle">
										<div class="typography-heading">PENCARIAN PAKET</div>
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Apa saja paket yang ada di Halal Local?</b></div>
															<div class="toggle-txt" style="display: block;">Pencarian paket dibagi menjadi 3 yaitu <b>paket trip domestik, paket trip internasional</b> dan <b>paket Umrah</b> . User bisa memilih paket wisata berdasarkan minat user dalam fitur “Tour”.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										</div>
									<!-- \\ toggle \\ -->
									</div>
									<div class="tabs-content-i">
										<!-- // toggle // -->
										<div class="toggle">
										<div class="typography-heading">CARA MENDAPATKAN KUPON</div>
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Bagaimana cara mendapatkan kupon Halal Local?</b></div>
															<div class="toggle-txt" style="display: block;">Dapatkan kupon diskon Halal Local dengan cara memilih paket wisata atau umroh lalu isi data di form yang kami sediakan. Setelah melakukan pengisian data, kami akan kirimkan kupon melalui email.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										</div>
									<!-- \\ toggle \\ -->
									</div>
									<div class="tabs-content-i">
										<!-- // toggle // -->
										<div class="toggle">
										<div class="typography-heading">PEMBATALAN DAN PERUBAHAN DATA</div>
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Bagaimana cara membatalkan paket tour/umrah yang sudah saya pesan?</b></div>
															<div class="toggle-txt" style="display: block;">Anda dapat melakukan pembatalan paket trip/umroh dengan menghubungi tour operator sesuai dengan syarat dan ketentuan pembatalan paket trip dari tour operator bersangkutan</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										<!-- // -->
										<div class="toggle-i open">
											<div class="toggle-ia">
												<div class="toggle-ia-a">
													<div class="toggle-ia-l">
														<a href="#" class="toggle-trigger"></a>
													</div>
													<div class="toggle-ia-r">
														<div class="toggle-ia-rb">
															<div class="toggle-lbl"><b>Saya sudah memesan paket tour/umrah dan membayar tanda jadi tetapi ternyata jadwal saya berubah. Bagaimana cara melakukan perubahan paket umrah? </b></div>
															<div class="toggle-txt" style="display: block;">Anda dapat melakukan pengubahan jadwal keberangkatan atau penerbangan dengan menghubungi tour operator bersangkutan.</div>
														</div>
														<div class="clear"></div>
														</div>
													</div>
												<div class="clear"></div>
											</div>
										</div>
										<!-- \\ -->
										</div>
									<!-- \\ toggle \\ -->
									</div>
								</div>
							</div>
						</div>
						
						<div class="clear"></div>
					</div>
					<!-- \\ tabs \\ -->
					
					
					
				</div>
			</div>
		</div>
	
</div>