<!-- // main-cont // -->
<div class="main-cont">
	<div class="body-wrapper">
		<div class="wrapper-padding">
			<div class="two-colls">

				<!-- // sidebar // -->
				<div class="two-colls-left">
					
					<!-- // search result // -->
					<!-- <div class="srch-results-lbl fly-in">
						<span><?php echo $jml; ?>  results found.</span>
					</div>  -->
					<!-- \\ search result \\ -->
					
					<div class="side-block fly-in">
						<div class="side-stars">
							<div class="side-padding">
							<div class="side-lbl">Durasi Tour</div>  
							<div class="checkbox">
								<label>
								<input id="durasi1" style="margin-top:-1px;" type="checkbox" value="1" />
								1 - 2 Hari
								</label>
							</div> 
							<div class="checkbox">
								<label>
								<input id="durasi2" style="margin-top:-1px;" type="checkbox" value="2" />
								3-5 Hari
								</label>
							</div> 
							<div class="checkbox">
								<label>
								<input id="durasi3" style="margin-top:-1px;" type="checkbox" value="3" />
								6 - 9 Hari
								</label>
							</div>
							<div class="checkbox">
								<label>
								<input id="durasi4" style="margin-top:-1px;" type="checkbox" value="4" />
								10+ Hari
								</label>
							</div>          
							</div>
						</div>  
						</div>
						<!-- \\ side \\ -->
						<!-- // side // -->
						<div class="side-block fly-in">
						<div class="side-stars">
							<div class="side-padding">
							<div class="side-lbl">Tour type</div>  
							<div class="checkbox">
								<label>
								<input style="margin-top:-1px;" type="checkbox" value="" />
								International
								</label>
							</div> 
							<div class="checkbox">
								<label>
								<input style="margin-top:-1px;" type="checkbox" value="" />
								Domestik
								</label>
							</div> 
							</div>
						</div>  
						</div>
					<!-- \\ search result \\ -->
					
				</div>
				<!-- \\ sidebar \\ -->
				
				<!-- // content // -->
				<div class="two-colls-right">
					<div class="two-colls-right-b">
						<div class="padding">
							<!-- // sort // -->
							<div class="catalog-head fly-in">
								<label>Urutkan Berdasarkan:</label>
								<div class="search-select">
									<select>
										<option>Diskon</option>
										<option>Terbaru</option>
										<option>Termurah</option>
										<option>Termahal</option>

									</select>
								</div>
								<div class="clear"></div>
							</div>
							<!-- \\ sort \\ -->
							
							<!-- // listing // -->
							<div class="catalog-row grid">

								<?php
									$no = $offset;
									// digunakan untuk mengulangi semua data yang ada di database 
									foreach($data as $trip){ 
										$depature = strtotime($trip->depature_date);
										$return = strtotime($trip->return_date);
										$price = number_format($trip->price,0,',','.');
										$price_reteal = number_format($trip->price_reteal,0,',','.');

										$images = $this->beranda_model->view_images($trip->id);	
										$imageView = "";
										if(!isset($images->image)){
										 $imageView = assets_url('img/no-image.jpg');
											
										}else{
										 $imageView = base_url($images->image);
										}
									++$no;
								?>

								<!-- // -->
								
								<div class="offer-slider-i catalog-i tour-grid fly-in">
									<a href="<?php echo base_url('front-end/trip/trip/detail/'.$trip->id) ?>" class="offer-slider-img">
										<img alt="trip-listing-image" style="width: 100%; height: 175px;" src="<?php echo $imageView; ?>">
										<span class="offer-slider-overlay">
											<span class="offer-slider-btn">view details</span><span></span>
										</span>
									</a>
									<div class="offer-slider-txt">
										<div class="offer-slider-link"><a href="<?php echo base_url('front-end/trip/trip/detail/'.$trip->id) ?>"><?php echo $trip->title; ?></a></div>
										<div class="offer-slider-l">
											<div class="offer-slider-location-a"><?php echo date('d M Y', $depature); ?>  - <?php echo date('d M Y', $return); ?></div></div>
										<div class="offer-slider-r align-right">
											<span style="font-family: 'Raleway';font-size: 10px;font-weight: 600 !important; text-transform: uppercase;color: #000000;">RP</span>		
											<strike style="font-size: 22px;"><?php echo $price_reteal; ?></strike>
										</div>
										<div class="offer-slider-r">
											<span>RP</span>
											<b><?php echo $price; ?></b>
										</div>
										
										<!-- <div class="offer-slider-lead">Voluptatem quia voluptas sit sper natur aut odit aut fugit</div> -->
										<!-- <a href="#" class="cat-list-btn">Select</a> -->
									</div>
								</div>

								<?php } ?>
								<!-- \\ -->

							</div>
							<!-- \\ listing \\ -->

							<div class="clear"></div>
							
							<!-- // pagination // -->
							<div class="pagination">
								<!-- <a class="active" href="#">1</a>
								<a href="#">2</a>
								<a href="#">3</a>
								<div class="clear"></div> -->
								<?php 
								// foreach($halaman as $halaman){
									echo $halaman;
								// }
								?>
							</div>
							<!-- \\ pagination \\ -->       
						</div>
					</div>
					<br class="clear" />
				</div>
				<!-- \\ content \\ -->

			</div>
			<div class="clear"></div>
		
		</div>	
	</div>  
</div>
<!-- \\ main-cont \\ -->

<!-- <script>
	$(document).ready(function(){
		var durasi1 = "halallocal";
		var durasi2 = "halallocal";
		var durasi3 = "halallocal";
		var durasi4 = "halallocal";
		
		$("#durasi1" ).on("click", function() {
			if($('#durasi1').prop('checked')){
				durasi1 = $('#durasi1').val();
			}else if($('#durasi2').prop('checked')){
				durasi2 = $('#durasi2').val();
			}else if($('#durasi3').prop('checked')){
				durasi3 = $('#durasi3').val();
			}else if($('#durasi4').prop('checked')){
				durasi4 = $('#durasi4').val();
			}
			alert(durasi1 + durasi2 + durasi3 + durasi4);
		});
		$("#durasi2" ).on("click", function() {
			if($('#durasi1').prop('checked')){
				durasi1 = $('#durasi1').val();
			}else if($('#durasi2').prop('checked')){
				durasi2 = $('#durasi2').val();
			}else if($('#durasi3').prop('checked')){
				durasi3 = $('#durasi3').val();
			}else if($('#durasi4').prop('checked')){
				durasi4 = $('#durasi4').val();
			}
			
		alert(durasi1 + durasi2 + durasi3 + durasi4);
		});
		// $("#durasi3" ).on("click", function() {
		// 	if($('#durasi1').prop('checked')){
		// 		durasi1 = $('#durasi1').val();
		// 	}else if($('#durasi2').prop('checked')){
		// 		durasi2 = $('#durasi2').val();
		// 	}else if($('#durasi3').prop('checked')){
		// 		durasi3 = $('#durasi3').val();
		// 	}else if($('#durasi4').prop('checked')){
		// 		durasi4 = $('#durasi4').val();
		// 	}
			
		// alert(durasi1 + durasi2 + durasi3 + durasi4);
		// });
		// $("#durasi4" ).on("click", function() {
		// 	if($('#durasi1').prop('checked')){
		// 		durasi1 = $('#durasi1').val();
		// 	}else if($('#durasi2').prop('checked')){
		// 		durasi2 = $('#durasi2').val();
		// 	}else if($('#durasi3').prop('checked')){
		// 		durasi3 = $('#durasi3').val();
		// 	}else if($('#durasi4').prop('checked')){
		// 		durasi4 = $('#durasi4').val();
		// 	}
			
		// alert(durasi1 + durasi2 + durasi3 + durasi4);
		// });
		
	});

</script> -->