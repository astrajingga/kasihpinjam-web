
<!-- // book modal // -->
<div class="modal fade text-dark" id="book-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contactModalTitle">
                    Silahkan isi data diri Anda
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" action="<?php echo base_url('booking/tambah/')?>" method="get" enctype="multipart/form-data">                                      
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="hidden" name="id_tour" id="id_tour" value="<?php echo $detail_trip->id; ?> ">
                        <input
                            type="text"
                            name="nama"
                            id="nama"
                            class="form-control form-control-lg" 
                            placeholder=""
                            pattern="[a-zA-z ]+"
                            required
                            oninvalid="setCustomValidity('Inputan nama invalid. Silahkan coba lagi.')"
                            oninput="setCustomValidity('')">  
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            class="form-control form-control-lg"
                            placeholder=""
                            pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*"
                            required
                            oninvalid="setCustomValidity('Inputan email invalid. Silahkan coba lagi.')"
                            oninput="setCustomValidity('')">
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor Handphone / WhatsApp</label>
                        <input
                            type="tel"
                            name="telp"
                            id="telp"
                            class="form-control form-control-lg"
                            placeholder=""
                            required
                            oninvalid="setCustomValidity('Inputan nomor handphone / WhatsApp invalid. Silahkan coba lagi.')"
                            oninput="setCustomValidity('')">
                    </div>
                    <div class="form-group">
                        <label for="kupon">Jumlah Kupon</label>
                        <select class="form-control" id="qty"  name="qty">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div> 
                    <button type="submit" class="book-btn" >
                        PESAN VOUCHER SEKARANG
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- \\ book modal \\ -->

<!-- main-cont -->
<div class="main-cont">
    <div class="body-wrapper">
        <div class="wrapper-padding">
            <div class="sp-page">
                <div class="sp-page-a">
                    <div class="sp-page-l">
                        <div class="sp-page-lb">
                            <div class="sp-page-p">
                                <div class="mm-tabs-wrapper">
                                    <!-- // gallery // -->
                                    <div class="tab-item">
                                        <div class="tab-gallery-big">
                                        <?php
                                            $firstImage = "";
                                            if(!isset($images->image)){
                                                $firstImage = assets_url('img/no-image.jpg');
                                            }else{
                                                $firstImage = base_url($images->image);
                                            }
                                        ?>
                                            <img
                                                class="border"
                                                alt="trip-gallery-image"
                                                style="width: 100%; height: 424px;"
                                                src="<?php echo $firstImage; ?>">
                                        </div>
                                        <div class="tab-gallery-preview">
                                            <div id="gallery">
                                                <?php
                                                    foreach($all_image as $all){
                                                ?>
                                                <div class="gallery-i">
                                                    <a href="<?php echo base_url($all->image); ?>">
                                                        <img
                                                            alt="trip-gallery-image"
                                                            style="width:108px; height: 75px;"
                                                            src="<?php echo base_url($all->image); ?>">
                                                        <span></span>
                                                    </a>
                                                </div>
                                                <!-- \\ -->
                                                
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- \\ gallery \\ -->

                                </div>           
                             <div class="clear"></div>
                            <div class="content-tabs">
                                <div class="content-tabs-head last-item">
                                    <nav>
                                        <ul>
                                            <li><a class="active" href="#">Deskripsi</a></li>
                                            <li><a href="#">Fasilitas</a></li>
                                            <li><a href="#">Informasi Trevel</a></li>
                                            <li><a href="#">Syarat & Ketentuan</a></li>
                                        </ul>
                                    </nav>
                                    
                                    <div class="clear"></div>
                                </div>

                                <!-- // description tabs // -->
                                <div class="content-tabs-body">

                                    <!-- // deskripsi trip // -->
                                    <div class="content-tabs-i">
                                        <h2>Deskripsi Perjalanan</h2>
                                        <p>
                                            <?php echo $detail_trip->description; ?>         
                                        </p>

                                        <div class="preferences-devider-a"></div>

                                        <div class="tab-reasons">

                                            <!-- // daftar kegiatan // -->
                                            <div class="facilities">
                                                <h2>Daftar Kegiatan</h2>
                                                <table>
                                                    <?php foreach($intenerary as $int) {?>
                                                    <tr>
                                                        <td class="facilities-a"><b><?php echo "Hari ".$int->hari; ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="facilities-b"><?php echo $int->description; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </table>	
                                            </div>
                                            <!-- \\ daftar kegiatan \\ -->

                                        </div>
                                    </div>
                                    <!-- \\ deskripsi trip \\ -->
                                    
                                    <!-- // fasilitas trip // -->
                                    <div class="content-tabs-i">
                                        <h2>Fasilitas Termasuk</h2>
                                        <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui voluptatem sequi nesciunt. </p> -->
                                        <ul class="preferences-list">
                                        <?php $data = trim($detail_trip->facilities_include, '"'); 
                                            $arr = explode (",",$data);

                                            foreach($arr as $include)
                                                echo "<li class='internet' style='min-height: 33px;'>". $include ."</li>";    
                                        ?>
                                            
                                        </ul>
                                        <div class="clear"></div>
                                        <div class="preferences-devider-b"></div>
                                        <br>

                                        <h2>Fasilitas Tidak Termasuk</h2>
                                        <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui voluptatem sequi nesciunt. </p> -->
                                        <ul class="preferences-list">
                                        <?php $data = trim($detail_trip->facilities_exclude, '"'); 
                                            $arr = explode (",",$data);

                                            foreach($arr as $include)
                                                echo "<li class='conf-room' style='min-height: 33px;'>". $include ."</li>";    
                                        ?>
                                            
                                        </ul>
                                        <div class="clear"></div>

                                        <div class="preferences-devider-b"></div>

                                        

                                    </div>
                                    <!-- \\ fasilitas trip \\ -->
                                    
                                    <!-- Informasi Penyedia Trevel -->

                                    <div class="content-tabs-i">
                                        <div class="tab-reasons">
                                            <h2><?php echo $tour_operator->name_operator; ?> </h2>
                                            <div class="tab-reasons-h">
                                                <div class="tab-reasons-i reasons-01">
                                                    <b>Alamat</b>
                                                    <p><?php echo $tour_operator->address; ?></p>
                                                </div>
                                                <div class="tab-reasons-i reasons-02">
                                                    <b>Phone & WhatsApp</b>
                                                    <p><?php echo "Phone : ".$tour_operator->phone; ?> | <?php echo "Whatsapp : ".$tour_operator->wa; ?></p>
                                                </div>
                                                <div class="tab-reasons-i reasons-03">
                                                    <b>Website</b>
                                                    <p><a target="_blank" href="https://<?php echo $tour_operator->website; ?>"><?php echo $tour_operator->website; ?></a></p>
                                                </div>
                                                <div class="tab-reasons-i reasons-04">
                                                    <b>Email</b>
                                                    <p><?php echo $tour_operator->email; ?></p>
                                                </div>
                                                <div class="tab-reasons-i reasons-05">
                                                    <b>Surat Ijin Penyelenggara Travel</b>
                                                    <p><?php echo $tour_operator->sipt; ?></p>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <!-- End Informasi Penyedia Trevel -->

                                    <!-- Syarat dan Ketentuan -->
                                    <div class="content-tabs-i">
                                        <h2>Mengapa Halal Local</h2>
                                        <p style="text-align:justify;">
                                            Halal Local adalah penyedia voucher diskon untuk berbagai paket wisata dan umrah. Kupon voucher tersebut tinggal ditunjukkan ke agen travel untuk mendapatkan potongan harga. Segala aktivitas tur di tangani langsung oleh pihak travel. Pihak Halal Local hanyalah marketplace untuk mendapatkan deals potongan harga
                                        
                                        </p>
                                        <div class="clear"></div>

                                        <div class="preferences-devider-b"></div>

                                        

                                    </div>

                                    <!-- End Syarat dan ketentuan -->

                                </div>
                                <!-- \\ description tabs \\ -->

                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

                <div class="sp-page-r">
                    <div class="h-detail-r">
                        <div class="h-detail-lbl">
                            <div class="h-detail-lbl-a"><?php echo $detail_trip->title; ?></div>
                            <?php
                                $depature = strtotime($detail_trip->depature_date);
                                $return = strtotime($detail_trip->return_date);
                                $price = number_format($detail_trip->price,0,',','.');
                                $price_komisi = number_format($detail_trip->komisi,0,',','.');
                                $price_retael = number_format($detail_trip->price_reteal,0,',','.');


                                $diskon = ($detail_trip->komisi / $detail_trip->price_reteal) * 100;
                                // var_dump($diskon);
                            ?>
                            <div class="h-detail-lbl-b"><?php echo date('d M Y', $depature); ?> - <?php echo date('d M Y', $return); ?></div>
                        </div>
                        <div class="h-tour">
						    <span style="font-family: 'Raleway';font-size: 10px;font-weight: 600 !important; text-transform: uppercase;color: gray;">RP</span>		
							<strike style="font-size: 22px; color: gray; border-right: 2px solid #808080a6;padding-right: 7px;"><?php echo $price_retael; ?></strike> 
                                <b style="font-size: 22px; color: gray; ">
                                    <span>DISKON</span>
                                    <?php echo round($diskon,2); ?> %
                                </b>
                            <br>
                            <span>RP</span>
                            <b><?php echo $price; ?></b>
                        </div>
                        <button class="book-btn" data-toggle="modal" data-target="#book-modal">DAPATKAN DISKONMU DISINI</button>
                    </div>
                    
                    <!-- // help // -->
                    <!-- <div class="h-help">
                        <div class="h-help-lbl">Ada Pertanyaan?</div>
                        <div class="h-help-lbl-a">Kami akan membantu Anda dengan senang hati!</div>
                        <div class="h-help-phone">2-800-256-124 23</div>
                        <div class="h-help-email">info@halallocal.com</div>
                    </div> -->
                    <!-- \\ help \\ -->
                    
                    <!-- // perjalanan seru lainnya // -->
                    <div class="h-liked">
                        <div class="h-liked-lbl">Perjalanan Seru Lainnya</div>
                        <div class="h-liked-row">
                            <!-- // -->
                            <?php foreach($trip_lainnya as $lainnya){ 
                                $depature = strtotime($lainnya->depature_date);
                                $return = strtotime($lainnya->return_date);
                                $price = number_format($lainnya->price,0,',','.');
                                $price_komisi = number_format($lainnya->komisi,0,',','.');
                                $price_retael = number_format($lainnya->price_reteal,0,',','.');

                                $images = $this->beranda_model->view_images($lainnya->id);	
                                $firstImage = "";
                                if(!isset($images->image)){
                                    $firstImage = assets_url('img/no-image.jpg');
                                }else{
                                    $firstImage = base_url($images->image);
                                }

                                $diskon = ($lainnya->komisi / $lainnya->price_reteal) * 100;    
                            ?>
                            <div class="h-liked-item">
                                <div class="h-liked-item-i">
                                    <div class="h-liked-item-l">
                                        <a href="<?php echo base_url('front-end/trip/trip/detail').$lainnya->id; ?>">
                                            <img alt="perjalanan-lain-image" style="width: 80px; height: 60px;" src="<?php echo $firstImage; ?>">
                                        </a>
                                    </div>
                                    <div class="h-liked-item-c">
                                        <div class="h-liked-item-cb">
                                            <div class="h-liked-item-p">
                                                <div class="h-liked-title">
                                                    <a href="<?php echo base_url('front-end/trip/trip/detail/').$lainnya->id; ?>"><?php echo $lainnya->title; ?></a>
                                                </div>
                                                <div class="h-liked-foot">
                                                    <span class="h-liked-comment">RP</span>
                                                    <span class="h-liked-price"><?php echo $price_komisi ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>	
                            </div>
                            <?php }?>
                            <!-- \\ -->
                        </div>			
                    </div>
                    <!-- \\ perjalanan seru lainnya \\ -->
                    
                    <!-- // alasan memilih halal local // -->
                    <div class="h-reasons">
                        <div class="h-liked-lbl">Alasan Memilih Halal Local</div>
                        <div class="h-reasons-row">

                            <!-- // -->
                            <div class="reasons-i">
                                <div class="reasons-h">
                                    <div class="reasons-l">
                                        <img alt="alasan-pilih-image" src="<?php echo assets_url('img/reasons-a.png') ?>">
                                    </div>
                                    <div class="reasons-r">
                                        <div class="reasons-rb">
                                            <div class="reasons-p">
                                                <div class="reasons-i-lbl"  style="padding: 12px;" >Diskon Yang Menarik</div>
                                                <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p> -->
                                            </div>
                                        </div>
                                        <br class="clear" />
                                    </div>
                                </div>
                            <div class="clear"></div>
                            </div>
                            <!-- \\ -->

                            <!-- // -->
                            <div class="reasons-i">
                                <div class="reasons-h">
                                    <div class="reasons-l">
                                        <img alt="alasan-pilih-image" src="<?php echo assets_url('img/reasons-b.png') ?>">
                                    </div>
                                    <div class="reasons-r">
                                        <div class="reasons-rb">
                                        <div class="reasons-p">
                                            <div class="reasons-i-lbl"  style="padding: 12px;" >Banyak Pilihan Paket Wisata</div>
                                            <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p> -->
                                        </div>
                                        </div>
                                        <br class="clear" />
                                    </div>
                                </div>
                            <div class="clear"></div>
                            </div>
                            <!-- \\ -->

                            <!-- // -->
                            <div class="reasons-i">
                                <div class="reasons-h">
                                    <div class="reasons-l">
                                        <img alt="alasan-pilih-image" src="<?php echo assets_url('img/reasons-c.png') ?>">
                                    </div>
                                    <div class="reasons-r">
                                        <div class="reasons-rb">
                                        <div class="reasons-p">
                                            <div class="reasons-i-lbl" style="padding: 12px;">Perjalanan Dan Informasi Halal</div>
                                            <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p> -->
                                        </div>
                                        </div>
                                        <br class="clear" />
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        <!-- \\ -->				
                        </div>
                    </div>
                    <!-- \\ alasan memilih halal local \\ -->
                    
                </div>
                <div class="clear"></div>
            </div>

        </div>	
    </div>  
</div>
<!-- /main-cont -->



<style>
    .tab-reasons-i {
        width: 50%;
        min-height: 100px;
    }


</style>