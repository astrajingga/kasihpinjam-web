<!-- main-cont -->
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
    <div class="page-head">
      <div class="clear"></div>
    </div>

	<div class="sp-page">
		<div class="sp-page-a">
			<div class="sp-page-l">
  				<div class="sp-page-lb">
  					<div class="sp-page-p">
						<div class="booking-left">
							<h2>Booking Selesai</h2>
							
							<div class="comlete-alert">
								<div class="comlete-alert-a">
									<b>Terima Kasih. Konfirmasi pesanan anda.</b>
									<span>Selalu kunjungi website kami untuk mendapatkan potongan harga yang menarik. </span>
								</div>
							</div>
							
							<div class="complete-info">
								<h2>Informasi Pribadi Anda</h2>
								<div class="complete-info-table">
									<div class="complete-info-i">
										<div class="complete-info-l">Nama Lengkap&nbsp;&nbsp;&nbsp;&nbsp;:</div>
										<div class="complete-info-r"><?php echo $booking->nama; ?></div>
										<div class="clear"></div>
									</div>
									<div class="complete-info-i">
										<div class="complete-info-l">Alamat Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
										<div class="complete-info-r"><?php echo $booking->email; ?></div>
										<div class="clear"></div>
									</div>
									<div class="complete-info-i">
										<div class="complete-info-l">No.Telp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
										<div class="complete-info-r"><?php echo $booking->telp; ?></div>
										<div class="clear"></div>
									</div>
								</div>
								
								<div class="complete-devider"></div>
								
								<div class="complete-txt">
									<h2>Informasi Pembayaran</h2>
									<p>Anda telah memesan <b><?php echo $sum->qty; ?></b> kupon, berikut merupakan rincian dari pesanan kupon anda.</p>
                                    <?php
                                        $price = number_format($detail_trip->price,0,',','.');
                                        $total =  $sum->qty * $detail_trip->price;
                                        $priceTotal = number_format($total,0,',','.');
                                        $created = strtotime($sum->created_at);
                                    ?>
                                    <div class="shortcodes">
                                        <table class="table-a light">
                                            <tbody>
                                            <tr>
                                                <th>Tanggal Pemesanan</th>
                                                <th>Nama Tour</th>
                                                <th>Harga</th>
                                            </tr>
                                            <tr>
                                                <td><?php echo date('d M Y', $created); ?></td>
                                                <td><?php echo $detail_trip->title; ?></td>
                                                <td><?php echo 'Rp '.$price; ?></td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><b>Jumlah Kupon</b></td>
                                                <td><?php echo $sum->qty; ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><b>Total Harga</b></td>
                                                <td><?php echo 'Rp '.$priceTotal; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>						
                                    </div>

                                    <div class="complete-txt-link"><a href="<?php echo base_url('front-end/trip/trip/detail/').$booking->id_tour;?>">Lihat Informasi Paket Wisata</a></div>
								</div>
								
								<!-- <div class="complete-devider"></div>
								
								<div class="complete-txt final">
									<h2>Booking Details</h2>
									<p>Qoluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui voluptatem sequi nesciunt. Que porro quisqua. Sed ut perspiciatis unde omnis ste natus error.</p>
									<div class="complete-txt-link"><a href="#">Your Hotel Info</a></div>
								</div> -->
								
							</div>
							
						</div>
  					</div>
  				</div>
  				<div class="clear"></div>
			</div>
		</div>

		<div class="sp-page-r">

			<!-- // help // -->
            <div class="h-help">
            <div class="h-help-lbl">Ada Pertanyaan?</div>
            <div class="h-help-lbl-a">Kami akan membantu Anda dengan senang hati!</div>
            <div class="h-help-phone">2-800-256-124 23</div>
            <div class="h-help-email">info@halallocal.com</div>
        </div>
        <!-- \\ help \\ -->
        
        <!-- // perjalanan seru lainnya // -->
        <div class="h-liked">
        <div class="h-liked-lbl">Perjalanan Seru Lainnya</div>
        <div class="h-liked-row">

            <!-- // -->
            <div class="h-liked-item">
                <div class="h-liked-item-i">
                    <div class="h-liked-item-l">
                        <a href="<?php echo base_url('front-end/trip/trip/detail') ?>">
                            <img alt="perjalanan-lain-image" src="<?php echo assets_url('img/like-01.jpg') ?>">
                        </a>
                    </div>
                    <div class="h-liked-item-c">
                        <div class="h-liked-item-cb">
                            <div class="h-liked-item-p">
                                <div class="h-liked-title">
                                    <a href="<?php echo base_url('front-end/trip/trip/detail') ?>">Andrassy Thai Hotel</a>
                                </div>
                                <div class="h-liked-foot">
                                    <span class="h-liked-comment">RP</span>
                                    <span class="h-liked-price">1.200.000</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>	
            </div>
            <!-- \\ -->

            <!-- // -->
            <div class="h-liked-item">
                <div class="h-liked-item-i">
                    <div class="h-liked-item-l">
                        <a href="<?php echo base_url('front-end/trip/trip/detail') ?>">
                            <img alt="perjalanan-lain-image" src="<?php echo assets_url('img/like-02.jpg') ?>">
                        </a>
                    </div>
                    <div class="h-liked-item-c">
                        <div class="h-liked-item-cb">
                            <div class="h-liked-item-p">
                                <div class="h-liked-title">
                                    <a href="<?php echo base_url('front-end/trip/trip/detail') ?>">Campanile Cracovie</a>
                                </div>
                                <div class="h-liked-foot">
                                    <span class="h-liked-comment">RP</span>
                                    <span class="h-liked-price">1.200.000</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>	
            </div>
            <!-- \\ -->

            <!-- // -->
            <div class="h-liked-item">
                <div class="h-liked-item-i">
                    <div class="h-liked-item-l">
                        <a href="<?php echo base_url('front-end/trip/trip/detail') ?>">
                            <img alt="perjalanan-lain-image" src="<?php echo assets_url('img/like-03.jpg') ?>">
                        </a>
                    </div>
                <div class="h-liked-item-c">
                    <div class="h-liked-item-cb">
                        <div class="h-liked-item-p">
                            <div class="h-liked-title">
                                <a href="<?php echo base_url('front-end/trip/trip/detail') ?>">Ermin's Hotel</a>
                            </div>
                            <div class="h-liked-foot">
                                <span class="h-liked-comment">RP</span>
                                <span class="h-liked-price">1.200.000</span>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                </div>
                <div class="clear"></div>	
            </div>
            <!-- \\ -->
        </div>			
    </div>
    <!-- \\ perjalanan seru lainnya \\ -->
    <!-- // alasan memilih halal local // -->
    <div class="h-reasons">
        <div class="h-liked-lbl">Alasan Memilih Halal Local</div>
            <div class="h-reasons-row">

            <!-- // -->
                <div class="reasons-i">
                    <div class="reasons-h">
                        <div class="reasons-l">
                            <img alt="alasan-pilih-image" src="<?php echo assets_url('img/reasons-a.png') ?>">
                        </div>
                        <div class="reasons-r">
                            <div class="reasons-rb">
                                <div class="reasons-p">
                                    <div class="reasons-i-lbl"  style="padding: 12px;" >Diskon Yang Menarik</div>
                                        <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p> -->
                                    </div>
                                </div>
                                <br class="clear" />
                             </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- \\ -->

                    <!-- // -->
                    <div class="reasons-i">
                        <div class="reasons-h">
                            <div class="reasons-l">
                                <img alt="alasan-pilih-image" src="<?php echo assets_url('img/reasons-b.png') ?>">
                            </div>
                            <div class="reasons-r">
                                <div class="reasons-rb">
                                    <div class="reasons-p">
                                        <div class="reasons-i-lbl"  style="padding: 12px;" >Banyak Pilihan Paket Wisata</div>
                                            <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p> -->
                                        </div>
                                    </div>
                                <br class="clear" />
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- \\ -->

                    <!-- // -->
                    <div class="reasons-i">
                        <div class="reasons-h">
                            <div class="reasons-l">
                                <img alt="alasan-pilih-image" src="<?php echo assets_url('img/reasons-c.png') ?>">
                            </div>
                            <div class="reasons-r">
                                <div class="reasons-rb">
                                    <div class="reasons-p">
                                        <div class="reasons-i-lbl" style="padding: 12px;">Perjalanan Dan Informasi Halal</div>
                                            <!-- <p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p> -->
                                        </div>
                                    </div>
                                <br class="clear" />
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                        <!-- \\ -->				
                </div>
            </div>
        <!-- \\ alasan memilih halal local \\ -->
			
		</div>
		<div class="clear"></div>
	</div>

    </div>	
  </div>  
</div>
<!-- /main-cont -->