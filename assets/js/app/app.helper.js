/**
 * Created by agungrizkyana on 10/16/16.
 */
angular.module('app.helper', [
    'app.config'
])
    .constant('appConfig', {
        sqlFormatDate: 'YYYY-MM-DD'
    })
    .factory('formHelper', ['appConfig', function (appConfig) {

        var golonganDarah = function (id) {
            var _golonganDarah = [
                { id: 1, text: 'A' },
                { id: 2, text: 'B' },
                { id: 3, text: 'O' },
                { id: 4, text: 'AB' }
            ];
            return _.find(_golonganDarah, { text: id });
        };

        return {
            wargaMapSelect2: {
                golonganDarah: golonganDarah
            }
        }
    }])
    .factory('fileHelper', ['$http', 'config', 'appConfig', '$q', function ($http, config, appConfig, $q) {

        var upload = function (formData) {
            var defer = $q.defer();
            return $.ajax({
                url: (formData.newFileName) ? config.uploadPath + '/' + formData.newFileName : config.uploadPath,
                type: 'POST',
                data: formData.file,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data, textStatus, xhr) {
                    console.log("textStatus", textStatus);
                    if (typeof data.error == 'undefined') {
                        return defer.resolve(data);
                    } else {
                        return defer.reject(data.error);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log("error, ", textStatus);
                    return defer.reject(errorThrown);
                }
            })
        };

        function download() {
            return 'belum dibuat';
        }

        return {
            upload: upload,
            download: download,

        }

    }])
    .factory('dateHelper', ['appConfig', function (appConfig) {
        var parseToSqlDate = function (tanggalLahir, format) {

            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));

            var _date = null;

            if (tanggal > 31 || bulan > 12) {
                _date = null;
            } else {

                var date = moment(tanggalLahir, format).format(appConfig.sqlFormatDate);
                var umur = {
                    tahun: moment().diff(date, 'years'),
                    bulan: moment().diff(date, 'months'),
                    hari: moment().diff(date, 'days'),
                };
                _date = umur.tahun + '-' + umur.bulan + '-' + umur.hari;

            }

            return date;

        }
        return {
            parseToSqlDate: parseToSqlDate
        }
    }])
    .factory('base64Helper', function () {
        function isBase64(str) {
            try {
                return btoa(atob(str)) == str;
            } catch (err) {
                return false;
            }
        }

        return {
            isBase64: isBase64
        }
    });