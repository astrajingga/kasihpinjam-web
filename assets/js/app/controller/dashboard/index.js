/**
 * Created by agungrizkyana on 12/20/16.
 */
angular.module('app', [
    'app.components',
    'app.helper',
    'easypiechart'
]).controller('DashboardController', PelayananController);

PelayananController.$inject = ['$rootScope', '$scope', '$http'];

function PelayananController($rootScope, $scope, $http) {

    var vm = this;

    var $persenWargaTetap = $('#persen-warga-tetap');
    var $persenWargaSementara = $('#persen-warga-sementara');

    $scope.persentaseSebaranWarga = {
        wargaTetap : {},
        wargaSementara : {}
    };

    $scope.persentaseSebaranWarga.wargaTetap.options = {
        animate: {
            duration: 1000,
            enabled: true
        },
        percent: 75,
        lineWidth: 4,
        trackColor: '#e8eff0',
        barColor: '#7266ba',
        scaleColor: false,
        size: 115,
        rotate: 90,
        lineCap: 'butt'
    };

    $scope.persentaseSebaranWarga.wargaSementara.options = {
        animate: {
            duration: 1100,
            enabled: true
        },
        percent: 50,
        lineWidth: 4,
        trackColor: '#e8eff0',
        barColor: '#23b7e5',
        scaleColor: false,
        size: 115,
        rotate: 180,
        lineCap: 'butt'
    };

    $http.get(
        base_url + '/api/dashboard/sebaran_warga'
    ).then(function success(result) {
        $persenWargaTetap.text(result.data.persen_warga_tetap + '%');
        $persenWargaSementara.text(result.data.persen_warga_sementara + '%');

        $scope.persentaseSebaranWarga = {
            wargaTetap: {
                percent: result.data.persen_warga_tetap,

            },
            wargaSementara: {
                percent: result.data.persen_warga_sementara,

            }
        };

    }, function error(err) {
        console.log(err);
    });


}