
var table = $('#table_trip').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "stateDuration": 0,
    "ajax": {
        "url": site_url + 'api/trip/trip',
        "type": "POST"
    },
    "order": [[0, 'desc']],
    "columns": [
        {
            "data": "created_at",
            "render": function (data, type, row, meta) {
                return data + "<br /><i>" + moment(data).fromNow() + "</i>";
            }
        },
        {
            "data": "name_operator",
            "render": function(data, type, row, meta) {
                return data ;
            } 
        },
        {
            "data": "title",
            "render": function(data, type, row, meta) {
                return data;
            }
        },
        {
            "data": "price",
            "render": function (data, type, row, meta) {
                return "<b>Harga Eceran : </b>Rp. " + numeral(row.price_reteal).format('Rp0,0.00') + 
                        "<br>" + "<b>Komisi : </b>Rp. " + numeral(row.komisi).format('Rp0,0.00') + 
                        "<br>" + "<b>Harga Publish : </b>Rp. " + numeral(row.price).format('Rp0,0.00'); 
            }

        },
        {
            "data": "id",
            "className": "text-right",
            "sortable": false,
            "searchable": false,
            "render": function (data, type, row, meta) {
                if(row.visible == 0){
                    return "<a onclick='restore(" + data + ")' class='btn btn-danger btn-sm'>Restore</a>";
                }else if(row.visible == 1){
                    return "<a onclick='deleteItem("+row.id+")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>&nbsp;<a href='" + site_url + "trip/list_trip/update/" + row.id + "' class='btn btn-default btn-sm'><i class='fa fa-edit'></i></a>";
                }else if(row.visible == 2){
                    return "<a href='" + site_url + "trip/list_trip/approve/" + row.id + "' class='btn btn-success btn-sm'>Approve</a>";
                }
                // return "<a onclick='deleteItem("+row.id+")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>&nbsp;<a href='" + site_url + "trip/list_trip/update/" + row.id + "' class='btn btn-default btn-sm'><i class='fa fa-edit'></i></a>";
            }
            
        }
    ]
});


function deleteItem($id) {
    if (confirm("Anda yakin akan menghapus data trip?")) {
        window.location = site_url + "api/trip/trip/delete/" + $id;
    }
    return false;
}


function restore($id){
    
         var formData = new FormData();
            $.ajax({
                url: site_url + 'api/trip/trip/restore/' + $id,
                method: 'post',
                dataType: 'json',
                data: $id,
                contentType: false,
                processData: false,
            }).then(function (result) {
                toastr.success('Data Berhasil Disimpan', 'Success!');
                setTimeout(function(){
                    window.location.href = site_url + '/trip/trip_list';
                }, 1000);
            }, function (err) {
                toastr.error('Tidak Berhasil Disimpan', 'Failed!');
            });
    }
    