$(document).ready(function(){
    $("#category").change(function() {
        if($("#category").val() == 1){
            $("#sipt").prop( "disabled", true );
            $("#ijin_trevel_umrah").prop( "disabled", false );
            $("#ijin_trevel_haji").prop( "disabled", false );
            // $("#siup").append('disabled');   
        }else{
            $("#sipt").prop( "disabled", false );
            $("#ijin_trevel_umrah").prop( "disabled", true );
            $("#ijin_trevel_haji").prop( "disabled", true );
        }   
      });
});


$('#form_add_pelanggan').validate({
    submitHandler: function (form) {
        
        var formData = new FormData($(form));

        formData.append('category', $('#category').val());
        formData.append('nama_operator', $('#nama_operator').val());
        formData.append('phone', $('#phone').val());
        formData.append('email_operator', $('#email_operator').val());
        formData.append('wa', $('#wa').val());
        formData.append('website', $('#website').val());
        formData.append('alamat', $('#alamat').val());
        formData.append('sipt', $('#sipt').val());
        formData.append('ijin_trevel_umrah', $('#ijin_trevel_umrah').val());
        formData.append('ijin_trevel_haji', $('#ijin_trevel_haji').val());
        formData.append('inputfile', $('#upload') [0].files[0]); 

        $.ajax({
            url: site_url + 'api/master/pelanggan/add',
            method: 'post',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Data Alat Berhasil Ditambahkan', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + 'master/data_pelanggan';
            }, 2000);
        }, function (err) {
            toastr.error('Tidak Berhasil Menambahkan Data Alat', 'Failed!');
        });
    }
});


$('#back').click(function (event){
    window.location.href = site_url + 'master/data_pelanggan';
});





















