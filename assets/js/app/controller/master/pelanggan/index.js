
var table = $('#table_pelanggan').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "stateDuration": 0,
    "ajax": {
        "url": site_url + 'api/master/pelanggan',
        "type": "POST"
    },
    "order": [[0, 'desc']],
    "columns": [
        {
            "data": "created_at",
            "render": function (data, type, row, meta) {
                return data + "<br /><i>" + moment(data).fromNow() + "</i>";
            }
        },
        {
            "data": "avatar_operator",
            "render": function (data, type, row, meta) {
                return ("<img width='50px' height='50px' src='" + site_url + data + "'>");
            }
        },
        {
            "data": "name_operator",
            "render": function (data, type, row, meta) {
                return (data + '<br><b><i> ' + row.phone + '</i></b> ');
            }
        },
        {
            "data": "email_operator"
        },
        {
            "data": "address"
        },
        {
            "data": "website"
        },
        {
            "data": "action",
            "orderable": false,
            "render": function (data, type, row, meta) {
                return "<a onclick='deleteItem(" + row.id + ")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>&nbsp;<a href='" + site_url + "auth/user/edit/" + row.id + "' class='btn btn-default btn-sm'><i class='fa fa-edit'></i></a>&nbsp;<a onclick='" + site_url + "auth/user/edit/" + row.id + "' class='btn btn-default btn-sm'><i class='fa fa-eye'></i></a>";
            }
            
        }
    ]
});