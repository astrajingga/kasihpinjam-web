
var table = $('#table_fasilitas').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "stateDuration": 0,
    "ajax": {
        "url": site_url + 'api/master/fasilitas',
        "type": "POST"
    },
    "order": [[0, 'desc']],
    "columns": [
        {
            "data": "created_at",
            "render": function (data, type, row, meta) {
                return data + "<br /><i>" + moment(data).fromNow() + "</i>";
            }
        },
        {
            "data": "image",
            "render": function(data, type, row, meta) {
                return ("<img width='50px' height='50px' src='" + site_url + data + "'>" );
            } 
        },
        {
            "data": "name",
        },
        {
            "data": "description",
        },
        {
            "data": "id",
            "className": "text-right",
            "sortable": false,
            "searchable": false,
            "render": function (data, type, row, meta) {
                return "<a onclick='deleteItem("+row.id+")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>&nbsp;<a href='" + site_url + "auth/user/edit/" + row.id + "' class='btn btn-default btn-sm'><i class='fa fa-edit'></i></a>";
            }
            
        }
    ]
});