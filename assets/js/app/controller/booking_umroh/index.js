
var table = $('#table_booking').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "stateDuration": 0,
    "ajax": {
        "url": site_url + 'api/booking/booking_umroh',
        "type": "POST"
    },
    "order": [[0, 'desc']],
    "createdRow": function( row, data, dataIndex ) {
        if(data.approve == 1){
            $(row).css('background-color', '#dbf2c3');
        }else if(data.approve == 2){
            $(row).css('background-color', '#EDB6A6');
        }
    },
    "columns": [
        {
            "data": "created_at",
            "render": function (data, type, row, meta) {
                return data + "<br /><i>" + moment(data).fromNow() + "</i>";
            }
        },
        {
            "data": "username",
        },
        {
            "data": "nama",
        },
        {
            "data": "code",
        },
        {
            "data": "telp",
        },
        {
            "data": "email",
        },
        {
            "data": "id",
            "className": "text-right",
            "sortable": false,
            "searchable": false,
            "render": function (data, type, row, meta) {
                if(row.approve == 1){
                    return "<a onclick='reject(" + data + ")' class='btn btn-default btn-sm'>Reject</a>";
                }else if(row.approve == 2){
                    return "<a onclick='accept(" + data + ")' class='btn btn-success btn-sm'>Accept</a>";
                }else if(row.approve == 0){
                    return "<a onclick='accept(" + data + ")' class='btn btn-success btn-sm'>Accept</a>&nbsp;<a onclick='reject(" + data + ")' class='btn btn-default btn-sm'>Reject</a>";
                }
                
            }
            
        }
    ]
});



function accept($id){

     var formData = new FormData();
        $.ajax({
            url: site_url + 'api/booking/booking_umroh/accept/' + $id,
            method: 'post',
            dataType: 'json',
            data: $id,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Data Berhasil Disimpan', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + '/booking/booking_umroh';
            }, 1000);
        }, function (err) {
            toastr.error('Tidak Berhasil Disimpan', 'Failed!');
        });
}

function reject($id){
    
         var formData = new FormData();
            $.ajax({
                url: site_url + 'api/booking/booking_umroh/reject/' + $id,
                method: 'post',
                dataType: 'json',
                data: $id,
                contentType: false,
                processData: false,
            }).then(function (result) {
                toastr.success('Data Berhasil Disimpan', 'Success!');
                setTimeout(function(){
                    window.location.href = site_url + '/booking/booking_umroh';
                }, 1000);
            }, function (err) {
                toastr.error('Tidak Berhasil Disimpan', 'Failed!');
            });
    }