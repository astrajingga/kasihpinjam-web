textSpesifikasiGlobal = $('.summernote').summernote({
    height: 150
});

$('#form_add_fasilitas').validate({
    submitHandler: function (form) {
        
        var formData = new FormData($(form));

        formData.append('nama', $('#nama').val());
        formData.append('description', $('#description').val());
        formData.append('inputfile', $('#upload') [0].files[0]); 


        $.ajax({
            url: site_url + 'api/master/fasilitas/add',
            method: 'post',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Data Fasilitas Berhasil Ditambahkan', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + 'master/data_fasilitas';
            }, 2000);
        }, function (err) {
            toastr.error('Tidak Berhasil Menambahkan Data Fasilitas', 'Failed!');
        });
    }
});


$('#back').click(function (event){
    window.location.href = site_url + 'master/data_fasilitas';
});

    



















