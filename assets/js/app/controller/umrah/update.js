
 $('#komisi').change(function(){
    var pricePublish = $('#harga_eceran').val() - $('#komisi').val();
    $('#harga').val(pricePublish);
 });

 $('#harga_eceran').change(function(){
     var pricePublish =  $('#harga_eceran').val() - $('#komisi').val();
     $('#harga').val(pricePublish);
  });


function addIntenerary(){
    // var day = $("#valueDay").val();
        $( "#update_view_intenerary" ).append(
            '<div class="col-lg-6"><a class="pull-right" onclick="deleteAlat($(this))"> <i style="margin: 13px;" class="fa fa-close"></i> </a>'+
            '<div class="panel panel-default">' +
                '<div class="panel-heading">Tambah Hari </div>' +
                    '<div class="panel-body">' +
                        '<div class="flot-chart">' +
                            '<div class="row">' +
                                '<label class="col-lg-2 text-left">Hari Ke -</label>' + 
                                '<div class="col-lg-4">' + 
                                    '<input type="number" class="hari form-control" name="hari[]" id="hari[]" />' +
                                '</div>' +
                            '</div><br>'+
                        '<textarea required name="intenerary[]" id="intenerary[]" class="form-control intenerary" cols="10" rows="5"></textarea>' +
                    '</div>' + 
                '</div>' +
            '</div>'+
            '</div>'
        );
        textSpesifikasiGlobal = $('.intenerary').summernote({
            height: 150
        });
    // day++;
    // $("#valueDay").val(day);
}
$('#submit').click(function(){
    var hari = _.map($('.hari'), function (el) {
        return $(el).val();
    });
    var intenerary = _.map($('.intenerary'), function (el) {
        return $(el).val();
    });
    var formData = new FormData();
    formData.append('id', $('#id_tour_trip').val());

    for (var i = 0; i < hari.length; i++) {
        for (var i = 0; i < intenerary.length; i++) {
            console.log(intenerary[i]);
            console.log(hari[i]);
            formData.append('intenerary', intenerary[i]);
            formData.append('hari', hari[i]);

            $.ajax({
                url: site_url + "api/umrah/umrah/save",
                method:"POST",
                data:formData,
                contentType:false,
                cache:false,
                processData:false,
                }).then(function (result) {
            });
        }
    }
});

function deleteImage(id,id_tour) {
    // var txt;
    var r = confirm("Apa anda yakin ingin menghapus gambar?");
    if (r == true) {
        $.ajax({
            url: site_url + 'api/umrah/umrah/deleteImage/' + id ,
            method: 'post',
            dataType: 'json',
            data: id,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Success Delete Image', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + 'trip/list_umrah/update/' + id_tour;
            }, 1000);
        }, function (err) {
            toastr.error('Failed to Delete Image', 'Failed!');
        });
    } else {
        toastr.error('Gambar batal dihapus', 'Dibatalkan!');
    }
    
}

function deleteAlat(id,id_tour) {
    var r = confirm("Apa anda yakin ingin menghapus intenerary?");
    if (r == true) {
        $.ajax({
            url: site_url + 'api/umrah/umrah/deleteIntenerary/' + id ,
            method: 'post',
            dataType: 'json',
            data: id,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Success Delete Intenerary', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + 'trip/list_umrah/update/' + id_tour;
            }, 1000);
        }, function (err) {
            toastr.error('Failed to Delete Intenerary', 'Failed!');
        });
    } else {
        toastr.error('Intenerary batal dihapus', 'Dibatalkan!');
    }
}

$(document).ready(function () {
    textSpesifikasiGlobal = $('.summernote').summernote({
        height: 150
    });
    textSpesifikasiGlobal = $('.intenerary').summernote({
        height: 150
    });
    $('.view_upload_intenerary').summernote({
        height: 150
    });
    $('.view_upload_intenerary').summernote('disable');
    
    $('#submit').click(function(){
        var files = $('#files')[0].files;
        var error = '';
        var form_data = new FormData();
        for(var count = 0; count<files.length; count++)
        {
         var name = files[count].name;
         var extension = name.split('.').pop().toLowerCase();
         if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
         {
          error += "Invalid " + count + " Image File"
         }
         else
         {
          form_data.append('id', $('#id_tour_trip').val());
          form_data.append("files[]", files[count]);
         }
        }
        if(error == '')
        {
         $.ajax({
          url: site_url + "trip/list_umrah/upload",
          method:"POST",
          data:form_data,
          contentType:false,
          cache:false,
          processData:false,
         })
        }
        else
        {
         alert(error);
        }
       });
});



$('#form_update_trip').validate({
    submitHandler: function (form) {
        
        var formData = new FormData($(form));

        var formInput = {
            id: $(form).find('input[name=id]').val()
        };

        formData.append('category', $('#category').val());
        formData.append('tanggal_berangkat', $('#tanggal_berangkat').val());
        formData.append('tanggal_kepulangan', $('#tanggal_kepulangan').val());
        formData.append('title', $('#title').val());
        formData.append('harga', $('#harga').val());
        formData.append('harga_eceran', $('#harga_eceran').val());
        formData.append('komisi', $('#komisi').val());
        formData.append('description', $('#description').val());
        formData.append('include', $('#fasilities_include').val());
        formData.append('exclude', $('#fasilities_exclude').tagsinput('items'));
	


        $.ajax({
            url: site_url + 'api/umrah/umrah/update/' + formInput.id,
            method: 'post',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Data Trip Berhasil Ditambahkan', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + 'trip/list_umrah';
            }, 2000);
        }, function (err) {
            toastr.error('Tidak Berhasil Menambahkan Data Alat', 'Failed!');
        });
    }
});


$('#back').click(function (event){
    window.location.href = site_url + 'master/data_pelanggan';
});

    



















