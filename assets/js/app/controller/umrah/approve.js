
 $('#komisi').change(function(){
    var pricePublish = $('#harga_eceran').val() - $('#komisi').val();
    $('#harga').val(pricePublish);
 });

 $('#harga_eceran').change(function(){
     var pricePublish =  $('#harga_eceran').val() - $('#komisi').val();
     $('#harga').val(pricePublish);
  });


$(document).ready(function () {
    textSpesifikasiGlobal = $('.summernote').summernote({
        height: 150
    });
    $('.summernote').summernote('disable');
});



$('#form_update_trip').validate({
    submitHandler: function (form) {
        
        var formData = new FormData($(form));

        var formInput = {
            id: $(form).find('input[name=id]').val()
        };

        formData.append('harga', $('#harga').val());
        formData.append('harga_eceran', $('#harga_eceran').val());
        formData.append('komisi', $('#komisi').val());
	


        $.ajax({
            url: site_url + 'api/umrah/umrah/approve/' + formInput.id,
            method: 'post',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
        }).then(function (result) {
            toastr.success('Data Trip Berhasil Dipublist', 'Success!');
            setTimeout(function(){
                window.location.href = site_url + 'trip/list_umrah';
            }, 2000);
        }, function (err) {
            toastr.error('Tidak Berhasil Melakukan Approve', 'Failed!');
        });
    }
});


$('#back').click(function (event){
    window.location.href = site_url + 'master/data_pelanggan';
});

    



















