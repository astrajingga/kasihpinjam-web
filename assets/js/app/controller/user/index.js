
var table = $('#table_user').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "stateDuration": 0,
    "ajax": {
        "url": site_url + 'api/user',
        "type": "POST"
    },
    "order": [[0, 'desc']],
    "columns": [
        {
            "data": "registered",
            "render": function (data, type, row, meta) {
                return data ;
            }
        },
        { "data": "name_operator" },
        { "data": "username" },
        { "data": "email_operator" },
        { "data": "name" },
        {
            "data": "id",
            "className": "text-right",
            "sortable": false,
            "searchable": false,
            "render": function (data, type, row, meta) {
                return "<a onclick='deleteItem("+row.id+")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>&nbsp;<a href='" + site_url + "auth/user/edit/" + row.id + "' class='btn btn-default btn-sm'><i class='fa fa-edit'></i></a>";
            }
            
        }

    ]
});


function deleteItem($id) {
    if (confirm("Anda yakin akan menghapus data user?")) {
        window.location = site_url + "auth/user/delete/" + $id;
    }
    return false;
}

